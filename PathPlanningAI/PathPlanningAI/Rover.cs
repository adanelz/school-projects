﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PathPlanningAI
{
    class Rover
    {
        public Vector3 Location { get; set; }
        public float Speed { get; set; }
        public Course Route { get; set; }
        public DecisionEngine AI;
        public Control Manual;

        public Rover()
        {
            this.Location = null;
            this.Speed = 0;
            this.Route = null;
            this.AI = new DecisionEngine();
            this.Manual = new Control();
        }

        public void UpdateRoute()
        {

        }

        public void SetStartPosition()
        {

        }
    }
}

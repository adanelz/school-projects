﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PathPlanningAI
{
    public partial class Form1 : Form
    {
        Rover MarsRover = new Rover();
        int MouseX = 0;
        int MouseY = 0;
        Waypoint Start = new Waypoint();
        Waypoint End = new Waypoint();
        int ix = 0;
        int iy = 0;
        int fx = 0;
        int fy = 1;
        Bitmap Origional;
        Bitmap Pixelated;
        bool SetRover = false;
        Waypoint onPath = new Waypoint();

        

        public Form1()
        {
            InitializeComponent();
            LoadMap();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            LoadMap();
        }

        private void LoadMap()
        {
            Bitmap bm = new Bitmap(@"C:\Users\scoobah36\Documents\Visual Studio 2010\Projects\PathPlanningAI\PathPlanningAI\Maps\heightmap_1.jpg", true);
            Origional = bm;
            bm = MarsRover.AI.Pixelate(bm, new Rectangle(0, 0, bm.Width, bm.Height), int.Parse(numericUpDown1.Value.ToString()));
            Pixelated = bm;
            pictureBox1.Image = Pixelated;
        }

        private void DrawPath()
        {
            Bitmap bm = new Bitmap(Pixelated);
            Graphics g = Graphics.FromImage(bm);

            Course route = MarsRover.Route;

            Pen courseLine = new Pen(Color.Red, 1);

            int offset = (int)numericUpDown1.Value;

            for (int i = 0; i < route.Path.Count - 1; i++)
            {
                g.DrawLine(courseLine, (route.Path[i].X * offset) + (offset / 2),
                                        (route.Path[i].Y * offset) + (offset / 2),
                                        (route.Path[i + 1].X * offset) + (offset / 2),
                                        (route.Path[i + 1].Y * offset) + (offset / 2));
            }

            //bm = WriteWeights(bm, g);

            pictureBox1.Image = bm;
        }

        private void DrawPath(Bitmap bm)
        {
            Graphics g = Graphics.FromImage(bm);

            Course route = MarsRover.Route;

            Pen courseLine = new Pen(Color.Red, 1);

            int offset = (int)numericUpDown1.Value;
            if (route != null)
            {
                for (int i = 0; i < route.Path.Count - 1; i++)
                {
                    g.DrawLine(courseLine, (route.Path[i].X * offset) + (offset / 2),
                                            (route.Path[i].Y * offset) + (offset / 2),
                                            (route.Path[i + 1].X * offset) + (offset / 2),
                                            (route.Path[i + 1].Y * offset) + (offset / 2));
                }

            }
            //bm = WriteWeights(bm, g);

            pictureBox1.Image = bm;
        }

        private Bitmap WriteWeights(Bitmap bm, Graphics g)
        {
            int amount = (int)numericUpDown1.Value;

            for (int i = 0; i < MarsRover.AI.Nodes.Count; i++)
            {
                if (MarsRover.AI.Nodes[i].TempWeight != int.MaxValue)
                {
                    int nodeX = MarsRover.AI.Nodes[i].X * amount;
                    int nodeY = MarsRover.AI.Nodes[i].Y * amount;
                    string weight = MarsRover.AI.Nodes[i].TempWeight.ToString();
                    g.DrawString(weight, new Font("Arial", 7), new SolidBrush(Color.Yellow), nodeX, nodeY);
                }
            }

            return bm;
        }

        private void Reset()
        {
            MarsRover = new Rover();
            MouseX = 0;
            MouseY = 0;
            Start = new Waypoint();
            End = new Waypoint();
            LoadMap();
            label1.Text = "Click Start Location";
            label2.Text = "Click End Location";
            label2.Visible = false;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            int amount = (int)numericUpDown1.Value;

            if (e.X < pictureBox1.Image.Width && e.Y < pictureBox1.Image.Height)
            {

                MouseX = (e.X / amount);
                MouseY = (e.Y / amount);

                if (SetRover)
                {
                    Bitmap bm = new Bitmap(Pixelated);
                    Graphics g = Graphics.FromImage(bm);
                    if (MarsRover.Route != null)
                    {
                        for (int i = 0; i < MarsRover.Route.Path.Count; i++)
                        {
                            int x = MarsRover.Route.Path[i].X;
                            int y = MarsRover.Route.Path[i].Y;
                            if (MouseX == x || MouseY == y)
                            {
                                if (onPath != null)
                                {
                                    int newWeight = Math.Abs(MouseX - x) + Math.Abs(MouseY - y);
                                    int oldWeight = Math.Abs(MouseX - onPath.Y) + Math.Abs(MouseY - onPath.Y);
                                    if (newWeight < oldWeight)
                                    {
                                        onPath = MarsRover.Route.Path[i];
                                    }
                                }
                                else
                                {
                                    onPath = MarsRover.Route.Path[i];
                                }
                            }
                        }

                        int offset = amount / 2;

                        g.DrawEllipse(new Pen(Color.Green, 5), (onPath.X * amount) + offset, (onPath.Y * amount) + offset, 2, 2);
                        DrawPath(bm);
                    }
                }

                //label3.Text = "tmp weight:" + MarsRover.AI.Grid[MouseX, MouseY].TempWeight.ToString();
                //label4.Text = "weight:" + MarsRover.AI.Grid[MouseX, MouseY].Weight.ToString();

            }


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (SetRover)
            {
                if (onPath != null)
                {
                    Bitmap bm = Origional;
                    int offset = 50;
                    int amount = (int)numericUpDown1.Value;
                    int newRes = amount / 2;
                    int x = onPath.X * amount;
                    int y = onPath.Y * amount;
                    int width = offset * 2;
                    int height = offset * 2;
                    x = x - offset;
                    y = y - offset;

                    //to close to the left edge
                    if (x < 0)
                        x = 0;

                    //to close to the right edge
                    if ((bm.Width - x) < width)
                        x = bm.Width - width;

                    //to close to the top edge
                    if (y < 0)
                        y = 0;

                    //to close to the bottom edge
                    if ((bm.Height - x) < height)
                        y = bm.Height - height;

                    Graphics g = Graphics.FromImage(Pixelated);

                    Rectangle scanArea = new Rectangle(x, y, width, height);

                    bm = MarsRover.AI.Pixelate(bm, scanArea, newRes);

                    
                    pictureBox1.Image = bm;
                    DrawPath(bm);
                    SetRover = false;
                }
            }
            else
            {
                if (!Start.Discovered)
                {
                    LoadMap();
                    Start.X = MouseX;
                    Start.Y = MouseY;
                    Start.Discovered = true;
                    label2.Visible = true;
                    label1.Text = MouseX.ToString() + "," + MouseY.ToString();
                }
                else
                {
                    End.X = MouseX;
                    End.Y = MouseY;
                    End.Discovered = true;
                    label2.Text = MouseX.ToString() + "," + MouseY.ToString();
                }

                if (Start.Discovered && End.Discovered)
                {
                    MarsRover.Route = MarsRover.AI.DijkstraAlgorithm(MarsRover.AI.Grid[Start.X, Start.Y], MarsRover.AI.Grid[End.X, End.Y]);
                    DrawPath();

                    Start = new Waypoint();
                    End = new Waypoint();

                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {//Reset
            Reset();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled)
                timer1.Stop();
            else
                timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int width = pictureBox1.Image.Width / (int)numericUpDown1.Value;
            int height = pictureBox1.Image.Height / (int)numericUpDown1.Value;

            if (!((ix == fx) && (iy == fy)))
            {
                Reset();
                MarsRover.Route = MarsRover.AI.DijkstraAlgorithm(MarsRover.AI.Grid[ix, iy], MarsRover.AI.Grid[fx, fy]);
                DrawPath();
                MarsRover.Route = MarsRover.AI.CleanRoute(MarsRover.Route);
                DrawPath();


            }
            fy+= 10;
            if (fy >= height)
            {
                fy = 0;
                fx += 10;
                if (fx >= width)
                {
                    fx = 0;
                    iy += 10;
                    if (iy >= height)
                    {
                        iy = 0;
                        ix += 10;
                        if (ix >= width)
                        {
                            ix = 0;
                            timer1.Stop();
                        }
                    }
                }
            }
                        
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MarsRover.Route = MarsRover.AI.CleanRoute(MarsRover.Route);
           
            DrawPath();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!SetRover)
                SetRover = true;
            else
                SetRover = false;
        }
    }
}

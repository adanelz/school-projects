﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PathPlanningAI
{
    class Waypoint
    {
        public float Weight { get; set; }
        public float TempWeight { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool Discovered { get; set; }
        public List<Waypoint> Neighbors { get; set; }
        public int QueIndex { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Waypoint()
        {
            this.Weight = 0;
            this.TempWeight = 0;
            this.X = 0;
            this.Y = 0;
            this.Discovered = false;
            this.Neighbors = new List<Waypoint>();
            this.QueIndex = 0;
        }

        /// <summary>
        /// weight constructor
        /// </summary>
        /// <param name="wight">The difference of color from white</param>
        public Waypoint(int weight)
        {
            this.Weight = weight;
            this.TempWeight = 0;
            this.X = 0;
            this.Y = 0;
            this.Discovered = false;
            this.Neighbors = new List<Waypoint>();
            this.QueIndex = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="tmpWeight"></param>
        public Waypoint(int weight, int tmpWeight)
        {
            this.Weight = weight;
            this.TempWeight = tmpWeight;
            this.X = 0;
            this.Y = 0;
            this.Discovered = false;
            this.Neighbors = new List<Waypoint>();
            this.QueIndex = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="weight">The difference of color from white</param>
        /// <param name="x">referes to x location of waypoint on map in grid</param>
        /// <param name="y">referes to y location of waypoint on map in grid</param>
        public Waypoint(int weight, int x, int y)
        {
            this.Weight = weight;
            this.TempWeight = 0;
            this.X = x;
            this.Y = y;
            this.Discovered = false;
            this.Neighbors = new List<Waypoint>();
            this.QueIndex = 0;
        }

        /// <summary>
        /// Specific constructor
        /// </summary>
        /// <param name="weight">The difference of color from white</param>
        /// <param name="tempweight"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="disc"></param>
        public Waypoint(float weight, float tempweight, int x, int y, bool disc, List<Waypoint> list, int queIndex)
        {
            this.Weight = weight;
            this.TempWeight = tempweight;
            this.X = x;
            this.Y = y;
            this.Discovered = disc;
            this.Neighbors = list;
            this.QueIndex = queIndex;
        }

        public override string ToString()
        {
            string coordinates = "(" + this.X + ", " + this.Y + ")";

            return coordinates;
        }
    }
}

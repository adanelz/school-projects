﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Collections;

namespace PathPlanningAI
{

    class DecisionEngine
    {
        public Waypoint[,] Grid;
        public List<Waypoint> Nodes;
        /// <summary>
        /// Elevation Constant
        /// </summary>
        protected int E = 70;
        /// <summary>
        /// Distance Constant
        /// </summary>
        protected int D = 50;
        public int SmoothRoute = 0;

        public DecisionEngine()
        {
            Nodes = new List<Waypoint>();
        }

        public Bitmap Pixelate(Bitmap image, Rectangle rectangle, int amount)
        {
            int width = image.Width;
            int height = image.Height;
            Bitmap pixelated = new System.Drawing.Bitmap(width, height);
            Nodes.Clear();

            this.Grid = new Waypoint[(width / amount) + 1, (height / amount) + 1];

            // make an exact copy of the bitmap provided
            Graphics graphics = Graphics.FromImage(pixelated);
            graphics.DrawImage(image, new Rectangle(0, 0, width, height),
                    new Rectangle(0, 0, width, height), GraphicsUnit.Pixel);
            Graphics g = Graphics.FromImage(pixelated);

            // look at every pixel in the rectangle while making sure we're within the image bounds
            for (Int32 ix = rectangle.X; ix < rectangle.X + rectangle.Width; ix += amount)
            {
                for (Int32 iy = rectangle.Y; iy < rectangle.Y + rectangle.Height; iy += amount)
                {

                    Color pixel;
                    int count = 0;
                    int r = 0;
                    // get the pixel color in the center of the soon to be pixelated area
                    for (Int32 x = ix; x < ix + amount && x < width; x++)
                    {
                        for (Int32 y = iy; y < iy + amount && y < height; y++)
                        {
                            pixel = pixelated.GetPixel(ix, iy);
                            r += pixel.R;
                            count++;
                        }
                    }

                    //gets the average color
                    r = r / count;
                    int nodeX = ix / amount;
                    int nodeY = iy / amount;

                    this.Grid[nodeX, nodeY] = new Waypoint(r, nodeX, nodeY);
                    Nodes.Add(Grid[nodeX, nodeY]);

                   
                    Color averageColor = Color.FromArgb(r, r, r);
                   

                    //make a rectange with the average color and is the size that is to be pixelated
                    g.FillRectangle(new SolidBrush(averageColor), new Rectangle(ix,iy,amount,amount));
                }
                
            }
            return pixelated;
        }
        
        public List<Waypoint> PlanRoute(int x, int y)
        {
            List<Waypoint> route = new List<Waypoint>();
            Waypoint current = Grid[x, y];
                       

            //while (Math.Abs(route[0].Weight - route[route.Count - 1].Weight) < 50)
            //for (int i = 0; i < 200; i++)
            while(true)
            {
                //creates the first temp variable to have the most different weight posible
                Waypoint tmp = new Waypoint(2000); 
                //add current waypoint to the route
                route.Add(current);
                //set is as discovered
                current.Discovered = true;

                for (int ix = -1; ix <= 1; ix++)
                {//changes the x value
                    for (int iy = -1; iy <= 1; iy++)
                    {//changes the y value
                        if ( ((current.X + ix) < Math.Sqrt((double)Grid.Length) - 1) && ((current.X + ix) >= 0)  && 
                             ((current.Y + iy) < Math.Sqrt((double)Grid.Length) - 1) && ((current.Y + iy) >= 0)  )
                        {//makes sure the node being checked and inside the bounds of the array
                            if (Grid[current.X + ix, current.Y + iy] != null)
                            {//makes sure the node being checked isn't null
                                if (!Grid[current.X + ix, current.Y + iy].Discovered)
                                {//check to make sure it is not looking at a nod that is already found
                                    if ((Math.Abs(current.Weight - Grid[current.X + ix, current.Y + iy].Weight)) < (Math.Abs(current.Weight - tmp.Weight)))
                                    {//checks the weight difference of a new node if it is less then the stored tmp pointer node
                                        tmp = Grid[current.X + ix, current.Y + iy];
                                    }
                                }
                            }
                        }
                    }
                }

                //move the current pointer to the next closes waypoint
                current = Grid[tmp.X, tmp.Y];

                //stops the loop when there is no new node to be found
                if (tmp.Weight == 2000)
                    break;
            }

            return route;
        }

        public Course DijkstraAlgorithm(Waypoint start, Waypoint end)
        {// Pixelate() must run first
            List<Waypoint> nodeQue = new List<Waypoint>();
            Course bestRoute = new Course();
            Waypoint current = start;

            if (Grid[0, 0].X != -1)
            {//test is Pixelate() has generated the Grid

                #region Initialize
                //Initialize each Node
                for (int i = 0; i < Nodes.Count; i++)
                {
                    //add neighbors to each Node
                    #region add neighbors to each Node
                    Nodes[i].Neighbors.Clear();
                    for (int ix = -1; ix <= 1; ix++)
                    {//changes the x value
                        for (int iy = -1; iy <= 1; iy++)
                        {//changes the y value
                            if (((Nodes[i].X + ix) < Math.Sqrt((double)Grid.Length) - 1) && ((Nodes[i].X + ix) >= 0) &&
                                 ((Nodes[i].Y + iy) < Math.Sqrt((double)Grid.Length) - 1) && ((Nodes[i].Y + iy) >= 0) )
                            {//makes sure the node being checked and inside the bounds of the array
                                if (!(ix == 0 && iy == 0))
                                {
                                    if (Grid[Nodes[i].X + ix, Nodes[i].Y + iy] != null)
                                    {//makes sure the node being checked isn't null
                                        Nodes[i].Neighbors.Add(Grid[Nodes[i].X + ix, Nodes[i].Y + iy]);
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    //add each node to the que
                    nodeQue.Add(Nodes[i]);

                    //make each node's tmpweight to be Max value
                    Nodes[i].TempWeight = int.MaxValue;

                    //set starting node weight to 0
                    if(Nodes[i] == start)
                        Nodes[i].TempWeight = 0;
                }
                #endregion

                #region activate(discover) best weighted nodes
                while (current != end)
                {//stops when it finds the end node
                    

                    #region change neighbors weights
                    for (int i = 0; i < current.Neighbors.Count; i++)
                    {
                        Waypoint neighbor = current.Neighbors[i];
                        if (!neighbor.Discovered)
                        {
                            //distance based on cooradinates
                            int distance = Math.Max(Math.Abs(start.X - neighbor.X), Math.Abs(start.Y - neighbor.Y));
                            //elevation based on weight vs. start weight
                            int elevation = (int)Math.Abs(start.Weight - neighbor.Weight);

                            if (elevation < E)
                            {
                                //get weight with elevation and distance constants
                                neighbor.TempWeight = elevation + (distance * D);
                            }
                            else
                            {
                                //get weight with elevation and distance constants
                                neighbor.TempWeight = ((elevation * E) + (distance * D));
                            }

                        }
                    }
                    #endregion

                    #region find lightest node in nodeQue
                    Waypoint minWeight = new Waypoint(int.MaxValue, int.MaxValue);
                    for (int i = 0; i < nodeQue.Count; i++)
                    {
                        //set QueIndex
                        nodeQue[i].QueIndex = i;

                        //find the least weighted node
                        if (nodeQue[i].TempWeight < minWeight.TempWeight)
                            minWeight = nodeQue[i];
                    }
                    #endregion

                    //if no lighter node was found
                    if (minWeight.Weight == int.MaxValue)
                        break;

                    //Move to the next node and remove current from que
                    minWeight.Discovered = true;
                    nodeQue.Remove(current);
                    current = minWeight;

                }
                #endregion

                //start from the end node
                current = end;
                //remove from que
                nodeQue.Remove(end);

                #region Make best route
                while (current != start)
                {//stops when it gets back to start

                    //prevent lookback
                    current.Discovered = false;
                    //add to path
                    bestRoute.Path.Add(current);

                    Waypoint minWeight = new Waypoint(int.MaxValue, int.MaxValue);
                    for (int i = 0; i < current.Neighbors.Count; i++)
                    {
                        //find the least weighted node
                        if (current.Neighbors[i].Discovered)
                        {
                            if (current.Neighbors[i].TempWeight < minWeight.TempWeight)
                                minWeight = current.Neighbors[i];

                        }
                    }

                    //if no lighter node was found
                    if (minWeight.Weight == int.MaxValue)
                    {
                        Waypoint supposedEnd = bestRoute.Path[bestRoute.Path.Count - 1];
                        if (supposedEnd != start)
                        {
                            for (int i = 0; i < bestRoute.Path.Count; i++)
                            {
                                bestRoute.Path[i].Discovered = true;
                            }
                            supposedEnd.Discovered = false;
                            current = end;
                            bestRoute.Path.Clear();
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        //move current to next node
                        current = minWeight;
                    }
                }
                #endregion

                bestRoute.Path.Add(start);

                //make start the 0 index
                bestRoute.FlipPath();

                return bestRoute;
            }

            //image not pixelated
            return new Course();
        }

        public Course CleanRoute(Course route)
        {
            //new course to be made
            Course bestRoute = new Course();
            //current node path
            List<Waypoint> nodeQue = route.Path;
            //index of the node to jump to
            int jump = 0;


            for (int i = 0; i < nodeQue.Count; i++)
            {//move through the entire que

                //will never jump to 0th index
                jump = 0;
                for (int j = i + 1; j < nodeQue.Count; j++)
                {//move through the qeu to compare to the neighbors of i
                    for (int k = 0; k < nodeQue[i].Neighbors.Count; k++)
                    {
                        if (nodeQue[i].Neighbors[k] == nodeQue[j])
                        {//found neighbor in que keep index
                            jump = j;
                        }
                    }
                        
                }
                if (jump < 1)
                {//no jump was found
                    bestRoute.Path.Add(nodeQue[i]);
                }
                else
                {//add current node and the node to jump to to cut out the uneeded sections of the path
                    bestRoute.Path.Add(nodeQue[i]);
                    bestRoute.Path.Add(nodeQue[jump]);
                    i = jump - 1;
                }
            }
            
            //new route without unwanted switchbacks
            return bestRoute;
        }

        public List<Waypoint> DeleteSection(List<Waypoint> route, Waypoint startOfIncision, Waypoint endOfIncision)
        {
            List<Waypoint> newRoute = new List<Waypoint>();
            bool keep = true;

            for (int i = 0; i < route.Count; i++)
            {
                if (route[i] == endOfIncision)
                    keep = true;

                if (keep)
                    newRoute.Add(route[i]);

                if (route[i] == startOfIncision)
                    keep = false;
            }

            return newRoute;
        }

    }
}

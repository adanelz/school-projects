﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PathPlanningAI
{
    class Course
    {
        
        public List<Waypoint> Path { get; set; }
        public int Weight { get; set; }
        protected Waypoint start;
        protected Waypoint end;

        public Waypoint Start
        {
            get
            {
                return Path[0];
            }
            set
            {
                start = value;
            }
        }
        public Waypoint End
        {
            get
            {
                if (Path.Count > 0)
                    return Path[Path.Count - 1];
                else
                    return Path[0];
            }
            set
            {
                end = value;
            }
        }

        public Course()
        {
            this.Start = new Waypoint();
            this.End = new Waypoint();
            this.Path = new List<Waypoint>();
            this.Weight = int.MaxValue;
        }

        /// <summary>
        /// Switches the course so that the start is at the 0 index
        /// </summary>
        /// <returns> the flipped path</returns>
        public Course FlipPath()
        {
            Course flipped = new Course();
            for (int i = this.Path.Count - 1; i >= 0; i--)
            {
                flipped.Path.Add(this.Path[i]);
            }

            return flipped;
        }
    }
}

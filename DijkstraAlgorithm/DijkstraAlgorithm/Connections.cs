﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DijkstraAlgorithm
{
    class Connections
    {
        protected int value = 0;
        protected bool connected = false;
        protected bool visible = false;

        protected Router head = null;
        protected Router tail = null;


        public int Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
            }
        }

        public Router Head
        {
            get
            {
                return head;
            }
            set
            {
                head = value;
            }
        }

        public Router Tail
        {
            get
            {
                return tail;
            }
            set
            {
                tail = value;
            }
        }

        public bool Connected
        {
            get
            {
                return connected;
            }
            set
            {
                connected = value;
            }
        }

        public bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
            }
        }
    }
}

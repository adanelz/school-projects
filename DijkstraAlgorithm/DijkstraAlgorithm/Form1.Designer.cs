﻿namespace DijkstraAlgorithm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NUDProbOfInit = new System.Windows.Forms.NumericUpDown();
            this.NUDProbOfNew = new System.Windows.Forms.NumericUpDown();
            this.NUDProbOfFail = new System.Windows.Forms.NumericUpDown();
            this.NUDProbOfSpeed = new System.Windows.Forms.NumericUpDown();
            this.NUDSPeedOfSim = new System.Windows.Forms.NumericUpDown();
            this.NUDnumOfRout = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbOfInit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbOfNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbOfFail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbOfSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDSPeedOfSim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDnumOfRout)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NUDProbOfInit);
            this.groupBox1.Controls.Add(this.NUDProbOfNew);
            this.groupBox1.Controls.Add(this.NUDProbOfFail);
            this.groupBox1.Controls.Add(this.NUDProbOfSpeed);
            this.groupBox1.Controls.Add(this.NUDSPeedOfSim);
            this.groupBox1.Controls.Add(this.NUDnumOfRout);
            this.groupBox1.Location = new System.Drawing.Point(12, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 392);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 353);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "label8";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 340);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "label7";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 314);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(188, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "&Start Simulation";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 272);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Speed of Simulation (ms):";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Probability of Speed Change:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Probability of Failed Connection:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Probability of New Connection:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Pobability of Initial Connection:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Number of Routers:";
            // 
            // NUDProbOfInit
            // 
            this.NUDProbOfInit.DecimalPlaces = 2;
            this.NUDProbOfInit.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.NUDProbOfInit.Location = new System.Drawing.Point(6, 88);
            this.NUDProbOfInit.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            131072});
            this.NUDProbOfInit.Name = "NUDProbOfInit";
            this.NUDProbOfInit.Size = new System.Drawing.Size(188, 20);
            this.NUDProbOfInit.TabIndex = 5;
            this.NUDProbOfInit.Value = new decimal(new int[] {
            4,
            0,
            0,
            65536});
            // 
            // NUDProbOfNew
            // 
            this.NUDProbOfNew.DecimalPlaces = 2;
            this.NUDProbOfNew.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.NUDProbOfNew.Location = new System.Drawing.Point(6, 138);
            this.NUDProbOfNew.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            131072});
            this.NUDProbOfNew.Name = "NUDProbOfNew";
            this.NUDProbOfNew.Size = new System.Drawing.Size(188, 20);
            this.NUDProbOfNew.TabIndex = 4;
            this.NUDProbOfNew.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // NUDProbOfFail
            // 
            this.NUDProbOfFail.DecimalPlaces = 2;
            this.NUDProbOfFail.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.NUDProbOfFail.Location = new System.Drawing.Point(6, 188);
            this.NUDProbOfFail.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            131072});
            this.NUDProbOfFail.Name = "NUDProbOfFail";
            this.NUDProbOfFail.Size = new System.Drawing.Size(188, 20);
            this.NUDProbOfFail.TabIndex = 3;
            this.NUDProbOfFail.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // NUDProbOfSpeed
            // 
            this.NUDProbOfSpeed.DecimalPlaces = 2;
            this.NUDProbOfSpeed.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.NUDProbOfSpeed.Location = new System.Drawing.Point(6, 238);
            this.NUDProbOfSpeed.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            131072});
            this.NUDProbOfSpeed.Name = "NUDProbOfSpeed";
            this.NUDProbOfSpeed.Size = new System.Drawing.Size(188, 20);
            this.NUDProbOfSpeed.TabIndex = 2;
            this.NUDProbOfSpeed.Value = new decimal(new int[] {
            3,
            0,
            0,
            65536});
            // 
            // NUDSPeedOfSim
            // 
            this.NUDSPeedOfSim.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.NUDSPeedOfSim.Location = new System.Drawing.Point(6, 288);
            this.NUDSPeedOfSim.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.NUDSPeedOfSim.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.NUDSPeedOfSim.Name = "NUDSPeedOfSim";
            this.NUDSPeedOfSim.Size = new System.Drawing.Size(188, 20);
            this.NUDSPeedOfSim.TabIndex = 1;
            this.NUDSPeedOfSim.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // NUDnumOfRout
            // 
            this.NUDnumOfRout.Location = new System.Drawing.Point(6, 38);
            this.NUDnumOfRout.Name = "NUDnumOfRout";
            this.NUDnumOfRout.Size = new System.Drawing.Size(188, 20);
            this.NUDnumOfRout.TabIndex = 0;
            this.NUDnumOfRout.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(915, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.newToolStripMenuItem.Text = "&New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(98, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.Location = new System.Drawing.Point(6, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(569, 367);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseClick);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Location = new System.Drawing.Point(218, 27);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(581, 392);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(824, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "label9";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 431);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbOfInit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbOfNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbOfFail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDProbOfSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDSPeedOfSim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NUDnumOfRout)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown NUDProbOfInit;
        private System.Windows.Forms.NumericUpDown NUDProbOfNew;
        private System.Windows.Forms.NumericUpDown NUDProbOfFail;
        private System.Windows.Forms.NumericUpDown NUDProbOfSpeed;
        private System.Windows.Forms.NumericUpDown NUDSPeedOfSim;
        private System.Windows.Forms.NumericUpDown NUDnumOfRout;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Timer timer1;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DijkstraAlgorithm
{
    class Router
    {
        protected float x = 0;
        protected float y = 0;

        protected int value = 0;
        protected int index = 0;
        protected bool discovered = false;

        protected List<Router> conTo = new List<Router>();
        protected List<int> conWeight = new List<int>();

        public List<Router> ConnectedTo
        {
            get
            {
                return conTo;
            }
            set
            {
                conTo = value;
            }
        }

        public List<int> ConnectedWeight
        {
            get
            {
                return conWeight;
            }
            set
            {
                conWeight = value;
            }
        }

        public int Value
        {
            get
            {
                return value;
            }
            set
            {
                this.value = value;
            }
        }

        public int Index
        {
            get
            {
                return index;
            }
            set
            {
                index = value;
            }
        }

        public bool Discovered
        {
            get
            {
                return discovered;
            }
            set
            {
                discovered = value;
            }
        }

        public float X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }

        public float Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }
        
    }
}

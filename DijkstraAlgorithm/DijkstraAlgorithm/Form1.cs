﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

///OSPF + IS-IS
///use this for routing packets
///finds shortest path from one router to all other routers
///finds out how to send data from one computer to any other computer
namespace DijkstraAlgorithm
{
    public partial class Form1 : Form
    {
        //lists of connected edges and routers
        List<Router> routers = new List<Router>();
        List<Connections> edges = new List<Connections>();
        List<int> previous = new List<int>();

        //router that gets clicked on
        Router selected = new Router();
        
        //drawing
        float xBuffer = 0;
        float yBuffer = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MakeMap();
        }

        private void MakeMap()
        {
            Random r = new Random();

            //starts form scratch
            routers.Clear();
            edges.Clear();

            //making the routers
            for (int i = 0; i < NUDnumOfRout.Value; i++)
            {
                Router tmp = new Router();

                tmp.Value = int.MaxValue;
                for (int j = 0; j < NUDnumOfRout.Value; j++)
                {
                    tmp.ConnectedTo.Add(null);
                    tmp.ConnectedWeight.Add(int.MaxValue);
                }
                tmp.Discovered = false;
                tmp.X = r.Next(panel1.Width - 50);
                tmp.Y = r.Next(panel1.Height - 50);
                tmp.Index = i;
                routers.Add(tmp);

            }

            //making the connections
            for (int i = 0; i < routers.Count(); i++)
            {
                for (int j = 0; j < routers.Count(); j++)
                {                    
                    if ((i != j) && (r.NextDouble() < System.Convert.ToDouble(NUDProbOfInit.Value)))
                    {                        
                        Connections tmp = new Connections();
                        tmp.Visible = false;
                        tmp.Head = routers[i];
                        tmp.Tail = routers[j];
                        tmp.Value = r.Next(100);
                        routers[i].ConnectedTo[j] = routers[j];
                        routers[j].ConnectedTo[i] = routers[i];
                        routers[i].ConnectedWeight[j] = tmp.Value;
                        routers[j].ConnectedWeight[i] = tmp.Value;                      
                    }
                }                
            }
            DrawMap();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            DrawMap();
        }

        private void DrawMap()
        {
            // Create a bitmap for double buffering
            Bitmap bm = new Bitmap(panel1.Width, panel1.Height);
            Graphics g = Graphics.FromImage(bm);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            // Clear the screen 
            g.Clear(Color.Black);

            

            //uconnected edges
            for (int i = 0; i < routers.Count(); i++)
            {
                for (int j = 0; j < routers[i].ConnectedTo.Count; j++)
                {
                    if (routers[i].ConnectedTo[j] != null)
                    {
                        g.DrawLine(new Pen(Color.OrangeRed, 2), routers[i].X + 15, routers[i].Y + 15, routers[i].ConnectedTo[j].X + 15, routers[i].ConnectedTo[j].Y + 15);
                    }
                }
            }
            //for (int i = 0; i < previous.Count; i++)
            //{
            //    if (previous[i] != null)
            //    {
            //        g.DrawLine(new Pen(Color.Orange, 2), routers[i].X + 15, routers[i].Y + 15, routers[previous[i].Index].X + 15, routers[previous[i].Index].Y + 15);
            //    }
                
            //}

            //connected edges          
            for (int j = 0; j < edges.Count; j++)
            {
                if (edges[j] != null && edges[j].Connected)
                {
                    
                        g.DrawLine(new Pen(Color.Orange, 2), edges[j].Head.X + 15, edges[j].Head.Y + 15, edges[j].Tail.X + 15, edges[j].Tail.Y + 15);

                }
            }

            //routers
            for (int i = 0; i < routers.Count(); i++)
            {
                g.FillRectangle(new SolidBrush(Color.SaddleBrown), routers[i].X, routers[i].Y, 30, 30);
            }
                        
            // Draw to the panel
            Graphics g2 = panel1.CreateGraphics();
            g2.DrawImage(bm, 0, 0);

            // Clean up our resources
            g2.Dispose();
            g.Dispose();
            bm.Dispose();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {

            MakeMap();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {//when the a mouse button is depressed it runs this code
           
            selected = null;

            if (e.Button == MouseButtons.Left)
            {//gives me the router that cursor is over 

                if (e.X <= panel1.Width && e.Y <= panel1.Height)
                {
                    //finds the router that the user clicked on
                    for (int i = 0; i < routers.Count; i++)
                    {
                        if (((e.X > routers[i].X) && (e.X < routers[i].X + 30)) && ((e.Y > routers[i].Y) && (e.Y < routers[i].Y + 30)))
                        {
                            selected = routers[i];

                            //remembers how far from the corner the cursor clicked
                            xBuffer = (e.X - selected.X);
                            yBuffer = (e.Y - selected.Y);
                        }
                    } 
                }
            }

            Dijkstra(selected);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {//stuff happening while the cursur is moving

            //makes sure there is something selected
            if ((selected != null) && (e.X < panel1.Width) && (e.Y < panel1.Width))
            {
                selected.X = (e.X - xBuffer);
                selected.Y = (e.Y - yBuffer);
            }

            //draws just the pieces
            DrawMap();

        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            //stuff happening when the click button is released

            //make sure that i clicked a piece
            if (selected != null)
            {
               selected.X = e.X - xBuffer;
               selected.Y = e.Y - yBuffer;
            }

            //the panel is no loger being clicked and no piece should be selected
            selected = null;

            //draws just the pieces
            DrawMap();
        }

        private void Dijkstra(Router selected)
        {
            //makes a list to keep track of each router's real time weight
            List<int> weights = new List<int>();
            weights.Clear();
            for (int i = 0; i < routers.Count; i++)
            {
                weights.Add(int.MaxValue);
            }

            //makes a list to keep track of each router's real time previous router
            previous.Clear();
            edges.Clear();

            //sets all of the values back to there original values
            for (int i = 0; i < routers.Count; i++)
            {
                previous.Add(int.MaxValue);
            }
            for (int i = 0; i < routers.Count; i++)
            {
                routers[i].Discovered = false;
                routers[i].Value = int.MaxValue;
            }

/******************************************************************************/
            //calculates best path

            //bool finished = false;
            int index = 0;
            int prevRout = 0;

            //makes the root router discovered
            if (selected != null)
            {
                if (selected.Index != 0) routers[0].Discovered = false;
                selected.Value = 0;
                weights[selected.Index] = selected.Value;
                selected.Discovered = true;

                prevRout = selected.Index;

                for (int i = 0; i < selected.ConnectedTo.Count; i++)
                {
                    if (selected.ConnectedTo[i] != null) weights[selected.ConnectedTo[i].Index] = selected.ConnectedWeight[selected.ConnectedTo[i].Index];
                }

            }

            for (int finished = 0; finished < routers.Count; finished++)
            {
                //temporary variable
                int weight = int.MaxValue;

                for (int i = 0; i < weights.Count; i++)
                {
                    if (weights[i] < weight && routers[i].Discovered != true)
                    {
                        weight = weights[i];
                        index = i;
                    }
                }

                routers[index].Value = weight;
                routers[index].Discovered = true;
                previous[index] = routers[prevRout].Index;

                Connections tmp = new Connections();
                tmp.Head = routers[previous[index]];
                tmp.Tail = routers[index];
                tmp.Connected = true;
                edges.Add(tmp);
                
                
                prevRout = index;

                for (int i = 0; i < routers[index].ConnectedTo.Count; i++)
                {
                    if (routers[index].ConnectedTo[i] != null && routers[index].ConnectedTo[i].Discovered != true)
                    {
                        if (routers[index].ConnectedTo[i].Value < int.MaxValue)
                        {
                            if (routers[index].ConnectedWeight[routers[index].ConnectedTo[i].Index] < weights[routers[index].ConnectedTo[i].Index])
                            {
                                weights[routers[index].ConnectedTo[i].Index] = routers[index].ConnectedWeight[routers[index].ConnectedTo[i].Index] + routers[index].Value;
                            }
                        }
                        else
                        {
                            weights[routers[index].ConnectedTo[i].Index] = routers[index].ConnectedWeight[routers[index].ConnectedTo[i].Index] + routers[index].Value;
                        }
                    }
                }

                //for (int i = 0; i < routers.Count; i++)
                //{

                //}

            }

            DrawMap();

            label9.Text = "";
            label9.Text = selected.Index.ToString();
        
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Random r = new Random();
            int tmp = 0;

            //making the connections
            for (int i = 0; i < routers.Count(); i++)
            {
                for (int j = 0; j < routers.Count(); j++)
                {
                    //new connection
                    if ((i != j) && (r.NextDouble() < System.Convert.ToDouble(NUDProbOfNew.Value)))
                    {
                        if (routers[i].ConnectedTo[j] == null && routers[j].ConnectedTo[i] == null)
                        {
                            Connections tmp2 = new Connections();
                            tmp2.Visible = false;
                            tmp2.Head = routers[i];
                            tmp2.Tail = routers[j];
                            tmp2.Value = r.Next(100);
                            routers[i].ConnectedTo[j] = routers[j];
                            routers[j].ConnectedTo[i] = routers[i];
                            routers[i].ConnectedWeight[j] = tmp2.Value;
                            routers[j].ConnectedWeight[i] = tmp2.Value;
                        }
                    }

                    //failed connection
                    if ((i != j) && (r.NextDouble() < System.Convert.ToDouble(NUDProbOfFail.Value)))
                    {
                        tmp = r.Next(100);
                        routers[i].ConnectedTo[j] = null;
                        routers[j].ConnectedTo[i] = null;
                        routers[i].ConnectedWeight[j] = int.MaxValue;
                        routers[j].ConnectedWeight[i] = int.MaxValue;
                    }


                    //speed change
                    if ((i != j) && (r.NextDouble() < System.Convert.ToDouble(NUDProbOfSpeed.Value)))
                    {
                        tmp = r.Next(100);
                        routers[i].ConnectedTo[j] = routers[j];
                        routers[j].ConnectedTo[i] = routers[i];
                        routers[i].ConnectedWeight[j] = tmp;
                        routers[j].ConnectedWeight[i] = tmp;
                    }
                }
            }

            Dijkstra(selected);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Interval = System.Convert.ToInt32(NUDSPeedOfSim.Value);
            if (timer1.Enabled == false)
            {
                button1.Text = "&Stop Simulation";
                timer1.Enabled = true;
            }
            else
            {
                button1.Text = "&Start Simulation";
                timer1.Enabled = false;
            }
        }
    }
}

/* Author				: Jackie Guiry
 * Date					: Novemeber 1, 2011
 * Purpose				: To create Phase 10
 * Assignment			: Project
 * Note					: Header file for Player class
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include <iostream>
#include "CardsLaid.h"
#include "Card.h"
using namespace std;

class Player
{
	public:
		Player(int position, string name, int phase, bool skipped, Card hand[10], Card laid[40]);

		int GetPosition();

		string GetName();

		int GetPhase();
		void SetPhase(int value);

		bool GetSkipped();
		void SetSkipped(bool skip);

		Card GetHand();
		void SetHand(Card **hand);

		Card GetLaid();
		void SetLaid(Card **laid);
		
	protected:
		int position;
		string name;
		int phase;
		bool skipped;
		Card **hand;
		Card **laid;

};

#endif /*PLAYER_H_*/
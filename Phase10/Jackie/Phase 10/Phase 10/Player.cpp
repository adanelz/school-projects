/* Author				: Jackie Guiry
 * Date					: Novemeber 1, 2011
 * Purpose				: To create Phase 10
 * Assignment			: Project
 * Note					: Source file for Player class
 */

#include "Player.h"

Player::Player(int position, string name, int phase, bool skipped, Card hand[10], Card laid[40])
{
	this->position = position;
	this->name = name;
	this->phase = phase;
	this->skipped = skipped;
	this->hand = hand;
	this->laid = laid;
}

int Player::GetPosition()
{
	return position;
}

string Player::GetName()
{
	return name;	
}

int Player::GetPhase()
{
	return phase;
}

void Player::SetPhase(int value)
{
	this->phase = value;
}

bool Player::GetSkipped()
{
	return skipped;
}

void Player::SetSkipped(bool skip)
{
	this->skipped = skip;
}

Card Player::GetHand()
{
	return hand;
}

void Player::SetHand(Card **Hand)
{
	this->hand = hand;
}

Card Player::GetLaid()
{
	return GetLaid;
}

void Player::SetLaid(Card **GetLaid)
{
	this->GetLaid = GetLaid;
}
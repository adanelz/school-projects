/* Author				: Jackie Guiry
 * Date					: Novemeber 1, 2011
 * Purpose				: To create Phase 10
 * Assignment			: Project
 */

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include "dice.h"
#include "Player.h"
using namespace std;

void Start()
{
	
}

/**********************************************/

void Directions()
{
	string filename = "directions.txt";
	string directions;

	ifstream infile;

	infile.open(filename.c_str()); //Opens file

	while (!infile.eof()) //Reads file till the end
	{
		infile >> directions; //Inputs words
		if(directions == "-1") //Skips a line
		{
			cout << "\n";
		}
		else if(directions == "-2") //Skips two lines
		{
			cout << "\n\n";
		}
		else
		cout << directions << " "; //Outputs words and spaces between words
	} 

	infile.close(); // Closes File
}

/**********************************************/

void End()
{
	system("pause");
}

/**********************************************/

void StartGame(int choice)
{
	if(choice == 1)
	{
		Start();
	}
	else if(choice == 2)
	{
		Directions();
	}
	else if(choice == 3)
	{
		End();
	}
}

/**********************************************/

void DisplayHomeScreen()
{
	int choice = 0;

	StartGame(choice);
}

/**********************************************/

int main()
{
	DisplayHomeScreen();

	return 0;
}
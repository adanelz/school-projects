Phase 10 Rules Objective: -1
The objective is to complete all 10 phases before anyone else. -2

Phases: -1
1.)	2 sets of 3 -1
2.)	1 set of 3 + 1 run of 4 -1
3.)	1 set of 4 + 1 run of 4 -1
4.)	1 run of 7 -1
5.)	1 run of 8 -1
6.)	1 run of 9 -1
7.)	2 sets of 4 -1
8.)	7 cards of 1 color -1
9.)	1 set of 5 + 1 set of 2 -1
10.)	1 set of 5 + 1 set of 3 -2

The Cards: -1
A special deck is used for this game. The deck contains four reference cards, cards numbered "1" through, colors blue, red, yellow and green, four "skip" cards, and eight "Wild" cards. -1

Wilds: Wild card can be used in place of any number card or any color. Several wild cards may be used in completing a phase. Once a wild card is played in a phase it can't be replaced and used elsewhere. -1

Skips: Skip cards can be used to skip another players turn. Discard the skip in the discard pile and then specify which person you are playing the skip card on. Skip cards can only be played as a skip card and can't be used in a phase. -2

Phase 10 Rules : How To Play -1
Start by dealing 10 cards face down to each player. Once you recieve your cards you may look at them, be careful and hold them so only you can see them. Place the remaining cards in the center, this becomes the draw pile. Turn over the top card of the draw pile, place it by the discard pile, this becomes the discard pile. -1

When it is your turn draw one card from the draw pile, you can either take the top card on the draw pile or the top card from the discard pile. Add this card to your hand and end your turn by discarding a card. Every player starts at phase 1. -1

Phase 10 Rules : How to Hit You may hit by laying a card down on a phase already laid down. Make sure the card fits in that phase.You may also hit with a "Wild" card by laying it down on a phase that is already laid down. In order to make a hit, you must have already completed the phase you were on. -1

Phase 10 rules : Completing a Hand When completing a phase, players try to "go out" as soon as possible. After a player "goes out" the game goes on for one more round giving each player one more chance to complete his/her phase. Then the players who completed there phase move on to the next phase, while the other players try to make the same phase again in the next hand. -1

Phase 10 Rules : Scoring The winner and those who got rid of all there cards score zero points. The first player to complete phase ten is the winner. Only players with cards in their hands score points, no points are scored if the cards are already laid down, points are as follows: -2

5 points for each card numbered 1-9 -1
10 points for each card numbered 10-12 -1
15 points for each "Skip" card -1
25 points for each "Wild" card -1
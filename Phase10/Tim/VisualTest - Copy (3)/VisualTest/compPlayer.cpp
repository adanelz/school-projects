#include<iostream>
#include"randgen.h"
#include<cmath>
#include "stdafx.h"

using namespace std;
int pCards[3][10];
int pPhase = 1;
int drawCard = 7;
int disCard = rand() % 12 + 1;

void sortHand(int cards[10])
{//sorts an array of 10 integers
	int key, i;

    for (int j = 0; j < 10; j++)
    {
        key = cards[j];
        i = j - 1;
        while ((i >= 0) && (cards[i] > key))
        {
            cards[i + 1] = cards[i];
            i--;

        }
        cards[i + 1] = key;
    }
}

int calcLeastFitCard(int cards[10], int discard)
{//looks at an array of the hand and the extra card and calcuates the best card and returns the index of the worst card
	int minIndex = 0;
	int wild = 13;
	int skip = 14;

	//11th index is the card in qeuestion to add to the hand
	int cardFit[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int cardTmp[11] = {cards[0], cards[1], cards[2], cards[3], cards[4], cards[5], cards[6], cards[7], cards[8], cards[9], discard};

	int fit = 100;
	double prob = rand() % 100;//generates a pobable number
	prob = prob / 100;

	for(int j = 0; j < 11; j++)
	{//all cards below 10 get a better fitness
		if(cardTmp[j] < 10)
		{
			cardFit[j] = 2;
		}
		else
		{
			cardFit[j] = 1;
		}
	}

//get fitness's based on phase
//-----------------------------------
	if(pPhase == 1 || pPhase == 7 || pPhase == 9 || pPhase == 10)
	{//only sets

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(i != j && cardTmp[i] == cardTmp[j]) 
				{//change fitness for ever other like card found
					cardFit[i]++;
				}
			}
		}
	}
	else if(pPhase == 4 || pPhase == 5 || pPhase == 6)
	{//only runs
		int runLength = pPhase - 1; //length of the needed run

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(cardTmp[i] != cardTmp[j] && (cardTmp[j] > (cardTmp[i] - runLength) && cardTmp[j] < (cardTmp[i] + runLength))) 
				{//change fitness for ever card based on how many cards it can make a run with.
				 //each card gets a value based on how different in number it is from the other cards.
					cardFit[i] += (12 - (-1 * (cardTmp[i] - cardTmp[j])));
				}
				else if(i != j && cardTmp[i] == cardTmp[j]) 
				{//makes cards that have the same number have a very low fitness
					cardFit[i] -= 50;
				}
			}
		}
	}
	else if(pPhase == 2 || pPhase == 3)
	{//both
		int runLength = 4; //length of the needed run

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				//cards that are both in a set and in a run will have fitnesses closer to 0.
				//cards that are only a run or a set will have a fitness farther away from zero.
				if(cardTmp[i] != cardTmp[j] && (cardTmp[j] > (cardTmp[i] - runLength) && cardTmp[j] < (cardTmp[i] + runLength))) 
				{//change fitness for ever card based on how many cards it can make a run with.
				 //each card gets a value based on how different in number it is from the other cards.
					cardFit[i] += (12 - (-1 * (cardTmp[i] - cardTmp[j])));
				}
				else if(i != j && cardTmp[i] == cardTmp[j]) 
				{//makes cards that have the same number have a fitness farth away from zero 
					cardFit[i] -= 100;
					if(abs(cardFit[i]) > 400) cardFit[i] = 0;//keeps player form getting more then 5 of one number
				}
			}
		}
	}
	else if(pPhase == 8)
	{//color
		//store the fitness of each card in a complementary array
		//for(int i = 0; i < 11; i++)
		//{//compare each card to the other cards
		//	for(int j = 0; j < 11; j++)
		//	{
		//		if(i != j && cardTmp[i]->GetColor() == cardTmp[j]->GetColor()) 
		//		{//change fitness for ever other like card found
		//			cardFit[i]++;
		//		}
		//	}
		//}
	}

//set fitness of wild and skip
//-----------------------------------
	for(int i = 0; i < 11; i++)
	{//find the index
		//set wild fitness to max so they are never discarded
		if(cardTmp[i] == 13) cardFit[i] = 500;

		//skips are discarded first
		if(cardTmp[i] == 14) cardFit[i] = 0;
	}


//get lowest fit card
//-------------------------------------
	for(int i = 0; i < 11; i++)
	{//find the index
		if(abs(cardFit[i]) < fit) 
		{//absolute value is taken so that the cards closest to 0 are bad
			fit = cardFit[i];
			minIndex = i;
		}
	}

	/*if the card in question has the same fitness as the lowest fit card
	it is a 50% chance that the computer will chose to not pick up the card.
	also if the player is on the 8th phase and has 5 of each color this will
	randomly select which color is kept and which color is abandoned*/
	if(cardFit[10] == cardFit[minIndex] && prob < .5)
	{
		return 10;
	}

	return minIndex;
}

void compDraw(int player, int card)
{//takes an extra card and a hand to see if the card should be kept or not
	//sorts the players hand
	sortHand(pCards[player]); 

	//gets the fitness of the card that is in the discard pile
	int leastFit = calcLeastFitCard(pCards[player], card);
		
	if(leastFit < 10)
		pCards[player][leastFit] = card;
	else
	{
		//card = drawNewCard() //draws a card from the top of the deck 
		leastFit = calcLeastFitCard(pCards[player], card);//gets fitness with the new card
		if(leastFit < 10)//if the new card is fit then it is put in it's place
			pCards[player][leastFit] = card;
	}
}
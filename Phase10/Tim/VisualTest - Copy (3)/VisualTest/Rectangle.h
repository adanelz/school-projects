/**
 * @file
 * @author  Timothy Hayduk
 * @version 1.0
 *
 * @section DESCRIPTION
 *
 * This holds the parameters for a simple rectangle.
 */

#ifndef RECTANGLE_H_
#define RECTANGLE_H_

#include <iostream>
using namespace std;

class Rectangle {
	public:
		Rectangle(int topLeftX, int topLeftY, int bottomRightX, int bottomRightY);

		bool Contains(int x, int y);

	private:
		int topLeftX;
		int topLeftY;
		int bottomRightX;
		int bottomRightY;

};

#endif /* RECTANGLE_H_ */

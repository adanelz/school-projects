#include"Game.h"
#include "stdafx.h"
#include <iostream>
using namespace std;

Game::Game()
{
		deck = new Stack();
		discardPile = new Stack();
		playerTurn = new Player();
		for(int i = 0; i < 4; i++)
		{
			players[i] = new Player();
			scores[i] = -1;
		}
}

void Game::InitializePlayers()
{
	players[0] = new Player(0,"Human", 1, false, NULL, NULL);
	players[1] = new Player(1,"Jordan", 1, false, NULL, NULL);
	players[2] = new Player(2,"Kennedy", 1, false, NULL, NULL);
	players[3] = new Player(3,"Mosley", 1, false, NULL, NULL);
}

void Game::MakeDeck()
{
	int i;
	int j;

	//Make the deck
/******************************************/
	Stack *Deck = new Stack;
	//(Tell tim, 0 = blue, 1 = green, 2 = red, 3 = yellow)
	//The basic 1-12 cards done twice
	for (i = 0; i < 4; i++)
	{
		for (j = 1; j <= 12; j++)
		{
			Card *c = new Card(i, j);
			Deck->Push(c);
		}
	}

	for (i = 0; i < 4; i++)
	{
		for (j = 1; j <= 12; j++)
		{
			Card *c = new Card(i, j);
			Deck->Push(c);

		}
	}

	//One color of each wild, twice
	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 2; j++)
		{
			Card *c = new Card(i, 13);
			Deck->Push(c);
		}
	}


	//Blue skip cards 
	for (i = 0; i < 1; i++)
	{
		for (j = 0; j < 4; j++)
		{
			Card *c = new Card(i, 14);
			Deck->Push(c);
		}
	}

//shuffle
/*************************************************************/

	int cnt = 0;
	int randNum = 0;
	int randNum2 = 0;
	Card *holder[108];
	Card *temp;

	while(cnt < 108)
	{
		temp = Deck->Pop();
		holder[cnt] = temp;
		cnt++;
	}

	cnt = 0;
	while(cnt < 10000)
	{
		randNum = rand() % 108 + 0;
		randNum2 = rand() % 108 + 0;
		temp = holder[randNum];
		holder[randNum] = holder[randNum2];
		holder[randNum2] = temp;
		cnt++;
	}

	for(int i = 0; i < 108; i++)
	{
		deck->Push(holder[i]);
	}

}

void Game::Deal()
{
	Card **one = new Card*[10];
	Card **two = new Card*[10];
	Card **three = new Card*[10];
	Card **four = new Card*[10];
	for(int i = 0; i < 10; i++)
	{
		one[i] = deck->Pop();
		two[i] = deck->Pop();
		three[i] = deck->Pop();
		four[i] = deck->Pop();
	}

	players[0]->SetHand((Card**)one);
	players[1]->SetHand((Card**)two);
	players[2]->SetHand((Card**)three);
	players[3]->SetHand((Card**)four);
}

void Game::StartRound()
{
	Card *c = deck->Pop();
	discardPile->Push(c);
	playerTurn = players[0];
}

Player* Game::GetPlayer()
{
	return playerTurn;
}

Card* Game::GetTopOFDiscard()
{
	return discardPile->Top()->GetCard();
}

Card* Game::Draw() 
{
	if(deck->Count() > 0)
	{
		return deck->Pop();
	}

	return NULL;
}

Card* Game::PickUp()
{
	if(discardPile->Count() > 0)
	{	
		return discardPile->Pop();
	}

	return NULL;
}

void Game::SortHand(Card *hand[10])
{
	int i;
	Card *key;

    for (int j = 0; j < 10; j++)
    {
        key = hand[j];
        i = j - 1;
        while ((i >= 0) && (hand[i]->GetValue() > key->GetValue()))
        {
            hand[i + 1] = hand[i];
            i--;

        }
        hand[i + 1] = key;
    }
}

int Game::CheckPhase(Card *hand[10], Card *extraCard, int phase)
{

	return 0;
}

void Game::Discard(Card *c)
{
	discardPile->Push(c);
}

void Game::EndTurn()
{
	int i = playerTurn->GetPosition();
	if(i < 3)
		playerTurn = players[i + 1];
	else
		playerTurn = players[0];
}


int Game::CalcLeastFitCard(Card *cards[10], Card *suspect)
{//looks at an array of the hand and the extra card and calcuates the best card and returns the index of the worst card
	int pPhase = playerTurn->GetPhase();
	int minIndex = 0;
	int wild = 13;
	int skip = 14;

	//11th index is the card in qeuestion to add to the hand
	int cardFit[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int cardTmp[11];

	//get just the card values
	for(int i = 0; i < 10; i++)
	{
		cardTmp[i] = cards[i]->GetValue();
	}
	cardTmp[10] = suspect->GetValue();

	int fit = 100;
	double prob = rand() % 100;//generates a pobable number
	prob = prob / 100;

	for(int j = 0; j < 11; j++)
	{//all cards below 10 get a better fitness
		if(cardTmp[j] < 10)
		{
			cardFit[j] = 2;
		}
		else
		{
			cardFit[j] = 1;
		}
	}

//get fitness's based on phase
//-----------------------------------
	if(pPhase == 1 || pPhase == 7 || pPhase == 9 || pPhase == 10)
	{//only sets

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(i != j && cardTmp[i] == cardTmp[j]) 
				{//change fitness for ever other like card found
					cardFit[i]++;
				}
			}
		}
	}
	else if(pPhase == 4 || pPhase == 5 || pPhase == 6)
	{//only runs
		int runLength = pPhase - 1; //length of the needed run

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(cardTmp[i] != cardTmp[j] && (cardTmp[j] > (cardTmp[i] - runLength) && cardTmp[j] < (cardTmp[i] + runLength))) 
				{//change fitness for ever card based on how many cards it can make a run with.
				 //each card gets a value based on how different in number it is from the other cards.
					cardFit[i] += (12 - (-1 * (cardTmp[i] - cardTmp[j])));
				}
				else if(i != j && cardTmp[i] == cardTmp[j]) 
				{//makes cards that have the same number have a very low fitness
					cardFit[i] -= 50;
				}
			}
		}
	}
	else if(pPhase == 2 || pPhase == 3)
	{//both
		int runLength = 4; //length of the needed run

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				//cards that are both in a set and in a run will have fitnesses closer to 0.
				//cards that are only a run or a set will have a fitness farther away from zero.
				if(cardTmp[i] != cardTmp[j] && (cardTmp[j] > (cardTmp[i] - runLength) && cardTmp[j] < (cardTmp[i] + runLength))) 
				{//change fitness for ever card based on how many cards it can make a run with.
				 //each card gets a value based on how different in number it is from the other cards.
					cardFit[i] += (12 - (-1 * (cardTmp[i] - cardTmp[j])));
				}
				else if(i != j && cardTmp[i] == cardTmp[j]) 
				{//makes cards that have the same number have a fitness farth away from zero 
					cardFit[i] -= 100;
					if(abs(cardFit[i]) > 400) cardFit[i] = 0;//keeps player form getting more then 5 of one number
				}
			}
		}
	}
	else if(pPhase == 8)
	{//color
		Card *cardz[11];

		//make array that includes extra card
		for(int i = 0; i < 10; i++)
		{
			cardz[i] = cards[i];
		}
		cardz[11] = suspect;

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(i != j && cards[i]->GetColor() == cards[j]->GetColor()) 
				{//change fitness for ever other like card found
					cardFit[i]++;
				}
			}
		}
	}

//set fitness of wild and skip
//-----------------------------------
	for(int i = 0; i < 11; i++)
	{//find the index
		//set wild fitness to max so they are never discarded
		if(cardTmp[i] == 13) cardFit[i] = 500;

		//skips are discarded first
		if(cardTmp[i] == 14) cardFit[i] = 0;
	}


//get lowest fit card
//-------------------------------------
	for(int i = 0; i < 11; i++)
	{//find the index
		if(abs(cardFit[i]) < fit) 
		{//absolute value is taken so that the cards closest to 0 are bad
			fit = cardFit[i];
			minIndex = i;
		}
	}

	/*if the card in question has the same fitness as the lowest fit card
	it is a 50% chance that the computer will chose to not pick up the card.
	also if the player is on the 8th phase and has 5 of each color this will
	randomly select which color is kept and which color is abandoned*/
	if(cardFit[10] == cardFit[minIndex] && prob < .5)
	{
		return 10;
	}

	return minIndex;
}

void Game::ExecuteComputers()
{//takes an extra card and a hand to see if the card should be kept or not
	Card **hand = new Card*[10];
	Card **tmp = playerTurn->GetHand();
	Card *card;

	for(int i = 0; i < 10; i++)
	{
		hand[i] = tmp[i];
	}

	//sorts the players hand 
	this->SortHand(hand); 

	//gets the fitness of the card that is in the discard pile
	card = discardPile->Top()->GetCard();
	int leastFit = this->CalcLeastFitCard(hand, card);
		
	if(leastFit < 10)
	{
		card = discardPile->Pop();
		this->Discard(hand[leastFit]);//discard the leat fit card
		hand[leastFit] = card;//put the fit card in the hand
	}
	else
	{
		card = this->Draw(); //draws a card from the top of the deck 
		leastFit = CalcLeastFitCard(hand, card);//gets fitness with the new card
		if(leastFit < 10)//if the new card is fit then it is put in it's place
		{
			this->Discard(hand[leastFit]);//discard the leat fit card
			hand[leastFit] = card;//put the fit card in the hand
		}
		else
		{//discard the card that was drawn
			this->Discard(card);
		}
	}

	//sorts the players hand 
	this->SortHand(hand); 
}

void Game::SetScore(int player, int score)
{
	players[player]->SetScore(score);
}

int Game::GetScore(int player)
{
	return players[player]->GetScore();
}
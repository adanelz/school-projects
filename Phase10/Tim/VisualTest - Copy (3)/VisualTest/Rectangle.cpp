#include "stdafx.h"
#include "Rectangle.h"

Rectangle::Rectangle(int topLeftX, int topLeftY, int bottomRightX, int bottomRightY)
{
	this->topLeftX = topLeftX;
	this->topLeftY = topLeftY;
	this->bottomRightX = bottomRightX;
	this->bottomRightY = bottomRightY;
}

bool Rectangle::Contains(int x, int y)
{
	return(x >= this->topLeftX && x <= this->bottomRightX && y >= this->topLeftY  && y <= this->bottomRightY);
}
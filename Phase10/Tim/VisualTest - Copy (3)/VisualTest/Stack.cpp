/*
 * Stack.cpp
 *
 *  Created on: Sep 2, 2011
 *      Author: Justin Knox
 */

#include "Stack.h"
#include "stdafx.h"

Stack::Stack()
{
	top = NULL;
	count = 0;
}

void Stack::Push(Card *c)
{
	count++;
	top = new Node(c, top);
}

Card* Stack::Pop()
{
	count--;

	Node *current = top;

	if(current != NULL)
	{
		Card *c = current->GetCard();
		top = top->GetNext();
		delete current;
		return c;
	}
	return NULL;
}

int Stack::Count()
{
	return count;
}

Node* Stack::Top()
{
	return top;
}

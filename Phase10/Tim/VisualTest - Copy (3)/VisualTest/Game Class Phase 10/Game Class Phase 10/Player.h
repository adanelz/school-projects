/* Author				: Jackie Guiry
 * Date					: Novemeber 1, 2011
 * Purpose				: To create Phase 10
 * Assignment			: Project
 * Note					: Header file for Player class
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include <iostream>
#include "Card.h"
using namespace std;

class Player
{
	public:
		Player();
		Player(int position, string name, int phase, bool skipped, Card **hand, Card **laid);

		int GetPosition();

		string GetName();

		int GetPhase();
		void SetPhase(int value);

		bool GetSkipped();
		void SetSkipped(bool skip);

		Card** GetHand();
		void SetHand(Card **hand);

		Card** GetLaid();
		void SetLaid(Card **laid);
		
		int GetScore();
		void SetScore(int value);
		
	protected:
		int position;
		string name;
		int phase;
		bool skipped;
		Card **hand;
		Card **laid;
		int score;

};

#endif /*PLAYER_H_*/
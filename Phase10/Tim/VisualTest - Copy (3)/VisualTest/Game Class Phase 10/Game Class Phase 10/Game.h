#ifndef GAME_H_
#define GAME_H_

#include"Card.h"
#include"Player.h"
#include"Stack.h"

class Game
{
	public:
		Game();

		void InitializePlayers();//Makes players with null hand and laid

		void MakeDeck();//makes a shuffled stack of cards
		//cleat deck()

		void Deal();//makes 4 hands from the deck and sets them to the players
		

		void StartRound();//puts the top card from the deck into the discard pile
							//and sets player[0] to the player whose turn it its variable
		Card* GetTopOFDiscard();//peeks at the top of the discard pile
		Player *GetPlayer();//returns the player whose turn it is

		Card *Draw();
		Card *PickUp();
		void SortHand(Card *hand[10]);
		int CheckPhase(Card *hand[10], Card *extraCard, int phase);
		void Discard(Card *c);
		void EndTurn();

		void ExecuteComputers();
		int CalcLeastFitCard(Card *cards[10], Card *discard);


		int GetScore(int player);
		void SetScore(int player, int score);

	protected:
		Stack *deck;
		Stack *discardPile;
		Player *players[4];
		Player *playerTurn;
		int scores[4];

};

#endif /*GAME_H_*/
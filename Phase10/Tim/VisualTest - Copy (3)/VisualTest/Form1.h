#pragma once
#include "DynamicArray.h"
#include "Instructions.h"
#include "Game.h"
#include "Node.h"

namespace VisualTest {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	private: System::Windows::Forms::ToolStripMenuItem^  startGameToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  instructionsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  qUITToolStripMenuItem;

	private: static int mWidth = 900;
	private: static int mHeight = 600;
	private: static int cardWidth = 43;
	private: static int cardHeight = 65;
	private: static int playerCardGap = 10;
	private: static int laidOutGap = -20;
	private: static int AICardGap = -20;
	private: static int drawIndex = 0;
	private: static int boundCounter = -1;
	private: static Game *mainProgram = new Game();// = Game::Game();
	//private: static DynamicArray *kik = new DynamicArray();
	//private: static int ::Rectangle *jij = new Rectangle(

	private: static bool PLAYER_INTERACT = true;
	private: static int AI_INDEX = 0;
	private: static bool PLAYER_DRAW = true;

	private: static bool p1LaidOut = false;
	private: static bool p2LaidOut = false;
	private: static bool p3LaidOut = false;
	private: static bool p4LaidOut = false;
	private: static bool current = false;

	private: array<System::Int32^>^ p1cards;
	private: array<System::Int32^>^ p2cards;
	private: array<System::Int32^>^ p3cards;
	private: array<System::Int32^>^ p4cards;
	private: array<System::Drawing::Rectangle^>^ p1cardBounds;

	private: array<System::Drawing::Bitmap^>^ cards;
	private: static int player1CardsX = (mWidth/2) - (cardWidth/2);
	private: static int player1CardsY = (mHeight - (mHeight/5)) - (cardHeight/2);
	private: static int player2CardsX = (mWidth/8) - (cardHeight/2);
	private: static int player2CardsY = (mHeight/2) - (cardWidth/2);
	private: static int player3CardsX = (mWidth/2) - (cardWidth/2);
	private: static int player3CardsY = (mHeight/7) - (cardHeight/2);
	private: static int player4CardsX = (mWidth - (mWidth/8)) - (cardHeight/2);
	private: static int player4CardsY = (mHeight/2) - (cardWidth/2);
    
	private: static int player1LaidOutY = player1CardsY - cardHeight;
	private: static int player2LaidOutX = player2CardsX + (cardWidth * 2);
	private: static int player3LaidOutY = player3CardsY + (cardHeight);
	private: static int player4LaidOutX = player4CardsX - cardWidth;
	

	private: static int discardNumber;
	private: static int player1CardNumber = 10;
 	private: static int player2CardNumber = 10;
 	private: static int player3CardNumber = 10;
 	private: static int player4CardNumber = 10;
	private: static Card **currentDrawingHand;
	private: static Card *currentCard;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Timer^  timer1;

	protected: 

	protected: 
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>



		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->startGameToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->instructionsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->qUITToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->menuStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// panel1
			// 
			this->panel1->Location = System::Drawing::Point(3, 23);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(900, 600);
			this->panel1->TabIndex = 0;
			this->panel1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &Form1::panel1_Paint);
			this->panel1->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Form1::panel1_MouseClick);
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {this->startGameToolStripMenuItem, 
				this->instructionsToolStripMenuItem, this->qUITToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(1000, 24);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			this->menuStrip1->ItemClicked += gcnew System::Windows::Forms::ToolStripItemClickedEventHandler(this, &Form1::menuStrip1_ItemClicked);
			// 
			// startGameToolStripMenuItem
			// 
			this->startGameToolStripMenuItem->Name = L"startGameToolStripMenuItem";
			this->startGameToolStripMenuItem->Size = System::Drawing::Size(77, 20);
			this->startGameToolStripMenuItem->Text = L"Start Game";
			// 
			// instructionsToolStripMenuItem
			// 
			this->instructionsToolStripMenuItem->Name = L"instructionsToolStripMenuItem";
			this->instructionsToolStripMenuItem->Size = System::Drawing::Size(81, 20);
			this->instructionsToolStripMenuItem->Text = L"Instructions";
			this->instructionsToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::instructionsToolStripMenuItem_Click);
			// 
			// qUITToolStripMenuItem
			// 
			this->qUITToolStripMenuItem->Name = L"qUITToolStripMenuItem";
			this->qUITToolStripMenuItem->Size = System::Drawing::Size(46, 20);
			this->qUITToolStripMenuItem->Text = L"QUIT";
			this->qUITToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::qUITToolStripMenuItem_Click);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(913, 38);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 34);
			this->button1->TabIndex = 2;
			this->button1->Text = L"Draw";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(913, 98);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 35);
			this->button2->TabIndex = 3;
			this->button2->Text = L"Take from Discard";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &Form1::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(913, 162);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(75, 37);
			this->button3->TabIndex = 4;
			this->button3->Text = L"Check Phase";
			this->button3->UseVisualStyleBackColor = true;
			// 
			// timer1
			// 
			this->timer1->Interval = 1000;
			this->timer1->Tick += gcnew System::EventHandler(this, &Form1::timer1_Tick);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1000, 600);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->menuStrip1);
			this->Name = L"Form1";
			this->Text = L"Phase 10";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
				
				 //(Tell tim, 0 = blue, 1 = green, 2 = red, 3 = yellow)
				cards = gcnew array<System::Drawing::Bitmap^>(60);
				 String^ temp = "";
				 for(int i = 0; i < 14; i++)
				 {
					temp = "blue" + (i + 1) + ".png";
					cards->SetValue(gcnew Bitmap(temp), i);
				 }
				 for(int i = 0; i < 14; i++)
				 {
					temp = "green" + (i + 1) + ".png";
					cards->SetValue(gcnew Bitmap(temp), i + 14);
				 }
				 for(int i = 0; i < 14; i++)
				 {
					temp = "red" + (i + 1) + ".png";
					cards->SetValue(gcnew Bitmap(temp), i + 28);
				 }
				 for(int i = 0; i < 14; i++)
				 {
					temp = "yellow" + (i + 1) + ".png";
					cards->SetValue(gcnew Bitmap(temp), i + 42);
				 }
				
				 for(int i = 0; i < 4; i++)
				 {
					temp = "back" + (i + 1) + ".png";
					cards->SetValue(gcnew Bitmap(temp), i + 56);
				 }

				p1cards = gcnew array<System::Int32^>(30);
				p2cards = gcnew array<System::Int32^>(30);
				p3cards = gcnew array<System::Int32^>(30);
				p4cards = gcnew array<System::Int32^>(30);
				for(int i = 0; i < 30; i++)
				{
					p1cards->SetValue(-1, i);
				}
				p1cards->SetValue(21, 0);
				p1cards->SetValue(36, 1);
				p1cards->SetValue(12, 2);
				p1cards->SetValue(43, 3);
				p1cards->SetValue(8, 4);
				p1cards->SetValue(39, 5);

				for(int i = 0; i < 10; i++)
				{
					p2cards->SetValue(56, i);
					p3cards->SetValue(57, i);
					p4cards->SetValue(58, i);
				}
				p1cardBounds = gcnew array<System::Drawing::Rectangle^>(50);
				Card *tempLol;
				mainProgram->InitializePlayers();
				mainProgram->MakeDeck();
				mainProgram->Deal();
				mainProgram->StartRound();
				//mainProgram->StartRound();
				tempLol = mainProgram->GetTopOFDiscard();
				discardNumber = tempLol->GetValue() + (tempLol->GetColor() * 14);
				p1LaidOut = true;
				//mainProgram = new Game();
				//timer1->Start();
			 }

	private: int cardStartPlayer(int cards, int startingPoint)
			 {
				//int temp = (cards * cardWidth) + (cards * 5);
				 int temp1 = ((cards - 1) * cardWidth)/2;
				 temp1 = temp1 + (((cards - 1) * playerCardGap)/2); 
				 int temp = (startingPoint - temp1);

				return temp;
			 }

	private: int cardStartAI(int cards, int startingPoint)
			 {
				//int temp = (cards * cardWidth) + (cards * 5);
				 int temp1 = ((cards - 1) * cardWidth)/2;
				 temp1 = temp1 + (((cards - 1) * AICardGap)/2); 
				 int temp = startingPoint - temp1;

				return temp;
			 }

 	private: int cardStartVert(int cards, int startingPoint)
			 {
				//int temp = (cards * cardWidth) + (cards * 5);
				 int temp1 = ((cards - 1) * cardHeight)/2;
				 temp1 = temp1 + (((cards - 1) * AICardGap)/2); 
				 int temp = startingPoint - temp1;

				return temp;
			 }

	private: void RoundOver()
			 {
				int p1, p2, p3, p4;
				int temp;
				//Card *temp;
				/**for(int yamato = 0; yamato < 4; yamato++)
				{
					currentDrawingHand = mainProgram->GetHand(yamato);
					for(int i = 0; i++; i <= 10)
					{
						if(currentDrawingHand[i] != NULL)
						{
							if(currentDrawingHand[i] > 1 && currentDrawingHand[i] < 9)
								p1 = p1 + 5;
							if(currentDrawingHand[i] > 10 && currentDrawingHand[i] < 12)
								//p1
								return;
						}
					}
				}*/

				mainProgram->SetScore(1, mainProgram->GetScore(1) + p1);
				mainProgram->SetScore(2, mainProgram->GetScore(2) + p2);
				mainProgram->SetScore(3, mainProgram->GetScore(3) + p3);
				mainProgram->SetScore(4, mainProgram->GetScore(4) + p4);
				p1LaidOut = false;
				p2LaidOut = false;
				p3LaidOut = false;
				p4LaidOut = false;
				//To do: Start a new round
			 }
	
	private: System::Void panel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
				if(drawIndex == 0)
				{
					int temp1 = 0;
					int counter = 0;
					Rectangle^ tempR;

					e->Graphics->DrawImage(gcnew Bitmap("background.jpg"), 0.0f, 0.0f);

					 Pen^ variableName = gcnew Pen(Color::SteelBlue);
					 SolidBrush^ goody = gcnew SolidBrush(Color::AntiqueWhite);

					for(int i = 0; i < 30; i++)
					{
						if(p1cards->GetValue(i) != -1)
						{
							counter++;
						}
					}
					temp1 = cardStartPlayer(10, mWidth/2);
					int currentCardIndex;
					currentDrawingHand = mainProgram->GetPlayer()->GetHand();
					Card *current;
					for(int i = 0; i < 10; i++)
					{
						//(System::Drawing::Rectangle^)
						//gcnew Rectangle(temp1 + (i * (cardWidth + playerCardGap)), player1CardsY, cardWidth, cardHeight)
						//e->Graphics->DrawImage((Bitmap^)cards->GetValue((int)p1cards->GetValue(i)), ((temp1 + ((cardWidth + 10) * i))), player1CardsY);	

						tempR = gcnew Rectangle(temp1 + (i * (cardWidth + playerCardGap)), player1CardsY, cardWidth, cardHeight);
						p1cardBounds->SetValue(tempR, i);
						current = currentDrawingHand[i];
						if(current != NULL)
						{
							currentCardIndex = current->GetValue() + (current->GetColor() * 14);
							e->Graphics->DrawImage((Bitmap^)cards->GetValue(currentCardIndex), temp1 + (i * (cardWidth + playerCardGap)), player1CardsY);
						}
					}
					if(this->current == true)
					{
						currentCardIndex = currentCard->GetValue() + (currentCard->GetColor() * 14);
					}
					if(p1LaidOut == true)
					{
						e->Graphics->DrawImage((Bitmap^)cards->GetValue(59), mWidth/2, player1LaidOutY);
						e->Graphics->DrawImage((Bitmap^)cards->GetValue(currentCardIndex), mWidth/2, player1CardsY + cardHeight);
					}

					//Player 2 Initialization
					currentDrawingHand = mainProgram->GetPlayer()->GetHand();
					temp1 = cardStartVert(10, mHeight/2);
					for(int i = 0; i < 10; i++)
					{
						current = currentDrawingHand[i];
						if(current != NULL)
						{
							e->Graphics->DrawImage((Bitmap^)cards->GetValue((int)p2cards->GetValue(i)), player2CardsX, temp1 + (i * (cardWidth + AICardGap)));	
						}
					}
					if(p2LaidOut == true)
					{
						e->Graphics->DrawImage((Bitmap^)cards->GetValue(59), player2LaidOutX, player2CardsY);
					}
					
					//Player 3 Initialization
					temp1 = cardStartAI(10, mWidth/2);
					for(int i = 0; i < 10; i++)
					{
						current = currentDrawingHand[i];
						if(current != NULL)
						{
							e->Graphics->DrawImage((Bitmap^)cards->GetValue((int)p3cards->GetValue(i)), temp1 + (i * (cardWidth + AICardGap)), player3CardsY);	
						}
					}
					if(p3LaidOut == true)
					{	
						e->Graphics->DrawImage((Bitmap^)cards->GetValue(59), mWidth/2, player3LaidOutY);
					}

					//Player 4 Initialization
					temp1 = cardStartVert(10, mHeight/2);
					for(int i = 0; i < 10; i++)
					{
						current = currentDrawingHand[i];
						if(current != NULL)
						{
							e->Graphics->DrawImage((Bitmap^)cards->GetValue((int)p4cards->GetValue(i)), player4CardsX, temp1 + (i * (cardWidth + AICardGap)));	
						}
					}
					if(p4LaidOut == true)
					{
						e->Graphics->DrawImage((Bitmap^)cards->GetValue(59), player4LaidOutX, player4CardsY);
					}
					
					//Discard pile and face-down card to represent deck und pile.
					e->Graphics->DrawImage((Bitmap^)cards->GetValue(discardNumber), (mWidth/2) - cardWidth, (mHeight/2) - cardHeight); //Middle Card
					e->Graphics->DrawImage((Bitmap^)cards->GetValue(59), (mWidth/2) + cardWidth, (mHeight/2) - cardHeight);
				}
				else
				{
					e->Graphics->Clear(Color::SpringGreen);
					drawIndex = 0;
				}
			 }
private: System::Void panel1_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			 //panel1->Refresh();

			if(PLAYER_INTERACT == false)
			{
				return;
			}
			if(PLAYER_DRAW == true)
			{
				return;
			}
			 Rectangle^ temp;
			 Card *current;
			 for(int i = 0; i < 10; i++)
			 {
				 temp = (System::Drawing::Rectangle^)p1cardBounds->GetValue(i);
				 if(temp->Contains(e->X, e->Y))
				 {
				    mainProgram->Discard(currentDrawingHand[i]);
					current = mainProgram->GetTopOFDiscard();
					discardNumber =  current->GetValue() + (current->GetColor() * 14);
					panel1->Refresh();
					AI_INDEX = 0;
					PLAYER_INTERACT = false;
					timer1->Start();
				 }
			 }

		 }
private: System::Void menuStrip1_ItemClicked(System::Object^  sender, System::Windows::Forms::ToolStripItemClickedEventArgs^  e) {
		 }
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) 
		 {
			 if(AI_INDEX < 3)
			 {
				if(AI_INDEX == 0)
					p2LaidOut = true;
				if(AI_INDEX == 1)
					p3LaidOut = true;
				if(AI_INDEX == 2)
					p4LaidOut = true;

				Card *current;
				mainProgram->EndTurn();
				mainProgram->ExecuteComputers();
				current = mainProgram->GetTopOFDiscard();
				discardNumber =  current->GetValue() + (current->GetColor() * 14);
				panel1->Refresh();
			 }
			 else
			 {
				mainProgram->EndTurn();
				PLAYER_INTERACT = true;
				PLAYER_DRAW = true;
				timer1->Stop();
			 }
			 AI_INDEX++;

		 }
private: System::Void instructionsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 Instructions i;
			 i.ShowDialog();
		 }
private: System::Void qUITToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 this->Close();
		 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(PLAYER_DRAW == true)
			 {
				 currentCard = mainProgram->Draw();
				 current = true;
				 PLAYER_DRAW = false;
				 panel1->Refresh();
			 }
		 }
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(PLAYER_DRAW == true)
			 {
				 if(true)
				 currentCard = mainProgram->PickUp();
				 current = true;
				 PLAYER_DRAW = false;
				 panel1->Refresh();
			 }
		 }
};
}


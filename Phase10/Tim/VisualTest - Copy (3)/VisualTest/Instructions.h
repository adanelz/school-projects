#pragma once

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;


namespace VisualTest {

	/// <summary>
	/// Summary for Instructions
	///
	/// WARNING: If you change the name of this class, you will need to change the
	///          'Resource File Name' property for the managed resource compiler tool
	///          associated with all .resx files this class depends on.  Otherwise,
	///          the designers will not be able to interact properly with localized
	///          resources associated with this form.
	/// </summary>
	public ref class Instructions : public System::Windows::Forms::Form
	{
	public:
		Instructions(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Instructions()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::TextBox^  textBox1;
	protected: 
	protected: static String^ instructions = "Phase 10 Rules Objective: \n The objective is to complete all 10 phases before anyone else. \nPhases: \n1.)	2 sets of 3 \n2.)	1 set of 3 + 1 run of 4 \n3.)	1 set of 4 + 1 run of 4 \n4.)	1 run of 7 \n5.)	1 run of 8 \n6.)	1 run of 9 \n7.)	2 sets of 4 \n8.)	7 cards of 1 color \n9.)	1 set of 5 + 1 set of 2 \n10.)	1 set of 5 + 1 set of 3 \n\nThe Cards: \nA special deck is used for this game. The deck contains four reference cards, cards numbered '1' through, colors blue, red, yellow and green, four 'skip' cards, and eight 'Wild' cards. \nWilds: Wild card can be used in place of any number card or any color. Several wild cards may be used in completing a phase. Once a wild card is played in a phase it can't be replaced and used elsewhere. \nSkips: Skip cards can be used to skip another players turn. Discard the skip in the discard pile and then specify which person you are playing the skip card on. Skip cards can only be played as a skip card and can't be used in a phase. \nPhase 10 Rules : How To Play \nStart by dealing 10 cards face down to each player. Once you recieve your cards you may look at them, be careful and hold them so only you can see them. Place the remaining cards in the center, this becomes the draw pile. Turn over the top card of the draw pile, place it by the discard pile, this becomes the discard pile. \nWhen it is your turn draw one card from the draw pile, you can either take the top card on the draw pile or the top card from the discard pile. Add this card to your hand and end your turn by discarding a card. Every player starts at phase 1. \nPhase 10 Rules : How to Hit You may hit by laying a card down on a phase already laid down. Make sure the card fits in that phase.You may also hit with a 'Wild' card by laying it down on a phase that is already laid down. In order to make a hit, you must have already completed the phase you were on. \nPhase 10 rules : Completing a Hand When completing a phase, players try to 'go out' as soon as possible. After a player 'goes out' the game goes on for one more round giving each player one more chance to complete his/her phase. Then the players who completed there phase move on to the next phase, while the other players try to make the same phase again in the next hand. \nPhase 10 Rules : Scoring The winner and those who got rid of all there cards score zero points. The first player to complete phase ten is the winner. Only players with cards in their hands score points, no points are scored if the cards are already laid down, points are as follows: \n\n5 points for each card numbered 1-9 \n10 points for each card numbered 10-12 \n15 points for each 'Skip' card \n25 points for each 'Wild' card \n";
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->SuspendLayout();
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(13, 13);
			this->textBox1->Multiline = true;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(500, 500);
			this->textBox1->TabIndex = 0;
			this->textBox1->Text = instructions;
			// 
			// Instructions
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(500, 500);
			this->Controls->Add(this->textBox1);
			this->Name = L"Instructions";
			this->Text = L"Instructions";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	};
}

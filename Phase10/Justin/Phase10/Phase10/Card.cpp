
#include "Card.h"
#include "Stack.h"
#include "Node.h"
#include <iostream>
using namespace std;

Card::Card()
{
	value = -1;
	color = -1;
}

Card::Card(int color, int value)
{
	this->value = value;
	this->color = color;
}

Card::~Card()
{

}

void Card::SetCard(int color, int value)
{
	this->value = value;
	this->color = color;

}
int Card::GetValue()
{

	return this->value; 
}

int Card::GetColor()
{
	return this->color;
}


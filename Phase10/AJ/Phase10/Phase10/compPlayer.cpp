#include<iostream>
#include"randgen.h"
#include<cmath>

using namespace std;
int pCards[3][10];
int pPhase = 1;
int drawCard = 7;
int disCard = rand() % 12 + 1;

void sortHand(int cards[10])
{//sorts an array of 10 integers
	int key, i;

    for (int j = 0; j < 10; j++)
    {
        key = cards[j];
        i = j - 1;
        while ((i >= 0) && (cards[i] > key))
        {
            cards[i + 1] = cards[i];
            i--;

        }
        cards[i + 1] = key;
    }
}

int calcLeastFitCard(int cards[10], int discard)
{//looks at an array of the hand and the extra card and calcuates the best card and returns the index of the worst card
	int minIndex = 0;
	int wild = 13;
	int skip = 14;

	//11th index is the card in qeuestion to add to the hand
	int cardFit[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int cardTmp[11] = {cards[0], cards[1], cards[2], cards[3], cards[4], cards[5], cards[6], cards[7], cards[8], cards[9], discard};

	int fit = 100;
	double prob = rand() % 100;//generates a pobable number
	prob = prob / 100;

	for(int j = 0; j < 11; j++)
	{//all cards below 10 get a better fitness
		if(cardTmp[j] < 10)
		{
			cardFit[j] = 2;
		}
		else
		{
			cardFit[j] = 1;
		}
	}

//get fitness's based on phase
//-----------------------------------
	if(pPhase == 1 || pPhase == 7 || pPhase == 9 || pPhase == 10)
	{//only sets

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(i != j && cardTmp[i] == cardTmp[j]) 
				{//change fitness for ever other like card found
					cardFit[i]++;
				}
			}
		}
	}
	else if(pPhase == 4 || pPhase == 5 || pPhase == 6)
	{//only runs
		int runLength = pPhase - 1; //length of the needed run

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(cardTmp[i] != cardTmp[j] && (cardTmp[j] > (cardTmp[i] - runLength) && cardTmp[j] < (cardTmp[i] + runLength))) 
				{//change fitness for ever card based on how many cards it can make a run with.
				 //each card gets a value based on how different in number it is from the other cards.
					cardFit[i] += (12 - (-1 * (cardTmp[i] - cardTmp[j])));
				}
				else if(i != j && cardTmp[i] == cardTmp[j]) 
				{//makes cards that have the same number have a very low fitness
					cardFit[i] -= 50;
				}
			}
		}
	}
	else if(pPhase == 2 || pPhase == 3)
	{//both
		int runLength = 4; //length of the needed run

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				//cards that are both in a set and in a run will have fitnesses closer to 0.
				//cards that are only a run or a set will have a fitness farther away from zero.
				if(cardTmp[i] != cardTmp[j] && (cardTmp[j] > (cardTmp[i] - runLength) && cardTmp[j] < (cardTmp[i] + runLength))) 
				{//change fitness for ever card based on how many cards it can make a run with.
				 //each card gets a value based on how different in number it is from the other cards.
					cardFit[i] += (12 - (-1 * (cardTmp[i] - cardTmp[j])));
				}
				else if(i != j && cardTmp[i] == cardTmp[j]) 
				{//makes cards that have the same number have a fitness farth away from zero 
					cardFit[i] -= 100;
					if(abs(cardFit[i]) > 400) cardFit[i] = 0;//keeps player form getting more then 5 of one number
				}
			}
		}
	}
	else if(pPhase == 8)
	{//color
		//store the fitness of each card in a complementary array
		//for(int i = 0; i < 11; i++)
		//{//compare each card to the other cards
		//	for(int j = 0; j < 11; j++)
		//	{
		//		if(i != j && cardTmp[i]->GetColor() == cardTmp[j]->GetColor()) 
		//		{//change fitness for ever other like card found
		//			cardFit[i]++;
		//		}
		//	}
		//}
	}

//set fitness of wild and skip
//-----------------------------------
	for(int i = 0; i < 11; i++)
	{//find the index
		//set wild fitness to max so they are never discarded
		if(cardTmp[i] == 13) cardFit[i] = 500;

		//skips are discarded first
		if(cardTmp[i] == 14) cardFit[i] = 0;
	}


//get lowest fit card
//-------------------------------------
	for(int i = 0; i < 11; i++)
	{//find the index
		if(abs(cardFit[i]) < fit) 
		{//absolute value is taken so that the cards closest to 0 are bad
			fit = cardFit[i];
			minIndex = i;
		}
	}

	/*if the card in question has the same fitness as the lowest fit card
	it is a 50% chance that the computer will chose to not pick up the card.
	also if the player is on the 8th phase and has 5 of each color this will
	randomly select which color is kept and which color is abandoned*/
	if(cardFit[10] == cardFit[minIndex] && prob < .5)
	{
		return 10;
	}

	return minIndex;
}

void compDraw(int player, int card)
{//takes an extra card and a hand to see if the card should be kept or not
	//sorts the players hand
	sortHand(pCards[player]); 

	//gets the fitness of the card that is in the discard pile
	int leastFit = calcLeastFitCard(pCards[player], card);
		
	if(leastFit < 10)
		pCards[player][leastFit] = card;
	else
	{
		//card = drawNewCard() //draws a card from the top of the deck 
		leastFit = calcLeastFitCard(pCards[player], card);//gets fitness with the new card
		if(leastFit < 10)//if the new card is fit then it is put in it's place
			pCards[player][leastFit] = card;
	}
}

int CheckPhase(int *hand, int phase)
{

	int cardMakePhase[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int tmp[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int numOfSCF = 0;//same cards
	int numOfSColF = 0;//same color
	int numOfCIR = 0;//cards in run
	int numOfSets = 0;
	int numOfWilds = 0;
	int numOfSpace = 0;
	int p = 0;//wild index pointer
	int tmpWildCount = 0;
	int wildsIndex[8] = {-1, -1, -1, -1, -1, -1, -1, -1};
	int n = 0;
	int set = 0;
	int set2 = 0;
	int run = 0;
	int color = 0;
	bool runFound = false;
	bool set1Found = false;
	bool set2Found = false;
	int oneSet[5] =  {-1, -1, -1, -1, -1};
	int twoSet[5] =  {-1, -1, -1, -1, -1};
	int set1Val = 0;//how many are in the set
	int set2Val = 0;

	for(int i = 0; i < 10; i++)
	{
		if(hand[i] == 13)
		{
			numOfWilds++;
			wildsIndex[n] = i;
			n++;
		}
	}

	n = 0;

/**/if(phase == 1 || phase == 7)/**************************************************/
	{//sets

		if(phase == 1)
		{
			set = 3;

		}
		else if(phase == 7)
 		{
			set = 4;
		}

		for(int i = 1; i <= 12; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 10; j++)
			{
				if(i == hand[j]) 
				{//change fitness for ever other like card found
					tmp[numOfSCF] = j;
					numOfSCF++;
				}
			}


			if(numOfSCF >= set)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				numOfSets++;
			}
			//gets the next two best posiblilities for sets
			else if(numOfWilds > 0)
			{//there are wilds
				if(numOfSCF > set2Val)
				{
					
					//move 1st set to 2nd set
					set2Val = set1Val;
					for(int j = 0; j < 5; j++)
					{
						if(j < set2Val)
							twoSet[j] = oneSet[j];
						else
							twoSet[j] = -1;
					}
					//set first set to next best set
					set1Val = numOfSCF;
					for(int j = 0; j < 5; j++)
					{
						if(j < set1Val)
							oneSet[j] = tmp[j];
						else
							oneSet[j] = -1;
					}
				}

			}
			else
			{
				for(int j = 0; j < 10; j++)
				{//set array that hold indexes to -1
					tmp[j] = -1;
				}
			}

			numOfSCF = 0;
		}

		if(numOfSets >= 2)
		{
			for(int j = 0; j < 10; j++)
			{
				cout << cardMakePhase[j] << endl;
			}
            return cardMakePhase[0];
		}
		else
		{
			if(numOfSets == 1)
			{
				int needed = set - set1Val;
				if(numOfWilds >= needed)
				{
					for(int j = 0; j < needed; j++)
					{//add wild to make the set
						oneSet[set1Val] = wildsIndex[j];
						set1Val++;
					}

					for(int j = 0; j < set; j++)
					{//add new set to the returned cards array
						cardMakePhase[n] = oneSet[j];
						n++;
					}

					return cardMakePhase[0];
				}

			}
			else
			{
				int needed1 = (set - set1Val);
				int needed2 = (set - set2Val);
				int p = 0;
				if(numOfWilds >= (needed1 + needed2))
				{
					for(int j = 0; j < needed1; j++)
					{//add wild to make the set
						oneSet[set1Val] = wildsIndex[p];
						set1Val++;
						p++;
					}

					for(int j = 0; j < needed2; j++)
					{//add wild to make the set
						twoSet[set2Val] = wildsIndex[p];
						set2Val++;
						p++;
					}

					for(int j = 0; j < set; j++)
					{//add new set to the returned cards array
						cardMakePhase[n] = oneSet[j];
						n++;
						cardMakePhase[n] = twoSet[j];
						n++;
					}

					return cardMakePhase[0];
				}
			}
            return -1;
		}	
	}
/**/else if(phase == 2 || phase == 3)/**************************************************/
	{//sets and runs
		p = 0;//wild index pointer
		tmpWildCount = numOfWilds;
		if(phase == 2)
		{
			set = 3;
			run = 4;
		}
		else
		{
			set = 4;
			run = 4;
		}

		//set
		for(int i = 1; i <= 12; i++)
		{//compare each card to the other cards
			if(set1Found) break;
			for(int j = 0; j < 10; j++)
			{
				if(i == hand[j]) 
				{//change fitness for ever other like card found
					tmp[numOfSCF] = j;
					numOfSCF++;
				}
			}

			if(numOfSCF >= set)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				set1Found = true;
			}
			//gets the next two best posiblilities for sets
			else if(numOfWilds > 0)
			{//there are wilds
				if(numOfSCF > set1Val)
				{//keeps the best set the hand has
					//set first set to next best set
					set1Val = numOfSCF;
					for(int j = 0; j < 5; j++)
					{
						if(j < set1Val)
							oneSet[j] = tmp[j];
						else
							oneSet[j] = -1;
					}
				}

			}
			else
			{
				for(int j = 0; j < 10; j++)
				{
					tmp[j] = -1;
				}

				numOfSCF = 0;
			}
		}

		//run
		tmp[numOfCIR] = 0;
		numOfCIR++;
		for(int i = 1; i < 10; i++)
		{
			/*system("cls");
			cout << "i: " << i << endl;
			for(int j = 0; j < 10; j++)
			{
				cout << tmp[j] << endl;
			}
			cout << "count: " << numOfCIR << endl;
			system("pause");*/
			if(hand[i] == (hand[i - 1] + 1) || (hand[i] == 13 && tmpWildCount > 0)) 
			{//change fitness for ever other like card found
				if(hand[i] == 13)
				{
					tmp[numOfCIR] = wildsIndex[p];
					numOfCIR++;
					p++;
					tmpWildCount--;
				}
				else
				{
					tmp[numOfCIR] = i;
					numOfCIR++;
				}
			}
			else
			{
				if(numOfCIR >= run)
				{
					for(int j = 0; j < 10; j++)
					{//adds the indexes of the cards that make the phase to an array
						if(tmp[j] >= 0)
						{
							cardMakePhase[n] = tmp[j];
							n++;
						}
					}
					runFound = true;
					break;
				}
				else if(hand[i] != hand[i - 1])
				{
					int needed = (hand[i] - hand[i - 1]) - 1;
					if(needed <= tmpWildCount)
					{//if there is a wild in the hand, use it
						for(int j = 0; j < needed; j++)
						{//add wilds
							tmp[numOfCIR] = wildsIndex[p];
							numOfCIR++;
							p++;
							tmpWildCount--;
						}
						//add the card in focus
						tmp[numOfCIR] = i;
						numOfCIR++;
					}
					else
					{//set everything back and start over
						for(int j = 0; j < 10; j++)
						{//sets tmp array back to -1s
							tmp[j] = -1;
						}
						tmpWildCount = numOfWilds;
						p = 0;
						tmp[0] = i;
						numOfCIR = 1;
					}
				}
			}
		}

		if(set1Found && runFound)
		{
             return cardMakePhase[0];
		}
		else
		{
			int needed = (set - set1Val);
			int p = 0;
			if(tmpWildCount >= needed)
			{
				for(int j = 0; j < needed; j++)
				{//add wild to make the set
					oneSet[set1Val] = wildsIndex[p];
					set1Val++;
					p++;
				}

				for(int j = 0; j < set; j++)
				{//add new set to the returned cards array
					cardMakePhase[n] = oneSet[j];
					n++;
				}

				return cardMakePhase[0];
			}

            return -1;
		}	
	}
/**/else if(phase == 4 || phase == 5 || phase == 6)/**************************************************/
	{//only runs
		p = 0;//wild index pointer
		tmpWildCount = numOfWilds;
		if(phase == 4)
		{
			run = 7;
		}
		else if(phase == 5)
		{
			run = 8;
		}
		else
		{
			run = 9;
		}

		//run
		tmp[numOfCIR] = 0;
		numOfCIR++;
		for(int i = 1; i < 10; i++)
		{
			system("cls");
			cout << "i: " << i << endl;
			for(int j = 0; j < 10; j++)
			{
				cout << tmp[j] << endl;
			}
			cout << "count: " << numOfCIR << endl;
			system("pause");
			if(hand[i] == (hand[i - 1] + 1) || (hand[i] == 13 && tmpWildCount > 0)) 
			{//change fitness for ever other like card found
				if(hand[i] == 13)
				{
					tmp[numOfCIR] = wildsIndex[p];
					numOfCIR++;
					p++;
					tmpWildCount--;
				}
				else
				{
					tmp[numOfCIR] = i;
					numOfCIR++;
				}
			}
			else
			{
				if(numOfCIR >= run)
				{
					for(int j = 0; j < 10; j++)
					{//adds the indexes of the cards that make the phase to an array
						if(tmp[j] >= 0)
						{
							cardMakePhase[n] = tmp[j];
							n++;
						}
					}
					runFound = true;
					break;
				}
				else if(hand[i] != hand[i - 1])
				{
					int needed = (hand[i] - hand[i - 1]) - 1;
					if(needed <= tmpWildCount)
					{//if there is a wild in the hand, use it
						for(int j = 0; j < needed; j++)
						{//add wilds
							tmp[numOfCIR] = wildsIndex[p];
							numOfCIR++;
							p++;
							tmpWildCount--;
						}
						//add the card in focus
						tmp[numOfCIR] = i;
						numOfCIR++;
					}
					else
					{//set everything back and start over
						for(int j = 0; j < 10; j++)
						{//sets tmp array back to -1s
							tmp[j] = -1;
						}
						tmpWildCount = numOfWilds;
						p = 0;
						tmp[0] = i;
						numOfCIR = 1;
					}
				}
			}
		}

		for(int j = 0; j < 10; j++)
		{
			cout << cardMakePhase[j] << endl;
		}

		if(runFound)
		{
             return cardMakePhase[0];
		}
		else
		{
             return -1;
		}
	}
	
/**/else if(phase == 8)/**************************************************/
	{//colors
		/*colors = 7;

		//set of colors
		for(int i = 1; i < 4; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 10; j++)
			{
				if(i == card[j]->GetColor()) 
				{//increas counter and save index
					tmp[numOfSColF] = j;
					numOfSColF++;
				}
			}
		
			if(numOfSColF >= colors)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				numOfSets++;
			}
			else
			{
				//if the number of the same cards found is less then the number needed. 
				//see if there is enough wilds in the hand to fill and then decrement
			}
			for(int j = 0; j < 10; j++)
			{//set array that hold indexes to -1
				tmp[j] = -1;
			}

			numOfSCF = 0;
		}

		if(numOfSets >= 2)
		{
             return cardMakePhase[0];
		}
		else
		{
             return -1;
		}	*/
             return -1;

	}
/**/else if (phase == 9 || phase == 10)/**************************************************/
	{//sets
		set = 5;
		if(phase == 9)
		{
			set2 = 2;
		}
		else if(phase == 10)
 		{
			set2 = 3;
		}

		for(int i = 1; i <= 12; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 10; j++)
			{
				if(i == hand[j]) 
				{//change fitness for ever other like card found
					tmp[numOfSCF] = j;
					numOfSCF++;
				}
			}

			if((numOfSCF >= set2 && numOfSCF < set) && !set2Found)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				set2Found = true;
			}
			else if(numOfSCF >= set && !set1Found)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				set1Found = true;
			}
			//gets the next two best posiblilities for sets
			else if(numOfWilds > 0)
			{//there are wilds
				if(numOfSCF > set2Val)
				{
					//move 1st set to 2nd set
					set2Val = set1Val;
					for(int j = 0; j < 5; j++)
					{
						if(j < set2Val)
							twoSet[j] = oneSet[j];
						else
							twoSet[j] = -1;
					}
					//set first set to next best set
					set1Val = numOfSCF;
					for(int j = 0; j < 5; j++)
					{
						if(j < set1Val)
							oneSet[j] = tmp[j];
						else
							oneSet[j] = -1;
					}
				}

			}
			else
			{
				for(int j = 0; j < 10; j++)
				{//set array that hold indexes to -1
					tmp[j] = -1;
				}
			}

			numOfSCF = 0;
		}


		if(set1Found && set2Found)
		{
             return cardMakePhase[0];
		}
		else
		{
			if(!set1Found && set2Found)
			{//small set was found but not the big set
				int needed = set - set1Val;
				if(numOfWilds >= needed)
				{
					for(int j = 0; j < needed; j++)
					{//add wild to make the set
						oneSet[set1Val] = wildsIndex[j];
						set1Val++;
					}

					for(int j = 0; j < set; j++)
					{//add new set to the returned cards array
						cardMakePhase[n] = oneSet[j];
						n++;
					}
					for(int j = 0; j < 10; j++)
					{
						cout << cardMakePhase[j] << endl;
					}

					return cardMakePhase[0];
				}

			}
			else if(set1Found && !set2Found)
			{//big set was found but not the small set
				int needed = set2 - set1Val;
				if(numOfWilds >= needed)
				{
					for(int j = 0; j < needed; j++)
					{//add wild to make the set
						oneSet[set1Val] = wildsIndex[j];
						set1Val++;
					}

					for(int j = 0; j < set; j++)
					{//add new set to the returned cards array
						cardMakePhase[n] = oneSet[j];
						n++;
					}
					
					for(int j = 0; j < 10; j++)
					{
						cout << cardMakePhase[j] << endl;
					}

					return cardMakePhase[0];
				}

			}
			else if(!set1Found && !set2Found)
			{//neither were found
				cout << set1Val << endl;
				cout << set2Val << endl;
				int needed1 = (set - set1Val);
				int needed2 = (set2 - set2Val);
				int p = 0;
				if(numOfWilds >= (needed1 + needed2))
				{
					for(int j = 0; j < needed1; j++)
					{//add wild to make the set
						oneSet[set1Val] = wildsIndex[p];
						set1Val++;
						p++;
					}

					for(int j = 0; j < needed2; j++)
					{//add wild to make the set
						twoSet[set2Val] = wildsIndex[p];
						set2Val++;
						p++;
					}

					for(int j = 0; j < set; j++)
					{//add new set to the returned cards array
						cardMakePhase[n] = oneSet[j];
						n++;
						cardMakePhase[n] = twoSet[j];
						n++;
					}
					
					for(int j = 0; j < 10; j++)
					{
						cout << cardMakePhase[j] << endl;
					}

					return cardMakePhase[0];
				}
			}

            return -1;
		}	
	}
}


int main() {
	
	int c[10] = {1, 1, 1, 1, 5, 6, 7, 8, 13, 13};


	cout << CheckPhase(c, 2) << endl; 
	



	/*int cardMakePhase[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int tmp[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int numOfSameCardFound = 0;
	int numOfSets = 0;
	int n = 0;

	//for(int j = 0; j < 10; j++)
	//{//give each player 10 random cards
	//	c[j] = rand() % 12 + 1;
	//}
	sortHand(c);

	//for(int i = 1; i <= 12; i++)
	//{//compare each card to the other cards
		for(int j = 0; j < 9; j++)
		{
			if(c[j+1] == (c[j] + 1)) 
			{//change fitness for ever other like card found
				tmp[numOfSameCardFound] = j;
				numOfSameCardFound++;
			}
			else
			{
				for(int j = 0; j < 10; j++)
				{
					tmp[j] = -1;
				}

				numOfSameCardFound = 0;
			}
		}

		

	//}



	cout << numOfSets << endl;

	for(int j = 0; j < 10; j++)
	{
		cout << c[j] << ", ";
	}
	cout << endl;
	for(int j = 0; j < 10; j++)
	{
		cout << cardMakePhase[j] << ", ";
	}
	cout << endl;*/


	////sim game
	//int count = 0;
	//int discard = 0;

	////make hands
	//for(int i = 0; i < 3; i++)
	//{//each player
	//	for(int j = 0; j < 10; j++)
	//	{//give each player 10 random cards
	//		pCards[i][j] = rand() % 12 + 1;
	//	}
	//}
	//	cout << "Phase: " << pPhase << endl;
	//	cout << "_____________________________" << endl;

	//for(int i = 0; i < 30; i++)
	//{
	//	discard = rand() % 12 + 1;
	//	cout << "player: " << count + 1 << endl;
	//	cout << "hand: ";
	//	for(int j = 0; j < 10; j++)
	//	{
	//		cout << pCards[count][j] << ", ";
	//	}
	//	cout << endl << endl;

	//	compDraw(count, discard);
	//	count++;
	//	if(count > 2)
	//	{
	//		pPhase++;
	//		cout << "Phase: " << pPhase << endl;
	//		cout << "_____________________________" << endl; 
	//		count = 0;
	//system("pause");
	//	}
	//}

	system("pause");
	return 0;
}
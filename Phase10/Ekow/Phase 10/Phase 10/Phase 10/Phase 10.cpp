#include <iostream>
using namespace std;

int Phase = 9;

int CheckPhase(int hand[10], int phase)
{

	int cardMakePhase[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int tmp[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int numOfSCF = 0;//same cards
	int numOfSColF = 0;//same color
	int numOfCIR = 0;//cards in run
	int numOfSets = 0;
	int n = 0;
	int set = 0;
	int set2 = 0;
	int run = 0;
	int color = 0;
	bool runFound = false;
	bool set1Found = false;
	bool set2Found = false;

/**/if (phase == 1 || phase == 7)
	{//sets

		if(phase == 1)
		{
			set = 3;
		}
		else if(phase == 7)
 		{
			set = 3;
		}

		for(int i = 1; i <= 12; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 10; j++)
			{
				if(i == hand[j]) 
				{//change fitness for ever other like card found
				tmp[numOfSCF] = j;
				numOfSCF++;
				}
			}
		//if the number of the same cards found is less then the number needed. 
		//see if there is enough wilds in the hand to fill and then decrement
			if(numOfSCF >= set)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				numOfSets++;
			}
			else
			{
				for(int j = 0; j < 10; j++)
				{//set array that hold indexes to -1
					tmp[j] = -1;
				}
			}

			numOfSCF = 0;
		}

		if(numOfSets >= 2)
		{
             return cardMakePhase[0];
		}
		else
		{
             return -1;
		}	
	}
/**/else if(phase == 2 || phase == 3)
	{//sets and runs
		
		if(phase == 2)
		{
			set = 3;
			run = 4;
		}
		else
		{
			set = 4;
			run = 4;
		}

		//set
		for(int i = 1; i <= 12; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 10; j++)
			{
				if(i == hand[j]) 
				{//change fitness for ever other like card found
				tmp[numOfSCF] = j;
				numOfSCF++;
				}
			}
		//if the number of the same cards found is less then the number needed. 
		//see if there is enough wilds in the hand to fill and then decrement
			if(numOfSCF >= set)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				numOfSets++;
			}

			for(int j = 0; j < 10; j++)
			{
				tmp[j] = -1;
			}

			numOfSCF = 0;
		}

		//run
		numOfCIR = 0;
		for(int j = 0; j < 10; j++)
		{
			if(hand[j+1] == (hand[j] + 1)) 
			{//change fitness for ever other like card found
				tmp[numOfCIR] = j;
				numOfCIR++;
			}
			else
			{
				if(numOfCIR >= run)
				{
					for(int j = 0; j < 10; j++)
					{//adds the indexes of the cards that make the phase to an array
						if(tmp[j] >= 0)
						{
							cardMakePhase[n] = tmp[j];
							n++;
						}
					}
					runFound = true;
					break;
				}
				else if(hand[j] != (hand[j + 1]))
				{
					for(int j = 0; j < 10; j++)
					{
						tmp[j] = -1;
					}
				
					numOfCIR = 0;
				}
			}
		}

		if(numOfSets >= 1 && run)
		{
             return cardMakePhase[0];
		}
		else
		{
             return -1;
		}	
	}
/**/else if(phase == 4 || phase == 5 || phase == 6)
	{//only runs

		if(phase == 4)
		{
			run = 7;
		}
		else if(phase == 5)
		{
			run = 8;
		}
		else
		{
			run = 9;
		}

		//run
		for(int i = 0; i < 10; i++)
		{
			if(hand[i+1] == (hand[i] + 1)) 
			{//change fitness for ever other like card found
				tmp[numOfCIR] = i;
				numOfCIR++;
			}
			else
			{
				if(numOfCIR >= run)
				{
					for(int j = 0; j < 10; j++)
					{//adds the indexes of the cards that make the phase to an array
						if(tmp[j] >= 0)
						{
							cardMakePhase[n] = tmp[j];
							n++;
						}
					}
					runFound = true;
					break;
				}
				else if(hand[i] != (hand[i + 1]))
				{
					for(int j = 0; j < 10; j++)
					{//sets tmp array back to -1s
						tmp[j] = -1;
					}
				
					numOfCIR = 0;
				}
			}
		}

		if(numOfSets >= 1 && run)
		{
             return cardMakePhase[0];
		}
		else
		{
             return -1;
		}
	}
/**/else if(phase == 8)
	{//colors
		/*colors = 7;

		//set of colors
		for(int i = 1; i < 4; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 10; j++)
			{
				if(i == card[j]->GetColor()) 
				{//increas counter and save index
					tmp[numOfSColF] = j;
					numOfSColF++;
				}
			}
		
			if(numOfSColF >= colors)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				numOfSets++;
			}
			else
			{
				//if the number of the same cards found is less then the number needed. 
				//see if there is enough wilds in the hand to fill and then decrement
			}
			for(int j = 0; j < 10; j++)
			{//set array that hold indexes to -1
				tmp[j] = -1;
			}

			numOfSCF = 0;
		}

		if(numOfSets >= 2)
		{
             return cardMakePhase[0];
		}
		else
		{
             return -1;
		}	*/
             return -1;

	}
/**/else if (phase == 9 || phase == 10)
	{//sets
		set = 5;
		if(phase == 1)
		{
			set2 = 2;
		}
		else if(phase == 7)
 		{
			set2 = 3;
		}

		for(int i = 1; i <= 12; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 10; j++)
			{
				if(i == hand[j]) 
				{//change fitness for ever other like card found
				tmp[numOfSCF] = j;
				numOfSCF++;
				}
			}
			//if the number of the same cards found is less then the number needed. 
			//see if there is enough wilds in the hand to fill and then decrement
			if(numOfSCF >= set2 && numOfSCF < set && !set2Found)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				set2Found = true;
			}
			else if(numOfSCF >= set && !set1Found)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				set1Found = true;
			}
			else
			{
				for(int j = 0; j < 10; j++)
				{//set array that hold indexes to -1
					tmp[j] = -1;
				}
			}

			numOfSCF = 0;
		}

		if(set1Found && set2Found)
		{
             return cardMakePhase[0];
		}
		else
		{
             return -1;
		}	
	}
}


int main() {
	int c[10] = {1, 1, 1, 1, 1, 2, 2, 2, 9, 11};


	cout << CheckPhase(c, 1) << endl;
	
	return 0;
}
#include"Game.h"
#include <iostream>
using namespace std;

Game::Game()
{
		deck = new Stack();
		discardPile = new Stack();
		playerTurn = new Player();
		for(int i = 0; i < 4; i++)
		{
			players[i] = new Player();
			scores[i] = -1;
		}
}

void Game::InitializePlayers()
{
	players[0] = new Player(0,"Human", 1, false, NULL, NULL);
	players[1] = new Player(1,"Jordan", 1, false, NULL, NULL);
	players[2] = new Player(2,"Kennedy", 1, false, NULL, NULL);
	players[3] = new Player(3,"Mosley", 1, false, NULL, NULL);
}

void Game::MakeDeck()
{
	int i;
	int j;

	//Make the deck
/******************************************/
	Stack *Deck = new Stack;
	//(Tell tim, 0 = blue, 1 = green, 2 = red, 3 = yellow)
	//The basic 1-12 cards done twice
	for (i = 0; i < 4; i++)
	{
		for (j = 1; j <= 12; j++)
		{
			Card *c = new Card(i, j);
			Deck->Push(c);
		}
	}

	for (i = 0; i < 4; i++)
	{
		for (j = 1; j <= 12; j++)
		{
			Card *c = new Card(i, j);
			Deck->Push(c);

		}
	}

	//One color of each wild, twice
	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 2; j++)
		{
			Card *c = new Card(i, 13);
			Deck->Push(c);
		}
	}


	//Blue skip cards 
	for (i = 0; i < 1; i++)
	{
		for (j = 0; j < 4; j++)
		{
			Card *c = new Card(i, 14);
			Deck->Push(c);
		}
	}

//shuffle
/*************************************************************/

	int cnt = 0;
	int randNum = 0;
	int randNum2 = 0;
	Card *holder[108];
	Card *temp;

	while(cnt < 108)
	{
		temp = Deck->Pop();
		holder[cnt] = temp;
		cnt++;
	}

	cnt = 0;
	while(cnt < 10000)
	{
		randNum = rand() % 108 + 0;
		randNum2 = rand() % 108 + 0;
		temp = holder[randNum];
		holder[randNum] = holder[randNum2];
		holder[randNum2] = temp;
		cnt++;
	}

	for(int i = 0; i < 108; i++)
	{
		deck->Push(holder[i]);
	}

}

void Game::Deal()
{
	Card **one = new Card*[10];
	Card **two = new Card*[10];
	Card **three = new Card*[10];
	Card **four = new Card*[10];
	for(int i = 0; i < 10; i++)
	{
		one[i] = deck->Pop();
		two[i] = deck->Pop();
		three[i] = deck->Pop();
		four[i] = deck->Pop();
	}

	players[0]->SetHand((Card**)one);
	players[1]->SetHand((Card**)two);
	players[2]->SetHand((Card**)three);
	players[3]->SetHand((Card**)four);
}

void Game::StartRound()
{
	Card *c = deck->Pop();
	discardPile->Push(c);
	playerTurn = players[0];
}

Player* Game::GetPlayer()
{
	return playerTurn;
}

Card* Game::GetTopOFDiscard()
{
	return discardPile->Top()->GetCard();
}

Card* Game::Draw() 
{
	if(deck->Count() > 0)
	{
		return deck->Pop();
	}

	return NULL;
}

Card* Game::PickUp()
{
	if(discardPile->Count() > 0)
	{	
		return discardPile->Pop();
	}

	return NULL;
}

void Game::SortHand(Card *hand[10])
{
	int i;
	Card *key;

    for (int j = 0; j < 10; j++)
    {
		if(key != NULL)
		{
			key = hand[j];
			i = j - 1;
			while ((i >= 0) && (hand[i]->GetValue() > key->GetValue()))
			{
				hand[i + 1] = hand[i];
				i--;

			}
			hand[i + 1] = key;
		}
    }
}

int* Game::CheckPhase(Card *cards[10], Card *extraCard, int phase)
{
	int cardMakePhase[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int tmp[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int noPhase[10] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	int numOfSCF = 0;//same cards
	int numOfSColF = 0;//same color
	int numOfCIR = 0;//cards in run
	int numOfSets = 0;
	int numOfWilds = 0;
	int numOfSpace = 0;
	int p = 0;//wild index pointer
	int tmpWildCount = 0;
	int wildsIndex[8] = {-1, -1, -1, -1, -1, -1, -1, -1};
	int n = 0;
	int set = 0;
	int set2 = 0;
	int run = 0;
	int color = 0;
	bool runFound = false;
	bool set1Found = false;
	bool set2Found = false;
	int oneSet[5] =  {-1, -1, -1, -1, -1};
	int twoSet[5] =  {-1, -1, -1, -1, -1};
	int set1Val = 0;//how many are in the set
	int set2Val = 0;
	int hand[11] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
	for(int i = 0; i < 10; i++)
	{
		hand[i] = cards[i]->GetValue();
	}
	if(extraCard != NULL)
		hand[10] = extraCard->GetValue();

	for(int i = 0; i < 11; i++)
	{
		if(hand[i] == 13)
		{
			numOfWilds++;
			wildsIndex[n] = i;
			n++;
		}
	}

	n = 0;

/**/if(phase == 1 || phase == 7)/**************************************************/
	{//sets

		if(phase == 1)
		{
			set = 3;

		}
		else if(phase == 7)
 		{
			set = 4;
		}

		for(int i = 1; i <= 12; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(i == hand[j]) 
				{//change fitness for ever other like card found
					tmp[numOfSCF] = j;
					numOfSCF++;
				}
			}


			if(numOfSCF >= set)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				numOfSets++;
			}
			//gets the next two best posiblilities for sets
			else if(numOfWilds > 0)
			{//there are wilds
				if(numOfSCF > set2Val)
				{
					
					//move 1st set to 2nd set
					set2Val = set1Val;
					for(int j = 0; j < 5; j++)
					{
						if(j < set2Val)
							twoSet[j] = oneSet[j];
						else
							twoSet[j] = -1;
					}
					//set first set to next best set
					set1Val = numOfSCF;
					for(int j = 0; j < 5; j++)
					{
						if(j < set1Val)
							oneSet[j] = tmp[j];
						else
							oneSet[j] = -1;
					}
				}

			}
			else
			{
				for(int j = 0; j < 10; j++)
				{//set array that hold indexes to -1
					tmp[j] = -1;
				}
			}

			numOfSCF = 0;
		}

		if(numOfSets >= 2)
		{
            return cardMakePhase;
		}
		else
		{
			if(numOfSets == 1)
			{
				int needed = set - set1Val;
				if(numOfWilds >= needed)
				{
					for(int j = 0; j < needed; j++)
					{//add wild to make the set
						oneSet[set1Val] = wildsIndex[j];
						set1Val++;
					}

					for(int j = 0; j < set; j++)
					{//add new set to the returned cards array
						cardMakePhase[n] = oneSet[j];
						n++;
					}

					return cardMakePhase;
				}

			}
			else
			{
				int needed1 = (set - set1Val);
				int needed2 = (set - set2Val);
				int p = 0;
				if(numOfWilds >= (needed1 + needed2))
				{
					for(int j = 0; j < needed1; j++)
					{//add wild to make the set
						oneSet[set1Val] = wildsIndex[p];
						set1Val++;
						p++;
					}

					for(int j = 0; j < needed2; j++)
					{//add wild to make the set
						twoSet[set2Val] = wildsIndex[p];
						set2Val++;
						p++;
					}

					for(int j = 0; j < set; j++)
					{//add new set to the returned cards array
						cardMakePhase[n] = oneSet[j];
						n++;
						cardMakePhase[n] = twoSet[j];
						n++;
					}

					return cardMakePhase;
				}
			}
            return noPhase;
		}	
	}
/**/else if(phase == 2 || phase == 3)/**************************************************/
	{//sets and runs
		p = 0;//wild index pointer
		tmpWildCount = numOfWilds;
		if(phase == 2)
		{
			set = 3;
			run = 4;
		}
		else
		{
			set = 4;
			run = 4;
		}

		//set
		for(int i = 1; i <= 12; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 10; j++)
			{
				if(i == hand[j]) 
				{//change fitness for ever other like card found
					tmp[numOfSCF] = j;
					numOfSCF++;
				}
			}

			if(numOfSCF >= set)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				set1Found = true;
			}
			//gets the next two best posiblilities for sets
			else if(numOfWilds > 0)
			{//there are wilds
				if(numOfSCF > set1Val)
				{//keeps the best set the hand has
					//set first set to next best set
					set1Val = numOfSCF;
					for(int j = 0; j < 5; j++)
					{
						if(j < set1Val)
							oneSet[j] = tmp[j];
						else
							oneSet[j] = -1;
					}
				}

			}
			else
			{
				for(int j = 0; j < 10; j++)
				{
					tmp[j] = -1;
				}

				numOfSCF = 0;
			}
		}

		//run
		tmp[numOfCIR] = 0;
		numOfCIR++;
		for(int i = 1; i < 10; i++)
		{
			/*system("cls");
			cout << "i: " << i << endl;
			for(int j = 0; j < 10; j++)
			{
				cout << tmp[j] << endl;
			}
			cout << "count: " << numOfCIR << endl;
			system("pause");*/
			if(hand[i] == (hand[i - 1] + 1) || (hand[i] == 13 && tmpWildCount > 0)) 
			{//change fitness for ever other like card found
				if(hand[i] == 13)
				{
					tmp[numOfCIR] = wildsIndex[p];
					numOfCIR++;
					p++;
					tmpWildCount--;
				}
				else
				{
					tmp[numOfCIR] = i;
					numOfCIR++;
				}
			}
			else
			{
				if(numOfCIR >= run)
				{
					for(int j = 0; j < 10; j++)
					{//adds the indexes of the cards that make the phase to an array
						if(tmp[j] >= 0)
						{
							cardMakePhase[n] = tmp[j];
							n++;
						}
					}
					runFound = true;
					break;
				}
				else if(hand[i] != hand[i - 1])
				{
					int needed = (hand[i] - hand[i - 1]) - 1;
					if(needed <= tmpWildCount)
					{//if there is a wild in the hand, use it
						for(int j = 0; j < needed; j++)
						{//add wilds
							tmp[numOfCIR] = wildsIndex[p];
							numOfCIR++;
							p++;
							tmpWildCount--;
						}
						//add the card in focus
						tmp[numOfCIR] = i;
						numOfCIR++;
					}
					else
					{//set everything back and start over
						for(int j = 0; j < 10; j++)
						{//sets tmp array back to -1s
							tmp[j] = -1;
						}
						tmpWildCount = numOfWilds;
						p = 0;
						tmp[0] = i;
						numOfCIR = 1;
					}
				}
			}
		}

		if(set1Found && runFound)
		{
             return cardMakePhase;
		}
		else
		{
			int needed = (set - set1Val);
			int p = 0;
			if(tmpWildCount >= needed)
			{
				for(int j = 0; j < needed; j++)
				{//add wild to make the set
					oneSet[set1Val] = wildsIndex[p];
					set1Val++;
					p++;
				}

				for(int j = 0; j < set; j++)
				{//add new set to the returned cards array
					cardMakePhase[n] = oneSet[j];
					n++;
				}

				return cardMakePhase;
			}

            return noPhase;
		}	
	}
/**/else if(phase == 4 || phase == 5 || phase == 6)/**************************************************/
	{//only runs
		p = 0;//wild index pointer
		tmpWildCount = numOfWilds;
		if(phase == 4)
		{
			run = 7;
		}
		else if(phase == 5)
		{
			run = 8;
		}
		else
		{
			run = 9;
		}

		//run
		tmp[numOfCIR] = 0;
		numOfCIR++;
		for(int i = 1; i < 10; i++)
		{
			/*system("cls");
			cout << "i: " << i << endl;
			for(int j = 0; j < 10; j++)
			{
				cout << tmp[j] << endl;
			}
			cout << "count: " << numOfCIR << endl;
			system("pause");*/
			if(hand[i] == (hand[i - 1] + 1) || (hand[i] == 13 && tmpWildCount > 0)) 
			{//change fitness for ever other like card found
				if(hand[i] == 13)
				{
					tmp[numOfCIR] = wildsIndex[p];
					numOfCIR++;
					p++;
					tmpWildCount--;
				}
				else
				{
					tmp[numOfCIR] = i;
					numOfCIR++;
				}
			}
			else
			{
				if(numOfCIR >= run)
				{
					for(int j = 0; j < 10; j++)
					{//adds the indexes of the cards that make the phase to an array
						if(tmp[j] >= 0)
						{
							cardMakePhase[n] = tmp[j];
							n++;
						}
					}
					runFound = true;
					break;
				}
				else if(hand[i] != hand[i - 1])
				{
					int needed = (hand[i] - hand[i - 1]) - 1;
					if(needed <= tmpWildCount)
					{//if there is a wild in the hand, use it
						for(int j = 0; j < needed; j++)
						{//add wilds
							tmp[numOfCIR] = wildsIndex[p];
							numOfCIR++;
							p++;
							tmpWildCount--;
						}
						//add the card in focus
						tmp[numOfCIR] = i;
						numOfCIR++;
					}
					else
					{//set everything back and start over
						for(int j = 0; j < 10; j++)
						{//sets tmp array back to -1s
							tmp[j] = -1;
						}
						tmpWildCount = numOfWilds;
						p = 0;
						tmp[0] = i;
						numOfCIR = 1;
					}
				}
			}
		}

		/*for(int j = 0; j < 10; j++)
		{
			cout << cardMakePhase[j] << endl;
		}*/

		if(runFound)
		{
             return cardMakePhase;
		}
		else
		{
             return noPhase;
		}
	}
	/*
	//gets the next two best posiblilities for sets
					if(numOfWilds > 0)
					{//there are wilds
						if(numOfCIR > set2Val)
						{
							//move 1st set to 2nd set
							set2Val = set1Val;
							for(int j = 0; j < 5; j++)
							{
								if(j < set2Val)
									twoSet[j] = oneSet[j];
								else
									twoSet[j] = -1;
							}
							//set first set to next best set
							set1Val = numOfCIR;
							for(int j = 0; j < 5; j++)
							{
								if(j < set1Val)
									oneSet[j] = tmp[j];
								else
									oneSet[j] = -1;
							}
						}

					}
					cout << set1Val << endl;
					cout << set2Val << endl;
/**/else if(phase == 8)/**************************************************/
	{//colors
		/*colors = 7;

		//set of colors
		for(int i = 1; i < 4; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 10; j++)
			{
				if(i == card[j]->GetColor()) 
				{//increas counter and save index
					tmp[numOfSColF] = j;
					numOfSColF++;
				}
			}
		
			if(numOfSColF >= colors)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				numOfSets++;
			}
			else
			{
				//if the number of the same cards found is less then the number needed. 
				//see if there is enough wilds in the hand to fill and then decrement
			}
			for(int j = 0; j < 10; j++)
			{//set array that hold indexes to -1
				tmp[j] = -1;
			}

			numOfSCF = 0;
		}

		if(numOfSets >= 2)
		{
             return cardMakePhase[0];
		}
		else
		{
             return -1;
		}	*/
             return noPhase;

	}
/**/else if (phase == 9 || phase == 10)/**************************************************/
	{//sets
		set = 5;
		if(phase == 9)
		{
			set2 = 2;
		}
		else if(phase == 10)
 		{
			set2 = 3;
		}

		for(int i = 1; i <= 12; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 10; j++)
			{
				if(i == hand[j]) 
				{//change fitness for ever other like card found
					tmp[numOfSCF] = j;
					numOfSCF++;
				}
			}

			if((numOfSCF >= set2 && numOfSCF < set) && !set2Found)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				set2Found = true;
			}
			else if(numOfSCF >= set && !set1Found)
			{
				for(int j = 0; j < 10; j++)
				{
					if(tmp[j] >= 0)
					{//adds the indexes of the cards that make the phase to an array
						cardMakePhase[n] = tmp[j];
						n++;
					}
				}
				set1Found = true;
			}
			//gets the next two best posiblilities for sets
			else if(numOfWilds > 0)
			{//there are wilds
				if(numOfSCF > set2Val)
				{
					//move 1st set to 2nd set
					set2Val = set1Val;
					for(int j = 0; j < 5; j++)
					{
						if(j < set2Val)
							twoSet[j] = oneSet[j];
						else
							twoSet[j] = -1;
					}
					//set first set to next best set
					set1Val = numOfSCF;
					for(int j = 0; j < 5; j++)
					{
						if(j < set1Val)
							oneSet[j] = tmp[j];
						else
							oneSet[j] = -1;
					}
				}

			}
			else
			{
				for(int j = 0; j < 10; j++)
				{//set array that hold indexes to -1
					tmp[j] = -1;
				}
			}

			numOfSCF = 0;
		}


		if(set1Found && set2Found)
		{
             return cardMakePhase;
		}
		else
		{
			if(!set1Found && set2Found)
			{//small set was found but not the big set
				int needed = set - set1Val;
				if(numOfWilds >= needed)
				{
					for(int j = 0; j < needed; j++)
					{//add wild to make the set
						oneSet[set1Val] = wildsIndex[j];
						set1Val++;
					}

					for(int j = 0; j < set; j++)
					{//add new set to the returned cards array
						cardMakePhase[n] = oneSet[j];
						n++;
					}

					/*for(int j = 0; j < 10; j++)
					{
						cout << cardMakePhase[j] << endl;
					}*/

					return cardMakePhase;
				}

			}
			else if(set1Found && !set2Found)
			{//big set was found but not the small set
				int needed = set2 - set1Val;
				if(numOfWilds >= needed)
				{
					for(int j = 0; j < needed; j++)
					{//add wild to make the set
						oneSet[set1Val] = wildsIndex[j];
						set1Val++;
					}

					for(int j = 0; j < set; j++)
					{//add new set to the returned cards array
						cardMakePhase[n] = oneSet[j];
						n++;
					}
					
					/*for(int j = 0; j < 10; j++)
					{
						cout << cardMakePhase[j] << endl;
					}*/

					return cardMakePhase;
				}

			}
			else if(!set1Found && !set2Found)
			{//neither were found
				/*cout << set1Val << endl;
				cout << set2Val << endl;*/
				int needed1 = (set - set1Val);
				int needed2 = (set2 - set2Val);
				int p = 0;
				if(numOfWilds >= (needed1 + needed2))
				{
					for(int j = 0; j < needed1; j++)
					{//add wild to make the set
						oneSet[set1Val] = wildsIndex[p];
						set1Val++;
						p++;
					}

					for(int j = 0; j < needed2; j++)
					{//add wild to make the set
						twoSet[set2Val] = wildsIndex[p];
						set2Val++;
						p++;
					}

					for(int j = 0; j < set; j++)
					{//add new set to the returned cards array
						cardMakePhase[n] = oneSet[j];
						n++;
						cardMakePhase[n] = twoSet[j];
						n++;
					}
					
					/*for(int j = 0; j < 10; j++)
					{
						cout << cardMakePhase[j] << endl;
					}*/

					return cardMakePhase;
				}
			}

            return noPhase;
		}	
	}
}

void Game::Discard(Card *c)
{
	discardPile->Push(c);
}

void Game::EndTurn()
{
	int i = playerTurn->GetPosition();
	if(i < 3)
		playerTurn = players[i + 1];
	else
		playerTurn = players[0];
}


int Game::CalcLeastFitCard(Card *cards[10], Card *suspect)
{//looks at an array of the hand and the extra card and calcuates the best card and returns the index of the worst card
	int pPhase = playerTurn->GetPhase();
	int minIndex = 0;
	int wild = 13;
	int skip = 14;

	//11th index is the card in qeuestion to add to the hand
	int cardFit[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	int cardTmp[11];

	//get just the card values
	for(int i = 0; i < 10; i++)
	{
		cardTmp[i] = cards[i]->GetValue();
	}
	cardTmp[10] = suspect->GetValue();

	int fit = 100;
	double prob = rand() % 100;//generates a pobable number
	prob = prob / 100;

	for(int j = 0; j < 11; j++)
	{//all cards below 10 get a better fitness
		if(cardTmp[j] < 10)
		{
			cardFit[j] = 2;
		}
		else
		{
			cardFit[j] = 1;
		}
	}

//get fitness's based on phase
//-----------------------------------
	if(pPhase == 1 || pPhase == 7 || pPhase == 9 || pPhase == 10)
	{//only sets

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(i != j && cardTmp[i] == cardTmp[j]) 
				{//change fitness for ever other like card found
					cardFit[i]++;
				}
			}
		}
	}
	else if(pPhase == 4 || pPhase == 5 || pPhase == 6)
	{//only runs
		int runLength = pPhase - 1; //length of the needed run

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(cardTmp[i] != cardTmp[j] && (cardTmp[j] > (cardTmp[i] - runLength) && cardTmp[j] < (cardTmp[i] + runLength))) 
				{//change fitness for ever card based on how many cards it can make a run with.
				 //each card gets a value based on how different in number it is from the other cards.
					cardFit[i] += (12 - (-1 * (cardTmp[i] - cardTmp[j])));
				}
				else if(i != j && cardTmp[i] == cardTmp[j]) 
				{//makes cards that have the same number have a very low fitness
					cardFit[i] -= 50;
				}
			}
		}
	}
	else if(pPhase == 2 || pPhase == 3)
	{//both
		int runLength = 4; //length of the needed run

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				//cards that are both in a set and in a run will have fitnesses closer to 0.
				//cards that are only a run or a set will have a fitness farther away from zero.
				if(cardTmp[i] != cardTmp[j] && (cardTmp[j] > (cardTmp[i] - runLength) && cardTmp[j] < (cardTmp[i] + runLength))) 
				{//change fitness for ever card based on how many cards it can make a run with.
				 //each card gets a value based on how different in number it is from the other cards.
					cardFit[i] += (12 - (-1 * (cardTmp[i] - cardTmp[j])));
				}
				else if(i != j && cardTmp[i] == cardTmp[j]) 
				{//makes cards that have the same number have a fitness farth away from zero 
					cardFit[i] -= 100;
					if(abs(cardFit[i]) > 400) cardFit[i] = 0;//keeps player form getting more then 5 of one number
				}
			}
		}
	}
	else if(pPhase == 8)
	{//color
		Card *cardz[11];

		//make array that includes extra card
		for(int i = 0; i < 10; i++)
		{
			cardz[i] = cards[i];
		}
		cardz[11] = suspect;

		//store the fitness of each card in a complementary array
		for(int i = 0; i < 11; i++)
		{//compare each card to the other cards
			for(int j = 0; j < 11; j++)
			{
				if(i != j && cards[i]->GetColor() == cards[j]->GetColor()) 
				{//change fitness for ever other like card found
					cardFit[i]++;
				}
			}
		}
	}

//set fitness of wild and skip
//-----------------------------------
	for(int i = 0; i < 11; i++)
	{//find the index
		//set wild fitness to max so they are never discarded
		if(cardTmp[i] == 13) cardFit[i] = 500;

		//skips are discarded first
		if(cardTmp[i] == 14) cardFit[i] = 0;
	}


//get lowest fit card
//-------------------------------------
	for(int i = 0; i < 11; i++)
	{//find the index
		if(abs(cardFit[i]) < fit) 
		{//absolute value is taken so that the cards closest to 0 are bad
			fit = cardFit[i];
			minIndex = i;
		}
	}

	/*if the card in question has the same fitness as the lowest fit card
	it is a 50% chance that the computer will chose to not pick up the card.
	also if the player is on the 8th phase and has 5 of each color this will
	randomly select which color is kept and which color is abandoned*/
	if(cardFit[10] == cardFit[minIndex] && prob < .5)
	{
		return 10;
	}

	return minIndex;
}

void Game::ExecuteComputers()
{//takes an extra card and a hand to see if the card should be kept or not
	Card **hand = new Card*[10];
	Card **tmp = playerTurn->GetHand();
	Card *card;
	int *phasedCards;

	for(int i = 0; i < 10; i++)
	{
		hand[i] = tmp[i];
	}

	//sorts the players hand 
	this->SortHand(hand); 

	//gets the fitness of the card that is in the discard pile
	card = discardPile->Top()->GetCard();
	int leastFit = this->CalcLeastFitCard(hand, card);
		
	if(leastFit < 10)
	{
		card = discardPile->Pop();
		this->Discard(hand[leastFit]);//discard the leat fit card
		hand[leastFit] = card;//put the fit card in the hand
	}
	else
	{
		card = this->Draw(); //draws a card from the top of the deck 
		leastFit = CalcLeastFitCard(hand, card);//gets fitness with the new card
		if(leastFit < 10)//if the new card is fit then it is put in it's place
		{
			this->Discard(hand[leastFit]);//discard the leat fit card
			hand[leastFit] = card;//put the fit card in the hand
		}
		else
		{//discard the card that was drawn
			this->Discard(card);
		}
	}

	//sorts the players hand 
	this->SortHand(hand); 
	phasedCards = this->CheckPhase(hand, NULL, playerTurn->GetPhase());

	for(int i = 0; i < 10; i++)
	{
		tmp[i] = NULL;
	}
	int n = 0;
	while(phasedCards[n] != -1)
	{
		tmp[n] = hand[phasedCards[n]];
		hand[phasedCards[n]] = NULL;
		n++;
	}
	
	playerTurn->SetLaid(tmp);

	this->SortHand(hand); 
	playerTurn->SetHand(hand);


	
}

void Game::SetScore(int player, int score)
{
	players[player]->SetScore(score);
}

int Game::GetScore(int player)
{
	return players[player]->GetScore();
}
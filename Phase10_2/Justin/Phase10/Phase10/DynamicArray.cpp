#include "DynamicArray.h"

DynamicArray::DynamicArray()
{
	length = 0;
	capacity = 108;
	array = new Card[capacity];
}

DynamicArray::~DynamicArray()
{
	delete [] array;
}

void	DynamicArray::Add(int value, int color)
{
	if(GetLength() >= capacity)
	{
		// Create a new temporary array twice the size
		capacity += 10;
		Card *temp = new Card[capacity];

		// Copy over the existing values
		for(int i = 0; i < GetLength(); i++)
		{
			temp[i] = array[i];
		}

		// Set array to the new temporary array and free up used memory
		delete [] array;
		array = temp;
		delete [] temp;
	}
	Card *card = new Card(value, color);
	array[length++] = card;
}

Card	DynamicArray::GetItem(int index)
{
	if(index >= 0 && index <= GetLength() - 1)
	{
		return array[index];
	}
}

void	DynamicArray::SetItem(int index, Card card)
{
	if(index >= 0 && index <= GetLength() - 1)
	{
		array[index] = card;
	}
}

Card	DynamicArray::Remove(int value)
{
	for(int i = 0; i < GetLength(); i++)
		{
			if(array[i]->GetValue() == value)
			{
				//Remove the value
				length--;
				for(int j = i; j + GetLength(); j++)
				{
					array[j] = array[j + 1];
				}
			}
		}
}

void	DynamicArray::RemoveAt(int index)
{
	if(index >= 0 && index <= GetLength() - 1)
	{
		length--;
		for(int j = index; j + GetLength(); j++)
		{
			array[j] = array[j + 1];
		}
	}
}

void	DynamicArray::Clear()
{
	length = 0;
}

int		DynamicArray::GetLength()
{
	return length;
}
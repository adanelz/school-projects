/*
 * node.h
 *
 *  Created on: Sep 2, 2011
 *      Author: Justin Knox
 */
#include "Card.h"

#ifndef NODE_H_
#define NODE_H_

#include <iostream>
using namespace std;

class Node {
	public:
		Node();
		Node(Card *c, Node *next);

		Card* GetCard();
		void SetCard(Card *c);

		Node* GetNext();
		void SetNext(Node *next);


	protected:
		Card *card;
		Node *next;

};

#endif /* NODE_H_ */

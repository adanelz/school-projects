#ifndef DYNAMICARRAY_H_
#define DYNAMICARRAY_H_

template <class T>
class DynamicArray
{
	public:
		DynamicArray();
		~DynamicArray();

		void Add(T value);
		T GetItem(int index);
		void SetValue(int index, T value);

		int Contains(T value);

		int Remove(T value);
		void RemoveAt(int index);
		void Clear();

		int GetLength();

	protected:
		T *array;
		int length;
		int capacity;
};

template <class T>
DynamicArray<T>::DynamicArray()
{
	length = 0;
	capacity = 10;
	array = new T[capacity];
}

template <class T>
DynamicArray<T>::~DynamicArray()
{
	delete [] array;
}

template <class T>
void DynamicArray<T>::Add(T value)
{
	if(length == capacity)
	{
		//Grow array, hundervorst.
		capacity *= 2;
		T *temp = new T[capacity];
		for(int i = 0; i < GetLength(); i++)
		{
			temp[i] = array[i];
		}
		delete[] array;
		array = temp;
	}
	array[length] = value;
	length++;
}

template <class T>
T DynamicArray<T>::GetItem(int index)
{
	if(index >= 0 && index <= GetLength() - 1)
	{
		return array[index];
	}
	return array[0];
}

template <class T>
void DynamicArray<T>::SetValue(int index, T value)
{
	if(index >= 0 && index <= GetLength() - 1)
	{
		array[index] = value;
	}
}

template <class T>
int DynamicArray<T>::Contains(T value)
{
	for(int i = 0; i < GetLength(); i++)
	{
		if(array[i] == value)
		{
			return 1;
		}
	}

	return 0;
}

template <class T>
int DynamicArray<T>::Remove(T value)
{
	for(int i = 0; i < GetLength(); i++)
	{
		if(array[i] == value)
		{
			length--;
			for(int j = i; j < GetLength() - 1; j++)
			{
				array[j] = array[j + 1];
			}
		}

		return 1;
	}

	return 0;
}

template <class T>
void DynamicArray<T>::RemoveAt(int index)
{
	if(index >= 0 && index <= GetLength() - 1)
	{
		length--;
		for(int j = index; j < GetLength() - 1; j++)
		{
			array[j] = array[j + 1];
		}
	}
}

template <class T>
void DynamicArray<T>::Clear()
{
	length = 0; //Instead of clearing the array, we just state that the
	//Valid index is now 0 and no numbers after that are VALID
}

template <class T>
int DynamicArray<T>::GetLength()
{
	return length;
}


#endif /* #ifndef DYNAMICARRAY_H_ */

#include <iostream>
using namespace std;
#ifndef CARD_H_
#define CARD_H_

class Card
{
	public:
		Card();
		Card(int color, int value);
		~Card();

		void SetCard(int color, int value);
		int GetValue();
		int GetColor();
		
	private:
		int value;
		int color;

};

#endif
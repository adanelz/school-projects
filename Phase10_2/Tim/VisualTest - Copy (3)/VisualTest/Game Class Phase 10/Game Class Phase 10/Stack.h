/*
 * Stack.h
 *
 *  Created on: Sep 2, 2011
 *      Author: Justin Knox
 */

#ifndef STACK_H_
#define STACK_H_

#include "Node.h"
#include "Card.h"

class Stack
{
	public:
		Stack();

		void Push(Card *c);
		Card* Pop();

		int Count();
		Node *Top();

	protected:
	Node *top;
	int count;

};

#endif /*STACK_H_*/



/* Author				: Jackie Guiry
 * Date					: Novemeber 1, 2011
 * Purpose				: To create Phase 10
 * Assignment			: Project
 * Note					: Source file for Player class
 */

#include "Player.h"

Player::Player()
{
	position = -1;
	name = "";
	phase = -1;
	skipped = false;
	hand = NULL;
	laid = NULL;
}

Player::Player(int position, string name, int phase, bool skipped, Card **hand, Card **laid)
{
	this->position = position;
	this->name = name;
	this->phase = phase;
	this->skipped = skipped;
	this->hand = hand;
	this->laid = laid;
}

int Player::GetPosition()
{
	return position;
}

string Player::GetName()
{
	return name;	
}

int Player::GetPhase()
{
	return phase;
}

void Player::SetPhase(int value)
{
	this->phase = value;
}

bool Player::GetSkipped()
{
	return skipped;
}

void Player::SetSkipped(bool skip)
{
	this->skipped = skip;
}

Card** Player::GetHand()
{
	return hand;
}

void Player::SetHand(Card **hand)
{
	//for(int i = 0; i < 10; i++)
	this->hand = hand;
}

Card** Player::GetLaid()
{
	return laid;
}

void Player::SetLaid(Card **laid)
{
	//for(int i = 0; i < 40; i++)
	this->laid = laid;
}


int Player::GetScore()
{
	return score;
}
void Player::SetScore(int value)
{
	this->score = value;
}
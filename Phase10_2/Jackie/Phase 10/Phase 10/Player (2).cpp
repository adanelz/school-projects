/* Author				: Jackie Guiry
 * Date					: Novemeber 1, 2011
 * Purpose				: To create Phase 10
 * Assignment			: Project
 * Note					: Source file for Player class
 */

#include "Player.h"
Player::Player()
{//this needed a default constructure for when i was using the class
	position;
	name;
	phase;
	skipped;
}

Player::Player(int position, string name, int phase, bool skipped)
{
	this->position = position;
	this->name = name;
	this->phase = phase;
	this->skipped = skipped;
}

int Player::GetPosition()
{
	return position;
}

string Player::GetName()
{
	return name;	
}

int Player::GetPhase()
{
	return phase;
}

void Player::SetPhase(int value)
{
	this->phase = value;
}

bool Player::GetSkipped()
{
	return skipped;
}

void Player::SetSkipped(bool skip)
{
	this->skipped = skip;
}

//Card Player::GetHand()
//{
//}
//
//void Player::SetHand(Card Hand[10])
//{
//}

/*Card Player::GetLaid()
{

}

void Player::SetLaid(Card laid[40])
{

}*/


#include "Card.h"
#include "Stack.h"
#include "Node.h"
#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
	int i;
	int j;
	int StartIndex = rand() % 108 + 0;
	int DestinationIndex = 0;


	//Make the deck
/******************************************/
	Stack *Deck = new Stack;
	

	//The basic 1-12 cards done twice
	for (i = 0; i < 4; i++)
	{
		for (j = 1; j <= 12; j++)
		{
			Card *c = new Card(i, j);
			Deck->Push(c);
		}
	}

	for (i = 0; i < 4; i++)
	{
		for (j = 1; j <= 12; j++)
		{
			Card *c = new Card(i, j);
			Deck->Push(c);

		}
	}

	//One color of each wild, twice
	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 2; j++)
		{
			Card *c = new Card(i, 13);
			Deck->Push(c);
		}
	}


	//Blue skip cards (Tell tim, 0 = blue, 1 = green, 2 = red, 3 = yellow)
	for (i = 0; i < 1; i++)
	{
		for (j = 0; j < 4; j++)
		{
			Card *c = new Card(i, 14);
			Deck->Push(c);
		}
	}
/**************************************************************/

//shuffle
/*************************************************************/

	int cnt = 0;
	int randNum = 0;
	int randNum2 = 0;
	Card *holder[108];
	Card *temp;
	/*Card *null;

	null = holder[0];
	*/

	while(cnt < 108)
	{
		temp = Deck->Pop();
		holder[cnt] = temp;
		cnt++;
	}

	cnt = 0;
	while(cnt < 10000)
	{
		randNum = rand() % 108 + 0;
		randNum2 = rand() % 108 + 0;
		temp = holder[randNum];
		holder[randNum] = holder[randNum2];
		holder[randNum2] = temp;
		cnt++;
	}

	Stack *shuffledDeck = new Stack;

	for(int i = 0; i < 108; i++)
	{
		shuffledDeck->Push(holder[i]);
		cout << holder[i]->GetValue() << endl;
	}
	return 0;
}
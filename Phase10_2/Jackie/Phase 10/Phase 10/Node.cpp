/*
 * node.cpp
 *
 *  Created on: Sep 2, 2011
 *      Author: Justin Knox
 */

#include "Node.h"

Node::Node()
{
	card = NULL;
	next = NULL;
}

Node::Node(Card *c, Node *next)
{
	this->card = c;
	this->next = next;
}

Card* Node::GetCard()
{
	return card;
}

void Node::SetCard(Card *c)
{
	this->card = c;
}

Node* Node::GetNext()
{
	return next;
}

void Node::SetNext(Node *next)
{
	this->next = next;
}



#include "Card.h"
#include <iostream>
using namespace std;

class DynamicArray
{
	public:
		DynamicArray();
		~DynamicArray();

		void Add(int value, int color);
		Card GetItem(int index);
		void SetItem(int index, Card card);

		Card Remove(int value);
		void RemoveAt(int index);
		void Clear();

		int GetLength();

	protected:
		Card *array;
		int length;
		int capacity;
};
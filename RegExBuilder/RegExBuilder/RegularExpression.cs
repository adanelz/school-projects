﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RegExBuilder
{
    /// <summary>
    /// Builds a Regular Expression from parameters:
    /// -Length
    /// -Excluded Characters
    /// -Specify (front, middle, and end) of the string
    /// </summary>
    class RegularExpression
    {
        #region Class Members

        private string excludedChar;
        protected int numOfStringSegments;
        protected int minLength;
        protected int maxLength;

        protected List<StringSegment> segments;

        #endregion

        #region Constructor

        public RegularExpression()
        {
            //Global
            this.minLength = 0;
            this.maxLength = 0;
            this.segments = new List<StringSegment>();
        }

        public RegularExpression(string regex)
        {
            //parse regex
        }

        #endregion

        #region Properties

        /// <summary>
        /// Characters to be excluded
        /// </summary>
        public string ExcludedChar
        {
            get
            {
                return excludedChar;
            }
            set
            {
                this.excludedChar = value;
            }
        }

        /// <summary>
        /// holds the number of string segments that the Regular Expression will have
        /// </summary>
        public int NumberOfStringSegments
        {
            get
            {
                return numOfStringSegments;
            }
            set
            {
                this.numOfStringSegments = value;
            }
        }

        /// <summary>
        /// Minamum length of the string
        /// </summary>
        public int MinLength
        {
            get
            {
                return minLength;
            }
            set
            {
                this.minLength = value;
            }
        }

        /// <summary>
        /// Maximum length of the string
        /// </summary>
        public int MaxLength
        {
            get
            {
                return maxLength;
            }
            set
            {
                this.maxLength = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Hold the list of String Segments
        /// </summary>
        /// <param name="n">Index of the desired segment</param>        
        /// <returns>the held list</returns>
        public StringSegment GetStringSegments(int n)
        {
            return segments[n];
        }

        /// <summary>
        /// Adds a sent in StringSegment to the held list
        /// </summary>
        /// <param name="s">StringSegment needing to be added</param>
        public void AddStringSegment(StringSegment s)
        {
            this.segments.Add(s);
        }

        public void ClearSegments()
        {
            this.segments.Clear();
        }

        #endregion Methods

        #region Overrloaded Methods

        /// <summary>
        ///excluded Characters
        ///        \   /\length/    \ String Segment /
        ///   ^(?=[^@#$]{17,19}$)(\b(?i:babc[A-Z0-9]+)\b)$
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder regEx = new StringBuilder();
            bool moreContent = false;

            // ^
            regEx.Append(SpecialCharacter.OpenRegEx);

            #region Global

            for (int i = 0; i < segments.Count; i++)
            {
                if (segments[i].More)
                    moreContent = true;
            }

            if (moreContent || excludedChar != "")
            {
                // (?=
                regEx.Append(SpecialCharacter.OpenLookaround);

                if (excludedChar != "")
                {
                    // [^@#$]
                    regEx.Append(SpecialCharacter.OpenExcBlock).Append(excludedChar).Append(SpecialCharacter.CloseExcBlock);
                }
                else
                {
                    regEx.Append("[^ ]");
                }

                if (moreContent)
                {
                    // {17,19}
                    regEx.Append(SpecialCharacter.BuildNumebrRange(minLength, maxLength));
                }
                else
                {
                    regEx.Append("{0,}");
                }

                // $)
                regEx.Append(SpecialCharacter.CloseLookaround);
            }
            #endregion

            // (\b
            regEx.Append(SpecialCharacter.StartWord);

            #region Segments
            for (int i = 0; i < segments.Count; i++)
            {
                if (segments[i].More || segments[i].SpecificString != "")
                {
                    // (?
                    regEx.Append(SpecialCharacter.OpenModifier);
                    if (segments[i].CaseSensitivity)
                    {
                        // -i
                        regEx.Append(SpecialCharacter.CaseSesativeOnFlag);
                    }
                    else
                    {
                        // i
                        regEx.Append(SpecialCharacter.CaseSesativeOffFlag);
                    }
                    // :
                    regEx.Append(SpecialCharacter.Colon);

                    // specific string
                    regEx.Append(segments[i].SpecificString);
                    if (segments[i].More && (segments[i].GetAllowChar() != "" || segments[i].GetAllowNum() != ""))
                    {
                        // [A-Z0-9]+
                        regEx.Append(SpecialCharacter.OpenCharacterRange).Append(segments[i].GetAllowChar()).Append(segments[i].GetAllowNum()).Append(SpecialCharacter.CloseCharacterRange);
                    }
                    // )
                    regEx.Append(SpecialCharacter.CloseModifier);
                }
            }

            #endregion Segments

            // \b)
            regEx.Append(SpecialCharacter.EndWord);

            // $
            regEx.Append(SpecialCharacter.CloseRegEx);

            return regEx.ToString();
        }

        #endregion
    }
}

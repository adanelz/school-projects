﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace RegExBuilder
{
    public partial class RegExBuilder : Form
    {

        #region Main

        [STAThread]
        static void Main()
        {
            string TESTINPUTREGULAREXPRESSION = "^(?=[^!@#]{12,15}$)(\\b(?-i:abc[A-Ua-vZ,z0-58,9]+)(?-i:mno[A-Ta-uz0-67]+)(?-i:xyz[A-Ua-uw0-78]+)\\b)$";

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new RegExBuilder());
            Application.Run(new RegExBuilder(TESTINPUTREGULAREXPRESSION));
        }

        #endregion

        RegularExpression RegularExpression = new RegularExpression();
        List<StringSegmentUI> StringSegmentForms = new List<StringSegmentUI>();

        public RegExBuilder()
        {
            InitializeComponent();
            AddNewStringSegmentForm();

        }

        public RegExBuilder(string regex)
        {
            InitializeComponent();
            PopulateControls(regex);
        }

        private void PopulateControls(string regex)
        {
            char[] indexChar = regex.ToCharArray();
            bool isWords = false;
            string word = "";
            StringBuilder excludedCharacters = new StringBuilder();
            StringBuilder minLength = new StringBuilder();
            StringBuilder maxLength = new StringBuilder();

            //excluded Characters
            //        \   /\length/   \  String Segment /
            //   ^(?=[^@#$]{17,19}$)(\b(?i:abc[A-Z0-9]+)\b)$
            //MessageBox.Show(indexChar[i].ToString());

            
            for (int i = 0; i < regex.Length; i++)
            {

                //compare the current charater being looked at against an )
                if(indexChar[i] == SpecialCharacter.OpenParenthesis)
                {
                    //Look for an open lookaround
                    word = regex.Substring(i, SpecialCharacter.OpenLookaround.Length);

                    //check if the index is at the start of a Lookaround
                    if (word == SpecialCharacter.OpenLookaround)
                    {

                        #region Getting info from Lookaround

                        //search through the Lookaround starting at the index and stopping at the the regex length minus the index
                        for (int j = i; j < regex.Length - i; j++)
                        {
                            //if the exclusion block is found
                            if (regex.Substring(j, SpecialCharacter.OpenExcBlock.Length) == SpecialCharacter.OpenExcBlock.ToString())
                            {
                                //search through the exclusion block starting after the ^
                                for (int k = j + SpecialCharacter.OpenExcBlock.Length; k < regex.Length - j; k++)
                                {
                                    if (indexChar[k].ToString() != SpecialCharacter.CloseExcBlock)
                                    {
                                        //build the exclusion string
                                        excludedCharacters.Append(indexChar[k]);
                                    }
                                    else
                                    {//if the SpecialCharacter.CloseExcBlock is found then stop searching
                                        //set the pointer
                                        j = k;
                                        break;
                                    }
                                }
                            }
                            if (indexChar[j].ToString() == SpecialCharacter.OpenNumberRange)
                            {
                                //build the string from the min length
                                for (int k = j + SpecialCharacter.OpenNumberRange.Length; k < regex.Length - j; k++)
                                {
                                    if (indexChar[k] != SpecialCharacter.Comma)
                                    {
                                        minLength.Append(indexChar[k]);
                                    }
                                    else
                                    {//if the SpecialCharacter.Comma is found then stop searching
                                        //set the pointer
                                        j = k;
                                        break;
                                    }
                                }
                                //build the string from the min length
                                for (int k = j + SpecialCharacter.CloseNumberRange.Length; k < regex.Length - j; k++)
                                {
                                    if (indexChar[k].ToString() != SpecialCharacter.CloseNumberRange)
                                    {
                                        maxLength.Append(indexChar[k]);
                                    }
                                    else
                                    {//if the SpecialCharacter.CloseCurly is found then stop searching
                                        //set the pointer
                                        j = k;
                                        break;
                                    }
                                }
                            }

                            //if the end of the lookaround is found
                            if (regex.Substring(j, SpecialCharacter.CloseLookaround.Length) == SpecialCharacter.CloseLookaround.ToString())
                            {
                                //set the index to the end of the lookaround
                                i = j + 1;
                            }
                        }

                        #endregion Lookaround

                    }

                    //search for the start of the word
                    word = regex.Substring(i, SpecialCharacter.StartWord.Length);

                    if (word == SpecialCharacter.StartWord)
                    {
                        isWords = true;
                        i += word.Length;
                    }


                    //check if the index is at the start of a word
                    if (isWords)
                    {
                        AddNewStringSegmentForm();
                        StringSegmentUI newStringSegmentForm = StringSegmentForms[StringSegmentForms.Count - 1];
                        StringSegment newStingSegment = new StringSegment();

                        if (regex.Substring(i, SpecialCharacter.OpenModifier.Length) == SpecialCharacter.OpenModifier)
                        {

                            #region Modifier

                            i += SpecialCharacter.OpenModifier.Length;
                            if (regex.Substring(i, SpecialCharacter.CaseSesativeOnFlag.Length) == SpecialCharacter.CaseSesativeOnFlag)
                            {
                                newStingSegment.CaseSensitivity = true;
                                i += SpecialCharacter.CaseSesativeOnFlag.Length;
                            }
                            else if (regex.Substring(i, SpecialCharacter.CaseSesativeOffFlag.Length) == SpecialCharacter.CaseSesativeOffFlag)
                            {
                                newStingSegment.CaseSensitivity = false;
                                i += SpecialCharacter.CaseSesativeOffFlag.Length;
                            }
                            i += SpecialCharacter.CloseModifier.Length;

                            #endregion Modifier

                            #region Specific String

                            for (int k = i; k < regex.Length; k++)
                            {
                                if (indexChar[k].ToString() == SpecialCharacter.OpenCharacterRange)
                                {
                                    int lengthOfSpecificString = k - i;
                                    newStingSegment.SpecificString = regex.Substring(i, lengthOfSpecificString);
                                    i += lengthOfSpecificString;
                                    break;
                                }
                            }

                            #endregion Specific String

                            #region Character and Numeric Range

                            if (indexChar[i].ToString() == SpecialCharacter.OpenCharacterRange)
                            {
                                i += SpecialCharacter.OpenCharacterRange.Length;
                                newStingSegment.More = true;
                                bool charsFound = false;

                                for (int k = i; k < regex.Length - 1; k++)
                                {
                                    if (Char.IsNumber(indexChar[k]) && !charsFound)
                                    {
                                        int lengthOfAllowChar = k - i;
                                        string allowChar = regex.Substring(i, lengthOfAllowChar);
                                        newStingSegment.AllowCharNeeded = true;
                                        newStingSegment.SetAllowChar(allowChar);
                                        i += lengthOfAllowChar;
                                        charsFound = true;
                                    }

                                    if (regex.Substring(k, SpecialCharacter.CloseCharacterRange.Length) == SpecialCharacter.CloseCharacterRange)
                                    {
                                        int lengthOfAllowNum = k - i;
                                        string allowNum = regex.Substring(i, lengthOfAllowNum);
                                        newStingSegment.AllowNumNeeded = true;
                                        newStingSegment.SetAllowNum(allowNum);
                                        i += lengthOfAllowNum + SpecialCharacter.CloseCharacterRange.Length;
                                        break;
                                    }
                                }
                            }

                            #endregion Character and Numeric Range

                            i += SpecialCharacter.CloseModifier.Length;

                            //set the information to the form
                            newStringSegmentForm.SetInformation(newStingSegment);
                        }
                    }
                }
            }

            nud_MinLength.Value = int.Parse(minLength.ToString());
            nud_MaxLength.Value = int.Parse(maxLength.ToString());
            tb_ExcludedChararacters.Text = excludedCharacters.ToString();
            
            rtb_RegularExpression.Text = regex;
        }

       

        private void AddNewStringSegmentForm()
        {
            #region Bug Fix
            ///This section of code only serves to fix a bug when adding the form that 
            ///went to far to the right and made the panel have to scoll. The form whould 
            ///be drawn in the wrong location. It is no longer a problem
            System.Windows.Forms.Panel testPanel = new System.Windows.Forms.Panel();
            testPanel.Size = new System.Drawing.Size(5, 5);
            testPanel.Location = new System.Drawing.Point(pnl_StringSegments.Width - 4, 0);
            pnl_StringSegments.Controls.Add(testPanel);
            ///__________________________________________________________________________________
            #endregion Bug Fix

            //variables
            StringSegmentUI newStringSegmentForm = new StringSegmentUI();
            int NumOfStringSegments = StringSegmentForms.Count;//how many String Segments there are
            System.Windows.Forms.Panel newPanel = new System.Windows.Forms.Panel();
            int xLocOfPanel = (NumOfStringSegments * (newStringSegmentForm.Size.Width + 3)) - pnl_StringSegments.HorizontalScroll.Value;
            int xLocOfButton = ((NumOfStringSegments + 1) * (newStringSegmentForm.Size.Width + 3)) - pnl_StringSegments.HorizontalScroll.Value;

            //move the button
            btn_AddStringSegment.Location = new System.Drawing.Point(xLocOfButton, 7);

            //Change Properties of the new form
            newStringSegmentForm.TopLevel = false;
            newStringSegmentForm.SetName(NumOfStringSegments.ToString());

            //Change Properties of the new panel
            newPanel.Size = new System.Drawing.Size((newStringSegmentForm.Size.Width + 3), (newStringSegmentForm.Size.Height + 3));
            newPanel.Location = new System.Drawing.Point(xLocOfPanel, 0);


            //store and show the new form
            StringSegmentForms.Add(newStringSegmentForm);
            newPanel.Controls.Add(newStringSegmentForm);
            pnl_StringSegments.Controls.Add(newPanel);
            newStringSegmentForm.Show();
        }

        private void btn_Create_Click(object sender, EventArgs e)
        {
            //Gets the Information to build the Regular Expression
            SetRegularExpression();

            //Prints the Regular Expresison to the screen
            rtb_RegularExpression.Text = RegularExpression.ToString();
        }

        private void btn_Test_Click(object sender, EventArgs e)
        {
            //test the entered string and the Regular Expression for a match
            tb_Result.Text = Regex.IsMatch(tb_TestString.Text.Trim(), RegularExpression.ToString()).ToString();
        }

        private void SetRegularExpression()
        {
            //collect all of the information for the Regular Expression
            RegularExpression.MinLength = (int)nud_MinLength.Value;
            RegularExpression.MaxLength = (int)nud_MaxLength.Value;
            RegularExpression.ExcludedChar = tb_ExcludedChararacters.Text;

            RegularExpression.ClearSegments();
            for (int i = 0; i < StringSegmentForms.Count; i++)
            {
                StringSegment newSegment = new StringSegment();

                RegularExpression.AddStringSegment(StringSegmentForms[i].GetStringSegment());
            }
        }

        private void btn_AddStringSegment_Click(object sender, EventArgs e)
        {
            //add new form when clicked
            AddNewStringSegmentForm();

            //pan all the way to the right
            pnl_StringSegments.HorizontalScroll.Value = pnl_StringSegments.HorizontalScroll.Maximum;
        }

        private void cb_CaseSensitivity_CheckedChanged(object sender, EventArgs e)
        {
            //goes through all of the forms and sets the CaseSensitivity check boxes
            for (int i = 0; i < StringSegmentForms.Count; i++)
            {
                StringSegmentForms[i].SetCaseSensitivity(cb_CaseSensitivity.Checked);
            }
        }

        private void nud_MinLength_ValueChanged(object sender, EventArgs e)
        {
            //prevents the Min length Value form exceeding the Max Length Value
            nud_MaxLength.Minimum = nud_MinLength.Value;
        }

        private void nud_MaxLength_ValueChanged(object sender, EventArgs e)
        {
            //prevents the Max length Value form deceeding the Min Length Value
            nud_MinLength.Maximum = nud_MaxLength.Value;
        }
    }
}

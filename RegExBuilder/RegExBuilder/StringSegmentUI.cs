﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace RegExBuilder
{
    internal partial class StringSegmentUI : Form
    {
        public StringSegmentUI()
        {
            InitializeComponent();
        }

        public StringSegment GetStringSegment()
        {
            StringSegment seg = new StringSegment();

            seg.CaseSensitivity = cb_CaseSensitivity.Checked;
            seg.SpecificString = tb_SpecificString.Text;
            seg.More = gb_MoreContent.Enabled;
            seg.AllowCharNeeded = cb_CharRange.Checked;
            seg.SetAllowChar(dud_ACSUpp_RangeStart.Text, dud_ACSUpp_RangeEnd.Text, dud_ACSLow_RangeStart.Text, dud_ACSLow_RangeEnd.Text, tb_ACS_SpecificCharacters.Text);
            seg.AllowNumNeeded = cb_NumRange.Checked;
            seg.SetAllowNum(nud_ANS_RangeStart.Value.ToString(), nud_ANS_RangeEnd.Value.ToString(), tb_ANS_SpecificNumbers.Text);

            return seg;
        }

        /// <summary>
        /// Sets the Text that is displayed at the top of the Group Box
        /// </summary>
        /// <param name="name">String to set as the Name</param>
        public void SetName(string name)
        {
            gb_StringSegment.Text = name;
        }

        public void SetInformation(StringSegment segmentInfo)
        {
            int dashcount = 0;
            tb_SpecificString.Text = segmentInfo.SpecificString;

            cb_CharRange.Checked = segmentInfo.AllowCharNeeded;
            string allowChar = segmentInfo.GetAllowChar();

            for (int i = 0; i < allowChar.Length; i++)
            {
                if (allowChar[i] == SpecialCharacter.Dash && dashcount == 1)
                {
                    dud_ACSLow_RangeStart.Text = allowChar[i - 1].ToString();
                    dud_ACSLow_RangeEnd.Text = allowChar[i + 1].ToString();
                    dashcount++;
                }

                if (allowChar[i] == SpecialCharacter.Dash && dashcount == 0)
                {
                    dud_ACSUpp_RangeStart.Text = allowChar[i - 1].ToString();
                    dud_ACSUpp_RangeEnd.Text = allowChar[i + 1].ToString();
                    dashcount++;
                }

                if (allowChar[i] == SpecialCharacter.Comma)
                {
                    tb_ACS_SpecificCharacters.Text = allowChar.Substring(i - 1, allowChar.Length - (i - 1));
                    break;
                }
            }

            cb_NumRange.Checked = segmentInfo.AllowNumNeeded;
            string allowNum = segmentInfo.GetAllowNum();

            for (int i = 0; i < allowNum.Length; i++)
            {
                if (allowNum[i] == SpecialCharacter.Dash)
                {
                    nud_ANS_RangeStart.Text = allowNum[i - 1].ToString();
                    nud_ANS_RangeEnd.Text = allowNum[i + 1].ToString();
                }

                if (allowNum[i] == SpecialCharacter.Comma)
                {
                    tb_ANS_SpecificNumbers.Text = allowNum.Substring(i - 1, allowNum.Length - (i - 1));
                    break;
                }
            }

            cb_CaseSensitivity.Checked = segmentInfo.CaseSensitivity;
            cb_MoreContent.Checked = segmentInfo.More;

        }

        /// <summary>
        /// Sets the checkbox for case sensitivity
        /// </summary>
        /// <param name="caseSensi">Boolean variable that sets the Case sensitivity Check box</param>
        public void SetCaseSensitivity(bool caseSensi)
        {
            cb_CaseSensitivity.Checked = caseSensi;
        }

        private void cb_MoreContent_CheckedChanged(object sender, EventArgs e)
        {
            //enables or disables the More Content Group Box
            gb_MoreContent.Enabled = cb_MoreContent.Checked;
        }

        private void cb_NumRange_CheckedChanged(object sender, EventArgs e)
        {
            //enables or disables the numeric up downs
            nud_ANS_RangeStart.Enabled = cb_NumRange.Checked;
            nud_ANS_RangeEnd.Enabled = cb_NumRange.Checked;
        }

        private void cb_CharRange_CheckedChanged(object sender, EventArgs e)
        {
            //enables or disables the domain up downs
            dud_ACSLow_RangeStart.Enabled = cb_CharRange.Checked;
            dud_ACSLow_RangeEnd.Enabled = cb_CharRange.Checked;
            dud_ACSUpp_RangeStart.Enabled = cb_CharRange.Checked;
            dud_ACSUpp_RangeEnd.Enabled = cb_CharRange.Checked;
        }

        private void tb_ANS_SpecificNumbers_Leave(object sender, EventArgs e)
        {
            /* This should work to make sure the entered text is in the correct format but it isn't for some reason
            Regex regex = new Regex("^(\b([0-9])|([0-9](,[0-9])+)\b)$");

            if (regex.IsMatch(tb_ANS_SpecificNumbers.Text.Trim()) && tb_ANS_SpecificNumbers.Text != "")
            {
                MessageBox.Show("The Specific Numbers must be in the format: 0,1,2,3");
                tb_ANS_SpecificNumbers.Text = "";
            }
            */
        }

        private void tb_ACS_SpecificCharacters_Leave(object sender, EventArgs e)
        {
            /* This should work to make sure the entered text is in the correct format but it isn't for some reason
            Regex regex = new Regex("^(\b([a-zA-Z])|([a-zA-Z](,[a-zA-Z])+)\b)$");

            if (!regex.IsMatch(tb_ACS_SpecificCharacters.Text.Trim()) && tb_ACS_SpecificCharacters.Text != "")
            {
                MessageBox.Show("The Specific Numbers must be in the format: A,b,C,d");
                tb_ACS_SpecificCharacters.Text = "";
            }
            */
        }
    }
}

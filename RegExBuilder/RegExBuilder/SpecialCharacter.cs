﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RegExBuilder
{
    static class SpecialCharacter
    {
        #region Properties

        /// <summary>
        /// ^
        /// </summary>
        static public string OpenRegEx
        {
            get
            {
                return "^";
            }
        }

        /// <summary>
        /// $
        /// </summary>
        static public string CloseRegEx
        {
            get
            {
                return "$";
            }
        }

        /// <summary>
        /// (\b
        /// </summary>
        static public string StartWord
        {
            get
            {
                return "(\\b";
            }
        }

        /// <summary>
        /// \b)
        /// </summary>
        static public string EndWord
        {
            get
            {
                return "\\b)";
            }
        }

        /// <summary>
        /// (?=
        /// </summary>
        static public string OpenLookaround
        {
            get
            {
                return "(?=";
            }
        }

        /// <summary>
        /// $)
        /// </summary>
        static public string CloseLookaround
        {
            get
            {
                return "$)";
            }
        }

        /// <summary>
        /// (?
        /// </summary>
        static public string OpenModifier
        {
            get
            {
                return "(?";
            }
        }

        /// <summary>
        /// )
        /// </summary>
        static public string CloseModifier
        {
            get
            {
                return ")";
            }
        }

        /// <summary>
        /// {start,end}
        /// </summary>
        /// <param name="start">Number range start</param>
        /// <param name="end">Number range end</param>
        /// <returns>Built code for the range</returns>
        static public string BuildNumebrRange(int start, int end)
        {
            //if start is greater then end...
            StringBuilder sb = new StringBuilder();

            sb.Append(SpecialCharacter.OpenCurly).Append(start.ToString()).Append(SpecialCharacter.Comma).Append(end.ToString()).Append(SpecialCharacter.ClosedCurly);

            return sb.ToString();
        }

        /// <summary>
        /// [^
        /// </summary>
        static public string OpenExcBlock
        {
            get
            {
                return "[^";
            }
        }

        /// <summary>
        /// ]
        /// </summary>
        static public string CloseExcBlock
        {
            get
            {
                return "]";
            }
        }

        /// <summary>
        /// i
        /// </summary>
        static public string CaseSesativeOffFlag
        {
            get
            {
                return "i";
            }
        }

        /// <summary>
        /// -i
        /// </summary>
        static public string CaseSesativeOnFlag
        {
            get
            {
                return "-i";
            }
        }

        /// <summary>
        /// [
        /// </summary>
        static public string OpenCharacterRange
        {
            get
            {
                return "[";
            }
        }

        /// <summary>
        /// ]+
        /// </summary>
        static public string CloseCharacterRange
        {
            get
            {
                return "]+";
            }
        }

        /// <summary>
        /// {
        /// </summary>
        static public string OpenNumberRange
        {
            get
            {
                return "{";
            }
        }

        /// <summary>
        /// }
        /// </summary>
        static public string CloseNumberRange
        {
            get
            {
                return "}";
            }
        }

        /// <summary>
        /// [
        /// </summary>
        static public char OpenBracket
        {
            get
            {
                return '[';
            }
        }

        /// <summary>
        /// ]
        /// </summary>
        static public char CloseBracket
        {
            get
            {
                return ']';
            }
        }

        /// <summary>
        /// {
        /// </summary>
        static public char OpenCurly
        {
            get
            {
                return '{';
            }
        }

        /// <summary>
        /// }
        /// </summary>
        static public char ClosedCurly
        {
            get
            {
                return '}';
            }
        }

        /// <summary>
        /// (
        /// </summary>
        static public char OpenParenthesis
        {
            get
            {
                return '(';
            }
        }

        /// <summary>
        /// )
        /// </summary>
        static public char ClosedParenthesis
        {
            get
            {
                return ')';
            }
        }

        /// <summary>
        /// -
        /// </summary>
        static public char Dash
        {
            get
            {
                return '-';
            }
        }

        /// <summary>
        /// :
        /// </summary>
        static public char Colon
        {
            get
            {
                return ':';
            }
        }

        /// <summary>
        /// ,
        /// </summary>
        static public char Comma
        {
            get
            {
                return ',';
            }
        }

        #endregion Properties
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RegExBuilder
{
    class StringSegment
    {
        #region Class Members

        //parts of a String segment
        protected bool caseSensitivity;
        protected string specificString;
        protected bool allowCharNeeded;
        protected string allowChar;
        protected bool allowNumNeeded;
        protected string allowNum;
        protected bool more;

        #endregion Class Members

        #region Constructor

        public StringSegment()
        {
            this.specificString= "";
            this.allowChar = "";
            this.allowNum = "";
        }

        #endregion Constructor

        #region Properties

        /// <summary>
        /// Case Sensitivity of the String Segment
        /// </summary>
        public bool CaseSensitivity
        {
            get
            {
                return caseSensitivity;
            }
            set
            {
                this.caseSensitivity = value;
            }
        }

        /// <summary>
        /// Is there going to be more to the Specific string
        /// </summary>
        public bool More
        {
            get
            {
                return more;
            }
            set
            {
                this.more = value;
            }
        }

        /// <summary>
        /// Specific String to be included
        /// </summary>
        public string SpecificString
        {
            get
            {
                return specificString;
            }
            set
            {
                this.specificString = value;
            }
        }

        /// <summary>
        /// Is a character range needed
        /// </summary>
        public bool AllowCharNeeded
        {
            get
            {
                return allowCharNeeded;
            }
            set
            {
                this.allowCharNeeded = value;
            }
        }

        /// <summary>
        /// Is a number range needed
        /// </summary>
        public bool AllowNumNeeded
        {
            get
            {
                return allowNumNeeded;
            }
            set
            {
                this.allowNumNeeded = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// the held code that defines what characters to allow
        /// </summary>
        /// <returns>the held code as a string</returns>
        public string GetAllowChar()
        {
            return allowChar;
        }

        /// <summary>
        /// Sets the Allowable Characters for the part of the string
        /// </summary>
        /// <param name="startCap">Begining of the CAPITAL Character Range</param>
        /// <param name="endCap">End of the CAPITAL Character Range</param>
        /// <param name="startLow">Begining of the lowercase Character Range</param>
        /// <param name="endLow">End of the lowercase Character Range</param>
        /// <param name="specific">Specific Characters to be included</param>
        public void SetAllowChar(string startCap, string endCap, string startLow, string endLow, string specific)
        {
            StringBuilder allowChar = new StringBuilder();

            if (allowCharNeeded)
            {
                allowChar.Append(startCap).Append(SpecialCharacter.Dash).Append(endCap).Append(startLow).Append(SpecialCharacter.Dash).Append(endLow);
            }
            allowChar.Append(specific);

            this.allowChar = allowChar.ToString();
        }

        /// <summary>
        /// Set the allowable character string when the string already exists
        /// </summary>
        /// <param name="allowChar">The existing allowable character string</param>
        public void SetAllowChar(string allowChar)
        {
            this.allowChar = allowChar;
        }

        /// <summary>
        /// the held code that defines what numbers to allow
        /// </summary>
        /// <returns>the held code as a string</returns>
        public string GetAllowNum()
        {
            return allowNum;
        }

        /// <summary>
        /// Sets the Allowable Numbers for the part of the string
        /// </summary>
        /// <param name="start">Begining of th Number Range</param>
        /// <param name="end">End of the Number Range</param>
        /// <param name="specific">Specific Numbers to be included</param>
        public void SetAllowNum(string start, string end, string specific)
        {
            StringBuilder allowNum = new StringBuilder();

            if (allowNumNeeded)
            {
                allowNum.Append(start).Append(SpecialCharacter.Dash).Append(end);
            }
            allowNum.Append(specific);

            this.allowNum = allowNum.ToString();
        }

        /// <summary>
        /// Set the allowable number string when the string already exists
        /// </summary>
        /// <param name="allowChar">The existing allowable number string</param>
        public void SetAllowNum(string allowNum)
        {
            this.allowNum = allowNum;
        }

        #endregion Methods
    }
}

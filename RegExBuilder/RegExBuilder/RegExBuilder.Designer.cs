﻿namespace RegExBuilder
{
    partial class RegExBuilder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_Global = new System.Windows.Forms.GroupBox();
            this.cb_CaseSensitivity = new System.Windows.Forms.CheckBox();
            this.tb_Result = new System.Windows.Forms.TextBox();
            this.rtb_RegularExpression = new System.Windows.Forms.RichTextBox();
            this.lbl_RegularExpression = new System.Windows.Forms.Label();
            this.lbl_TestString = new System.Windows.Forms.Label();
            this.tb_TestString = new System.Windows.Forms.TextBox();
            this.btn_Test = new System.Windows.Forms.Button();
            this.lbl_ExcludedChararacters = new System.Windows.Forms.Label();
            this.lbl_MaxLength = new System.Windows.Forms.Label();
            this.tb_ExcludedChararacters = new System.Windows.Forms.TextBox();
            this.lbl_MinLength = new System.Windows.Forms.Label();
            this.nud_MaxLength = new System.Windows.Forms.NumericUpDown();
            this.btn_Create = new System.Windows.Forms.Button();
            this.nud_MinLength = new System.Windows.Forms.NumericUpDown();
            this.pnl_StringSegments = new System.Windows.Forms.Panel();
            this.btn_AddStringSegment = new System.Windows.Forms.Button();
            this.gb_Global.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_MaxLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_MinLength)).BeginInit();
            this.pnl_StringSegments.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_Global
            // 
            this.gb_Global.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_Global.Controls.Add(this.cb_CaseSensitivity);
            this.gb_Global.Controls.Add(this.tb_Result);
            this.gb_Global.Controls.Add(this.rtb_RegularExpression);
            this.gb_Global.Controls.Add(this.lbl_RegularExpression);
            this.gb_Global.Controls.Add(this.lbl_TestString);
            this.gb_Global.Controls.Add(this.tb_TestString);
            this.gb_Global.Controls.Add(this.btn_Test);
            this.gb_Global.Controls.Add(this.lbl_ExcludedChararacters);
            this.gb_Global.Controls.Add(this.lbl_MaxLength);
            this.gb_Global.Controls.Add(this.tb_ExcludedChararacters);
            this.gb_Global.Controls.Add(this.lbl_MinLength);
            this.gb_Global.Controls.Add(this.nud_MaxLength);
            this.gb_Global.Controls.Add(this.btn_Create);
            this.gb_Global.Controls.Add(this.nud_MinLength);
            this.gb_Global.Location = new System.Drawing.Point(12, 12);
            this.gb_Global.Name = "gb_Global";
            this.gb_Global.Size = new System.Drawing.Size(462, 123);
            this.gb_Global.TabIndex = 28;
            this.gb_Global.TabStop = false;
            this.gb_Global.Text = "Global";
            // 
            // cb_CaseSensitivity
            // 
            this.cb_CaseSensitivity.AutoSize = true;
            this.cb_CaseSensitivity.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cb_CaseSensitivity.Checked = true;
            this.cb_CaseSensitivity.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_CaseSensitivity.Location = new System.Drawing.Point(21, 13);
            this.cb_CaseSensitivity.Name = "cb_CaseSensitivity";
            this.cb_CaseSensitivity.Size = new System.Drawing.Size(100, 17);
            this.cb_CaseSensitivity.TabIndex = 30;
            this.cb_CaseSensitivity.Text = "Case Sensitivity";
            this.cb_CaseSensitivity.UseVisualStyleBackColor = true;
            this.cb_CaseSensitivity.CheckedChanged += new System.EventHandler(this.cb_CaseSensitivity_CheckedChanged);
            // 
            // tb_Result
            // 
            this.tb_Result.Location = new System.Drawing.Point(363, 95);
            this.tb_Result.Name = "tb_Result";
            this.tb_Result.ReadOnly = true;
            this.tb_Result.Size = new System.Drawing.Size(92, 20);
            this.tb_Result.TabIndex = 29;
            // 
            // rtb_RegularExpression
            // 
            this.rtb_RegularExpression.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtb_RegularExpression.Location = new System.Drawing.Point(202, 17);
            this.rtb_RegularExpression.Name = "rtb_RegularExpression";
            this.rtb_RegularExpression.Size = new System.Drawing.Size(253, 45);
            this.rtb_RegularExpression.TabIndex = 26;
            this.rtb_RegularExpression.Text = "";
            // 
            // lbl_RegularExpression
            // 
            this.lbl_RegularExpression.AutoSize = true;
            this.lbl_RegularExpression.Location = new System.Drawing.Point(153, 17);
            this.lbl_RegularExpression.Name = "lbl_RegularExpression";
            this.lbl_RegularExpression.Size = new System.Drawing.Size(42, 13);
            this.lbl_RegularExpression.TabIndex = 25;
            this.lbl_RegularExpression.Text = "RegEx:";
            this.lbl_RegularExpression.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lbl_TestString
            // 
            this.lbl_TestString.AutoSize = true;
            this.lbl_TestString.Location = new System.Drawing.Point(134, 71);
            this.lbl_TestString.Name = "lbl_TestString";
            this.lbl_TestString.Size = new System.Drawing.Size(61, 13);
            this.lbl_TestString.TabIndex = 24;
            this.lbl_TestString.Text = "Test String:";
            this.lbl_TestString.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tb_TestString
            // 
            this.tb_TestString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_TestString.Location = new System.Drawing.Point(201, 68);
            this.tb_TestString.Name = "tb_TestString";
            this.tb_TestString.Size = new System.Drawing.Size(254, 20);
            this.tb_TestString.TabIndex = 23;
            // 
            // btn_Test
            // 
            this.btn_Test.Location = new System.Drawing.Point(278, 94);
            this.btn_Test.Name = "btn_Test";
            this.btn_Test.Size = new System.Drawing.Size(79, 23);
            this.btn_Test.TabIndex = 22;
            this.btn_Test.Text = "Test";
            this.btn_Test.UseVisualStyleBackColor = true;
            this.btn_Test.Click += new System.EventHandler(this.btn_Test_Click);
            // 
            // lbl_ExcludedChararacters
            // 
            this.lbl_ExcludedChararacters.AutoSize = true;
            this.lbl_ExcludedChararacters.Location = new System.Drawing.Point(19, 76);
            this.lbl_ExcludedChararacters.Name = "lbl_ExcludedChararacters";
            this.lbl_ExcludedChararacters.Size = new System.Drawing.Size(108, 13);
            this.lbl_ExcludedChararacters.TabIndex = 19;
            this.lbl_ExcludedChararacters.Text = "Excluded Characters:";
            // 
            // lbl_MaxLength
            // 
            this.lbl_MaxLength.AutoSize = true;
            this.lbl_MaxLength.Location = new System.Drawing.Point(19, 58);
            this.lbl_MaxLength.Name = "lbl_MaxLength";
            this.lbl_MaxLength.Size = new System.Drawing.Size(66, 13);
            this.lbl_MaxLength.TabIndex = 8;
            this.lbl_MaxLength.Text = "Max Length:";
            this.lbl_MaxLength.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tb_ExcludedChararacters
            // 
            this.tb_ExcludedChararacters.Location = new System.Drawing.Point(22, 91);
            this.tb_ExcludedChararacters.Name = "tb_ExcludedChararacters";
            this.tb_ExcludedChararacters.Size = new System.Drawing.Size(173, 20);
            this.tb_ExcludedChararacters.TabIndex = 18;
            // 
            // lbl_MinLength
            // 
            this.lbl_MinLength.AutoSize = true;
            this.lbl_MinLength.Location = new System.Drawing.Point(21, 39);
            this.lbl_MinLength.Name = "lbl_MinLength";
            this.lbl_MinLength.Size = new System.Drawing.Size(63, 13);
            this.lbl_MinLength.TabIndex = 7;
            this.lbl_MinLength.Text = "Min Length:";
            this.lbl_MinLength.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // nud_MaxLength
            // 
            this.nud_MaxLength.Location = new System.Drawing.Point(86, 55);
            this.nud_MaxLength.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nud_MaxLength.Name = "nud_MaxLength";
            this.nud_MaxLength.Size = new System.Drawing.Size(35, 20);
            this.nud_MaxLength.TabIndex = 6;
            this.nud_MaxLength.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nud_MaxLength.ValueChanged += new System.EventHandler(this.nud_MaxLength_ValueChanged);
            // 
            // btn_Create
            // 
            this.btn_Create.Location = new System.Drawing.Point(201, 94);
            this.btn_Create.Name = "btn_Create";
            this.btn_Create.Size = new System.Drawing.Size(71, 23);
            this.btn_Create.TabIndex = 4;
            this.btn_Create.Text = "Create";
            this.btn_Create.UseVisualStyleBackColor = true;
            this.btn_Create.Click += new System.EventHandler(this.btn_Create_Click);
            // 
            // nud_MinLength
            // 
            this.nud_MinLength.Location = new System.Drawing.Point(86, 33);
            this.nud_MinLength.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.nud_MinLength.Name = "nud_MinLength";
            this.nud_MinLength.Size = new System.Drawing.Size(35, 20);
            this.nud_MinLength.TabIndex = 5;
            this.nud_MinLength.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nud_MinLength.ValueChanged += new System.EventHandler(this.nud_MinLength_ValueChanged);
            // 
            // pnl_StringSegments
            // 
            this.pnl_StringSegments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnl_StringSegments.AutoScroll = true;
            this.pnl_StringSegments.BackColor = System.Drawing.SystemColors.Control;
            this.pnl_StringSegments.Controls.Add(this.btn_AddStringSegment);
            this.pnl_StringSegments.Location = new System.Drawing.Point(12, 141);
            this.pnl_StringSegments.Name = "pnl_StringSegments";
            this.pnl_StringSegments.Size = new System.Drawing.Size(462, 256);
            this.pnl_StringSegments.TabIndex = 29;
            // 
            // btn_AddStringSegment
            // 
            this.btn_AddStringSegment.AutoSize = true;
            this.btn_AddStringSegment.Location = new System.Drawing.Point(3, 7);
            this.btn_AddStringSegment.Name = "btn_AddStringSegment";
            this.btn_AddStringSegment.Size = new System.Drawing.Size(23, 23);
            this.btn_AddStringSegment.TabIndex = 30;
            this.btn_AddStringSegment.Text = "+";
            this.btn_AddStringSegment.UseVisualStyleBackColor = true;
            this.btn_AddStringSegment.Click += new System.EventHandler(this.btn_AddStringSegment_Click);
            // 
            // RegExBuilder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 409);
            this.Controls.Add(this.pnl_StringSegments);
            this.Controls.Add(this.gb_Global);
            this.Name = "RegExBuilder";
            this.Text = "Regular Expression Builder";
            this.gb_Global.ResumeLayout(false);
            this.gb_Global.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_MaxLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_MinLength)).EndInit();
            this.pnl_StringSegments.ResumeLayout(false);
            this.pnl_StringSegments.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_Global;
        private System.Windows.Forms.Label lbl_RegularExpression;
        private System.Windows.Forms.Label lbl_TestString;
        private System.Windows.Forms.TextBox tb_TestString;
        private System.Windows.Forms.Button btn_Test;
        private System.Windows.Forms.Label lbl_ExcludedChararacters;
        private System.Windows.Forms.Label lbl_MaxLength;
        private System.Windows.Forms.TextBox tb_ExcludedChararacters;
        private System.Windows.Forms.Label lbl_MinLength;
        private System.Windows.Forms.NumericUpDown nud_MaxLength;
        private System.Windows.Forms.Button btn_Create;
        private System.Windows.Forms.NumericUpDown nud_MinLength;
        private System.Windows.Forms.RichTextBox rtb_RegularExpression;
        private System.Windows.Forms.Panel pnl_StringSegments;
        private System.Windows.Forms.TextBox tb_Result;
        private System.Windows.Forms.Button btn_AddStringSegment;
        private System.Windows.Forms.CheckBox cb_CaseSensitivity;

    }
}


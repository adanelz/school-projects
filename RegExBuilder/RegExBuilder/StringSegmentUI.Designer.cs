﻿namespace RegExBuilder
{
    partial class StringSegmentUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gb_StringSegment = new System.Windows.Forms.GroupBox();
            this.lbl_SpecificString = new System.Windows.Forms.Label();
            this.cb_CaseSensitivity = new System.Windows.Forms.CheckBox();
            this.gb_MoreContent = new System.Windows.Forms.GroupBox();
            this.cb_CharRange = new System.Windows.Forms.CheckBox();
            this.cb_NumRange = new System.Windows.Forms.CheckBox();
            this.dud_ACSLow_RangeEnd = new System.Windows.Forms.DomainUpDown();
            this.dud_ACSLow_RangeStart = new System.Windows.Forms.DomainUpDown();
            this.dud_ACSUpp_RangeEnd = new System.Windows.Forms.DomainUpDown();
            this.dud_ACSUpp_RangeStart = new System.Windows.Forms.DomainUpDown();
            this.lbl_ACS_SpecificNumbers = new System.Windows.Forms.Label();
            this.lbl_ANS = new System.Windows.Forms.Label();
            this.lbl_ACS = new System.Windows.Forms.Label();
            this.lbl_ANS_SpecificNumbers = new System.Windows.Forms.Label();
            this.tb_ACS_SpecificCharacters = new System.Windows.Forms.TextBox();
            this.tb_ANS_SpecificNumbers = new System.Windows.Forms.TextBox();
            this.nud_ANS_RangeEnd = new System.Windows.Forms.NumericUpDown();
            this.nud_ANS_RangeStart = new System.Windows.Forms.NumericUpDown();
            this.cb_MoreContent = new System.Windows.Forms.CheckBox();
            this.tb_SpecificString = new System.Windows.Forms.TextBox();
            this.gb_StringSegment.SuspendLayout();
            this.gb_MoreContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ANS_RangeEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ANS_RangeStart)).BeginInit();
            this.SuspendLayout();
            // 
            // gb_StringSegment
            // 
            this.gb_StringSegment.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gb_StringSegment.BackColor = System.Drawing.SystemColors.Control;
            this.gb_StringSegment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gb_StringSegment.Controls.Add(this.lbl_SpecificString);
            this.gb_StringSegment.Controls.Add(this.cb_CaseSensitivity);
            this.gb_StringSegment.Controls.Add(this.gb_MoreContent);
            this.gb_StringSegment.Controls.Add(this.cb_MoreContent);
            this.gb_StringSegment.Controls.Add(this.tb_SpecificString);
            this.gb_StringSegment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gb_StringSegment.Location = new System.Drawing.Point(0, 1);
            this.gb_StringSegment.Name = "gb_StringSegment";
            this.gb_StringSegment.Size = new System.Drawing.Size(160, 227);
            this.gb_StringSegment.TabIndex = 27;
            this.gb_StringSegment.TabStop = false;
            // 
            // lbl_SpecificString
            // 
            this.lbl_SpecificString.AutoSize = true;
            this.lbl_SpecificString.Location = new System.Drawing.Point(6, 24);
            this.lbl_SpecificString.Name = "lbl_SpecificString";
            this.lbl_SpecificString.Size = new System.Drawing.Size(78, 13);
            this.lbl_SpecificString.TabIndex = 36;
            this.lbl_SpecificString.Text = "Specific String:";
            // 
            // cb_CaseSensitivity
            // 
            this.cb_CaseSensitivity.AutoSize = true;
            this.cb_CaseSensitivity.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cb_CaseSensitivity.Checked = true;
            this.cb_CaseSensitivity.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_CaseSensitivity.Location = new System.Drawing.Point(55, 11);
            this.cb_CaseSensitivity.Name = "cb_CaseSensitivity";
            this.cb_CaseSensitivity.Size = new System.Drawing.Size(100, 17);
            this.cb_CaseSensitivity.TabIndex = 33;
            this.cb_CaseSensitivity.Text = "Case Sensitivity";
            this.cb_CaseSensitivity.UseVisualStyleBackColor = true;
            // 
            // gb_MoreContent
            // 
            this.gb_MoreContent.Controls.Add(this.cb_CharRange);
            this.gb_MoreContent.Controls.Add(this.cb_NumRange);
            this.gb_MoreContent.Controls.Add(this.dud_ACSLow_RangeEnd);
            this.gb_MoreContent.Controls.Add(this.dud_ACSLow_RangeStart);
            this.gb_MoreContent.Controls.Add(this.dud_ACSUpp_RangeEnd);
            this.gb_MoreContent.Controls.Add(this.dud_ACSUpp_RangeStart);
            this.gb_MoreContent.Controls.Add(this.lbl_ACS_SpecificNumbers);
            this.gb_MoreContent.Controls.Add(this.lbl_ANS);
            this.gb_MoreContent.Controls.Add(this.lbl_ACS);
            this.gb_MoreContent.Controls.Add(this.lbl_ANS_SpecificNumbers);
            this.gb_MoreContent.Controls.Add(this.tb_ACS_SpecificCharacters);
            this.gb_MoreContent.Controls.Add(this.tb_ANS_SpecificNumbers);
            this.gb_MoreContent.Controls.Add(this.nud_ANS_RangeEnd);
            this.gb_MoreContent.Controls.Add(this.nud_ANS_RangeStart);
            this.gb_MoreContent.Enabled = false;
            this.gb_MoreContent.Location = new System.Drawing.Point(5, 78);
            this.gb_MoreContent.Name = "gb_MoreContent";
            this.gb_MoreContent.Size = new System.Drawing.Size(151, 144);
            this.gb_MoreContent.TabIndex = 35;
            this.gb_MoreContent.TabStop = false;
            // 
            // cb_CharRange
            // 
            this.cb_CharRange.AutoSize = true;
            this.cb_CharRange.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cb_CharRange.Location = new System.Drawing.Point(5, 76);
            this.cb_CharRange.Name = "cb_CharRange";
            this.cb_CharRange.Size = new System.Drawing.Size(61, 17);
            this.cb_CharRange.TabIndex = 32;
            this.cb_CharRange.Text = "Range:";
            this.cb_CharRange.UseVisualStyleBackColor = true;
            this.cb_CharRange.CheckedChanged += new System.EventHandler(this.cb_CharRange_CheckedChanged);
            // 
            // cb_NumRange
            // 
            this.cb_NumRange.AutoSize = true;
            this.cb_NumRange.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cb_NumRange.Checked = true;
            this.cb_NumRange.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_NumRange.Location = new System.Drawing.Point(4, 21);
            this.cb_NumRange.Name = "cb_NumRange";
            this.cb_NumRange.Size = new System.Drawing.Size(61, 17);
            this.cb_NumRange.TabIndex = 25;
            this.cb_NumRange.Text = "Range:";
            this.cb_NumRange.UseVisualStyleBackColor = true;
            this.cb_NumRange.CheckedChanged += new System.EventHandler(this.cb_NumRange_CheckedChanged);
            // 
            // dud_ACSLow_RangeEnd
            // 
            this.dud_ACSLow_RangeEnd.Enabled = false;
            this.dud_ACSLow_RangeEnd.Items.Add("a");
            this.dud_ACSLow_RangeEnd.Items.Add("b");
            this.dud_ACSLow_RangeEnd.Items.Add("c");
            this.dud_ACSLow_RangeEnd.Items.Add("d");
            this.dud_ACSLow_RangeEnd.Items.Add("e");
            this.dud_ACSLow_RangeEnd.Items.Add("f");
            this.dud_ACSLow_RangeEnd.Items.Add("g");
            this.dud_ACSLow_RangeEnd.Items.Add("h");
            this.dud_ACSLow_RangeEnd.Items.Add("i");
            this.dud_ACSLow_RangeEnd.Items.Add("j");
            this.dud_ACSLow_RangeEnd.Items.Add("k");
            this.dud_ACSLow_RangeEnd.Items.Add("l");
            this.dud_ACSLow_RangeEnd.Items.Add("m");
            this.dud_ACSLow_RangeEnd.Items.Add("n");
            this.dud_ACSLow_RangeEnd.Items.Add("o");
            this.dud_ACSLow_RangeEnd.Items.Add("p");
            this.dud_ACSLow_RangeEnd.Items.Add("q");
            this.dud_ACSLow_RangeEnd.Items.Add("r");
            this.dud_ACSLow_RangeEnd.Items.Add("s");
            this.dud_ACSLow_RangeEnd.Items.Add("t");
            this.dud_ACSLow_RangeEnd.Items.Add("u");
            this.dud_ACSLow_RangeEnd.Items.Add("v");
            this.dud_ACSLow_RangeEnd.Items.Add("w");
            this.dud_ACSLow_RangeEnd.Items.Add("x");
            this.dud_ACSLow_RangeEnd.Items.Add("y");
            this.dud_ACSLow_RangeEnd.Items.Add("z");
            this.dud_ACSLow_RangeEnd.Location = new System.Drawing.Point(38, 117);
            this.dud_ACSLow_RangeEnd.Name = "dud_ACSLow_RangeEnd";
            this.dud_ACSLow_RangeEnd.Size = new System.Drawing.Size(30, 20);
            this.dud_ACSLow_RangeEnd.TabIndex = 31;
            this.dud_ACSLow_RangeEnd.Text = "z";
            this.dud_ACSLow_RangeEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dud_ACSLow_RangeStart
            // 
            this.dud_ACSLow_RangeStart.Enabled = false;
            this.dud_ACSLow_RangeStart.Items.Add("a");
            this.dud_ACSLow_RangeStart.Items.Add("b");
            this.dud_ACSLow_RangeStart.Items.Add("c");
            this.dud_ACSLow_RangeStart.Items.Add("d");
            this.dud_ACSLow_RangeStart.Items.Add("e");
            this.dud_ACSLow_RangeStart.Items.Add("f");
            this.dud_ACSLow_RangeStart.Items.Add("g");
            this.dud_ACSLow_RangeStart.Items.Add("h");
            this.dud_ACSLow_RangeStart.Items.Add("i");
            this.dud_ACSLow_RangeStart.Items.Add("j");
            this.dud_ACSLow_RangeStart.Items.Add("k");
            this.dud_ACSLow_RangeStart.Items.Add("l");
            this.dud_ACSLow_RangeStart.Items.Add("m");
            this.dud_ACSLow_RangeStart.Items.Add("n");
            this.dud_ACSLow_RangeStart.Items.Add("o");
            this.dud_ACSLow_RangeStart.Items.Add("p");
            this.dud_ACSLow_RangeStart.Items.Add("q");
            this.dud_ACSLow_RangeStart.Items.Add("r");
            this.dud_ACSLow_RangeStart.Items.Add("s");
            this.dud_ACSLow_RangeStart.Items.Add("t");
            this.dud_ACSLow_RangeStart.Items.Add("u");
            this.dud_ACSLow_RangeStart.Items.Add("v");
            this.dud_ACSLow_RangeStart.Items.Add("w");
            this.dud_ACSLow_RangeStart.Items.Add("x");
            this.dud_ACSLow_RangeStart.Items.Add("y");
            this.dud_ACSLow_RangeStart.Items.Add("z");
            this.dud_ACSLow_RangeStart.Location = new System.Drawing.Point(4, 117);
            this.dud_ACSLow_RangeStart.Name = "dud_ACSLow_RangeStart";
            this.dud_ACSLow_RangeStart.Size = new System.Drawing.Size(30, 20);
            this.dud_ACSLow_RangeStart.TabIndex = 30;
            this.dud_ACSLow_RangeStart.Text = "a";
            this.dud_ACSLow_RangeStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dud_ACSUpp_RangeEnd
            // 
            this.dud_ACSUpp_RangeEnd.Enabled = false;
            this.dud_ACSUpp_RangeEnd.Items.Add("A");
            this.dud_ACSUpp_RangeEnd.Items.Add("B");
            this.dud_ACSUpp_RangeEnd.Items.Add("C");
            this.dud_ACSUpp_RangeEnd.Items.Add("D");
            this.dud_ACSUpp_RangeEnd.Items.Add("E");
            this.dud_ACSUpp_RangeEnd.Items.Add("F");
            this.dud_ACSUpp_RangeEnd.Items.Add("G");
            this.dud_ACSUpp_RangeEnd.Items.Add("H");
            this.dud_ACSUpp_RangeEnd.Items.Add("I");
            this.dud_ACSUpp_RangeEnd.Items.Add("J");
            this.dud_ACSUpp_RangeEnd.Items.Add("K");
            this.dud_ACSUpp_RangeEnd.Items.Add("L");
            this.dud_ACSUpp_RangeEnd.Items.Add("M");
            this.dud_ACSUpp_RangeEnd.Items.Add("N");
            this.dud_ACSUpp_RangeEnd.Items.Add("O");
            this.dud_ACSUpp_RangeEnd.Items.Add("P");
            this.dud_ACSUpp_RangeEnd.Items.Add("Q");
            this.dud_ACSUpp_RangeEnd.Items.Add("R");
            this.dud_ACSUpp_RangeEnd.Items.Add("S");
            this.dud_ACSUpp_RangeEnd.Items.Add("T");
            this.dud_ACSUpp_RangeEnd.Items.Add("U");
            this.dud_ACSUpp_RangeEnd.Items.Add("V");
            this.dud_ACSUpp_RangeEnd.Items.Add("W");
            this.dud_ACSUpp_RangeEnd.Items.Add("X");
            this.dud_ACSUpp_RangeEnd.Items.Add("Y");
            this.dud_ACSUpp_RangeEnd.Items.Add("Z");
            this.dud_ACSUpp_RangeEnd.Location = new System.Drawing.Point(38, 94);
            this.dud_ACSUpp_RangeEnd.Name = "dud_ACSUpp_RangeEnd";
            this.dud_ACSUpp_RangeEnd.Size = new System.Drawing.Size(30, 20);
            this.dud_ACSUpp_RangeEnd.TabIndex = 29;
            this.dud_ACSUpp_RangeEnd.Text = "Z";
            this.dud_ACSUpp_RangeEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dud_ACSUpp_RangeStart
            // 
            this.dud_ACSUpp_RangeStart.Enabled = false;
            this.dud_ACSUpp_RangeStart.Items.Add("A");
            this.dud_ACSUpp_RangeStart.Items.Add("B");
            this.dud_ACSUpp_RangeStart.Items.Add("C");
            this.dud_ACSUpp_RangeStart.Items.Add("D");
            this.dud_ACSUpp_RangeStart.Items.Add("E");
            this.dud_ACSUpp_RangeStart.Items.Add("F");
            this.dud_ACSUpp_RangeStart.Items.Add("G");
            this.dud_ACSUpp_RangeStart.Items.Add("H");
            this.dud_ACSUpp_RangeStart.Items.Add("I");
            this.dud_ACSUpp_RangeStart.Items.Add("J");
            this.dud_ACSUpp_RangeStart.Items.Add("K");
            this.dud_ACSUpp_RangeStart.Items.Add("L");
            this.dud_ACSUpp_RangeStart.Items.Add("M");
            this.dud_ACSUpp_RangeStart.Items.Add("N");
            this.dud_ACSUpp_RangeStart.Items.Add("O");
            this.dud_ACSUpp_RangeStart.Items.Add("P");
            this.dud_ACSUpp_RangeStart.Items.Add("Q");
            this.dud_ACSUpp_RangeStart.Items.Add("R");
            this.dud_ACSUpp_RangeStart.Items.Add("S");
            this.dud_ACSUpp_RangeStart.Items.Add("T");
            this.dud_ACSUpp_RangeStart.Items.Add("U");
            this.dud_ACSUpp_RangeStart.Items.Add("V");
            this.dud_ACSUpp_RangeStart.Items.Add("W");
            this.dud_ACSUpp_RangeStart.Items.Add("X");
            this.dud_ACSUpp_RangeStart.Items.Add("Y");
            this.dud_ACSUpp_RangeStart.Items.Add("Z");
            this.dud_ACSUpp_RangeStart.Location = new System.Drawing.Point(4, 94);
            this.dud_ACSUpp_RangeStart.Name = "dud_ACSUpp_RangeStart";
            this.dud_ACSUpp_RangeStart.Size = new System.Drawing.Size(30, 20);
            this.dud_ACSUpp_RangeStart.TabIndex = 28;
            this.dud_ACSUpp_RangeStart.Text = "A";
            this.dud_ACSUpp_RangeStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbl_ACS_SpecificNumbers
            // 
            this.lbl_ACS_SpecificNumbers.AutoSize = true;
            this.lbl_ACS_SpecificNumbers.Location = new System.Drawing.Point(73, 89);
            this.lbl_ACS_SpecificNumbers.Name = "lbl_ACS_SpecificNumbers";
            this.lbl_ACS_SpecificNumbers.Size = new System.Drawing.Size(48, 13);
            this.lbl_ACS_SpecificNumbers.TabIndex = 26;
            this.lbl_ACS_SpecificNumbers.Text = "Specific:";
            // 
            // lbl_ANS
            // 
            this.lbl_ANS.AutoSize = true;
            this.lbl_ANS.Location = new System.Drawing.Point(2, 9);
            this.lbl_ANS.Name = "lbl_ANS";
            this.lbl_ANS.Size = new System.Drawing.Size(113, 13);
            this.lbl_ANS.TabIndex = 25;
            this.lbl_ANS.Text = "Allowable Numeric Set";
            // 
            // lbl_ACS
            // 
            this.lbl_ACS.AutoSize = true;
            this.lbl_ACS.Location = new System.Drawing.Point(2, 62);
            this.lbl_ACS.Name = "lbl_ACS";
            this.lbl_ACS.Size = new System.Drawing.Size(120, 13);
            this.lbl_ACS.TabIndex = 23;
            this.lbl_ACS.Text = "Allowable Character Set";
            // 
            // lbl_ANS_SpecificNumbers
            // 
            this.lbl_ANS_SpecificNumbers.AutoSize = true;
            this.lbl_ANS_SpecificNumbers.Location = new System.Drawing.Point(72, 24);
            this.lbl_ANS_SpecificNumbers.Name = "lbl_ANS_SpecificNumbers";
            this.lbl_ANS_SpecificNumbers.Size = new System.Drawing.Size(48, 13);
            this.lbl_ANS_SpecificNumbers.TabIndex = 22;
            this.lbl_ANS_SpecificNumbers.Text = "Specific:";
            // 
            // tb_ACS_SpecificCharacters
            // 
            this.tb_ACS_SpecificCharacters.Location = new System.Drawing.Point(72, 105);
            this.tb_ACS_SpecificCharacters.Name = "tb_ACS_SpecificCharacters";
            this.tb_ACS_SpecificCharacters.Size = new System.Drawing.Size(77, 20);
            this.tb_ACS_SpecificCharacters.TabIndex = 21;
            this.tb_ACS_SpecificCharacters.Leave += new System.EventHandler(this.tb_ACS_SpecificCharacters_Leave);
            // 
            // tb_ANS_SpecificNumbers
            // 
            this.tb_ANS_SpecificNumbers.Location = new System.Drawing.Point(71, 39);
            this.tb_ANS_SpecificNumbers.Name = "tb_ANS_SpecificNumbers";
            this.tb_ANS_SpecificNumbers.Size = new System.Drawing.Size(77, 20);
            this.tb_ANS_SpecificNumbers.TabIndex = 20;
            this.tb_ANS_SpecificNumbers.Leave += new System.EventHandler(this.tb_ANS_SpecificNumbers_Leave);
            // 
            // nud_ANS_RangeEnd
            // 
            this.nud_ANS_RangeEnd.Location = new System.Drawing.Point(38, 38);
            this.nud_ANS_RangeEnd.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nud_ANS_RangeEnd.Name = "nud_ANS_RangeEnd";
            this.nud_ANS_RangeEnd.Size = new System.Drawing.Size(30, 20);
            this.nud_ANS_RangeEnd.TabIndex = 19;
            this.nud_ANS_RangeEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nud_ANS_RangeEnd.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // nud_ANS_RangeStart
            // 
            this.nud_ANS_RangeStart.Location = new System.Drawing.Point(4, 38);
            this.nud_ANS_RangeStart.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.nud_ANS_RangeStart.Name = "nud_ANS_RangeStart";
            this.nud_ANS_RangeStart.Size = new System.Drawing.Size(30, 20);
            this.nud_ANS_RangeStart.TabIndex = 18;
            this.nud_ANS_RangeStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // cb_MoreContent
            // 
            this.cb_MoreContent.AutoSize = true;
            this.cb_MoreContent.Location = new System.Drawing.Point(9, 64);
            this.cb_MoreContent.Name = "cb_MoreContent";
            this.cb_MoreContent.Size = new System.Drawing.Size(118, 17);
            this.cb_MoreContent.TabIndex = 34;
            this.cb_MoreContent.Text = "Allow More Content";
            this.cb_MoreContent.UseVisualStyleBackColor = true;
            this.cb_MoreContent.CheckedChanged += new System.EventHandler(this.cb_MoreContent_CheckedChanged);
            // 
            // tb_SpecificString
            // 
            this.tb_SpecificString.Location = new System.Drawing.Point(6, 38);
            this.tb_SpecificString.Name = "tb_SpecificString";
            this.tb_SpecificString.Size = new System.Drawing.Size(149, 20);
            this.tb_SpecificString.TabIndex = 0;
            // 
            // StringSegmentUI
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(160, 230);
            this.Controls.Add(this.gb_StringSegment);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "StringSegmentUI";
            this.Text = "StringSegmentUI";
            this.gb_StringSegment.ResumeLayout(false);
            this.gb_StringSegment.PerformLayout();
            this.gb_MoreContent.ResumeLayout(false);
            this.gb_MoreContent.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ANS_RangeEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ANS_RangeStart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_StringSegment;
        private System.Windows.Forms.CheckBox cb_CaseSensitivity;
        private System.Windows.Forms.GroupBox gb_MoreContent;
        private System.Windows.Forms.CheckBox cb_CharRange;
        private System.Windows.Forms.CheckBox cb_NumRange;
        private System.Windows.Forms.DomainUpDown dud_ACSLow_RangeEnd;
        private System.Windows.Forms.DomainUpDown dud_ACSLow_RangeStart;
        private System.Windows.Forms.DomainUpDown dud_ACSUpp_RangeEnd;
        private System.Windows.Forms.DomainUpDown dud_ACSUpp_RangeStart;
        private System.Windows.Forms.Label lbl_ACS_SpecificNumbers;
        private System.Windows.Forms.Label lbl_ANS;
        private System.Windows.Forms.Label lbl_ACS;
        private System.Windows.Forms.Label lbl_ANS_SpecificNumbers;
        private System.Windows.Forms.TextBox tb_ACS_SpecificCharacters;
        private System.Windows.Forms.TextBox tb_ANS_SpecificNumbers;
        private System.Windows.Forms.NumericUpDown nud_ANS_RangeEnd;
        private System.Windows.Forms.NumericUpDown nud_ANS_RangeStart;
        private System.Windows.Forms.CheckBox cb_MoreContent;
        private System.Windows.Forms.TextBox tb_SpecificString;
        private System.Windows.Forms.Label lbl_SpecificString;

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PathPlanningAI
{
    public class Vector2
    {
        public int X { get; set; }
        public int Y { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>

        public Vector2()
        {
            this.X = 0;
            this.Y = 0;
        }

        public Vector2(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}

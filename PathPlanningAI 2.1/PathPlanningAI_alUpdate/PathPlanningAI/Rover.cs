﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PathPlanningAI
{
    public class Rover
    {
        public Waypoint StartWaypoint { get; set; }
        public Waypoint DestinationWaypoint { get; set; }
        public Waypoint CurrentWaypoint { get; set; }
        public float Speed { get; set; }
        public Course Route { get; set; }
        public DecisionEngine AI;

        public Rover()
        {
            this.StartWaypoint = new Waypoint();
            this.DestinationWaypoint = new Waypoint();
            this.CurrentWaypoint = new Waypoint();
            this.Speed = 0;
            this.Route = new Course();
            this.AI = new DecisionEngine();
        }

        /// <summary>
        /// set the position of the rover on it's path based on the closest X,Y coordinate
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void UpdatePosition(int x, int y)
        {
            //           {index, weight}
            int[] bestNode = {0, int.MaxValue};
            int tmp;

            for (int i = 0; i < this.Route.Path.Count; i++)
            {
                Waypoint current = this.Route.Path[i];
                tmp = Math.Abs(x - current.Location.X) + Math.Abs(y - current.Location.Y);
                if (tmp < bestNode[1])
                {
                    bestNode[0] = i; //index
                    bestNode[1] = tmp; //weight
                }
            }

            this.CurrentWaypoint = this.Route.Path[bestNode[0]];
        }

        /// <summary>
        /// set the Start of the rover path in the grid and on the bm
        /// </summary>
        /// <param name="gridX"></param>
        /// <param name="gridY"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetStart(int x, int y)
        {
            this.StartWaypoint.Location.X = x;
            this.StartWaypoint.Location.Y = y;
        }

        /// <summary>
        /// set the destination of the rover path in the grid and on the bm
        /// </summary>
        /// <param name="gridX"></param>
        /// <param name="gridY"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void SetDestination(int x, int y)
        {
            this.DestinationWaypoint.Location.X = x;
            this.DestinationWaypoint.Location.Y = y;
        }

        public int PathIndexOf(Waypoint wp)
        {
            return this.Route.Path.IndexOf(wp);
        }

        public void GotToNextWaypoint()
        {
            int nextIndex = this.Route.Path.IndexOf(this.CurrentWaypoint);

            nextIndex++;

            if (nextIndex < this.Route.Path.Count && nextIndex != 0)
            {
                this.UpdatePosition(this.Route.Path[nextIndex].Location.X, this.Route.Path[nextIndex].Location.Y);
            }
        }
    }
}

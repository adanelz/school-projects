﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Collections;

namespace PathPlanningAI
{

    public class DecisionEngine
    {
        //changed every time pixelate is called
        public Waypoint[,] Grid;
        public Waypoint[,] MasterGrid;
        public List<Waypoint> Nodes;
        public List<Waypoint> Found;

        public System.Drawing.Bitmap Map { get; set; }
        private Rover MarsRover { get; set; }
        public System.Drawing.Bitmap Pixelated { get; set; }


        /// <summary>
        /// Elevation Constant, the higher this number the bigger 
        /// the elevation change that the rover will ignore.
        /// </summary>
        protected int E = 7;
        /// <summary>
        /// Distance Constant
        /// </summary>
        protected int D = 3;

        protected int RoverScanRange = 40;
        protected int RoverScanSensitivity = 2;
        protected int RoverUpdateFequency = 1;
       
        public int ECC = 10;
        public int InitialScanSensitivity = 8;
        protected int Amount;
        protected System.Drawing.Rectangle Rectangle;

        public DecisionEngine()
        {
            Nodes = new List<Waypoint>();
            Found = new List<Waypoint>();
        }

        public void Initialize(System.Drawing.Bitmap heightMap, Rover newRover)
        {
            Map = heightMap;
            MarsRover = newRover;
            Found = new List<Waypoint>();
            this.MasterGrid = new Waypoint[heightMap.Width, heightMap.Height];
            Pixelate(heightMap, new System.Drawing.Rectangle(0, 0, heightMap.Width, heightMap.Height), InitialScanSensitivity);
        }

        public System.Drawing.Bitmap DrawPath(int roverX, int roverY)
        {

            System.Drawing.Bitmap bm = new System.Drawing.Bitmap(MarsRover.AI.Map);
            //Bitmap bm = new Bitmap(Pixelated);
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bm);

            List<Waypoint> path = MarsRover.Route.Path;

            System.Drawing.Pen courseLine = new System.Drawing.Pen(System.Drawing.Color.Red, 1);

            for (int i = 0; i < path.Count - 1; i++)
            {
                g.DrawLine(courseLine, path[i].Location.X,
                                        path[i].Location.Y,
                                        path[i + 1].Location.X,
                                        path[i + 1].Location.Y);
            }

            g.FillEllipse(new System.Drawing.SolidBrush(System.Drawing.Color.Green), roverX - 5, roverY - 5, 10, 10);

            return bm;
        }

        public void Pixelate(System.Drawing.Bitmap image, System.Drawing.Rectangle rectangle, int amount)
        {
            int width = image.Width;
            int height = image.Height;

            System.Drawing.Bitmap pixelated = new System.Drawing.Bitmap(image.Width, image.Height);
            Nodes = new List<Waypoint>();
            Amount = amount;
            Rectangle = rectangle;

            this.Grid = new Waypoint[(rectangle.Width / Amount) + 2, (rectangle.Height / Amount) + 2];

            // make an exact copy of the bitmap provided
            System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(pixelated);
            graphics.DrawImage(image, new System.Drawing.Rectangle(0, 0, width, height),
                    new System.Drawing.Rectangle(0, 0, width, height), System.Drawing.GraphicsUnit.Pixel);

            // look at every pixel in the rectangle while making sure we're within the image bounds
            for (Int32 ix = rectangle.X; ix <(rectangle.X + rectangle.Width); ix += Amount)
            {
                for (Int32 iy = rectangle.Y; iy < (rectangle.Y + rectangle.Height); iy += Amount)
                {

                    System.Drawing.Color pixel;
                    int count = 0;
                    int r = 0;
                    // get the pixel color in the center of the soon to be pixelated area
                    for (Int32 x = ix; x < ix + Amount; x++)
                    {
                        for (Int32 y = iy; y < iy + Amount; y++)
                        {
                            pixel = pixelated.GetPixel(ix, iy);
                            r += pixel.R;
                            count++;
                        }
                    }

                    //gets the average color
                    r = r / count;
                    int gridX = (ix - rectangle.X) / Amount;
                    int gridY = (iy - rectangle.Y) / Amount;
                    int centerX = ix + (Amount / 2);
                    int centerY = iy + (Amount / 2);

                    this.Grid[gridX, gridY] = new Waypoint(r, new Vector2(centerX, centerY));
                    this.MasterGrid[centerX, centerY] = Grid[gridX, gridY];
                    Nodes.Add(Grid[gridX, gridY]);
                }
                
            }
            this.Pixelated = pixelated;
        }

        public int[] GetClosestGridIndexes(Waypoint location)
        {
            int[] XnY = { 0, 0 };

            XnY[0] = (int)Math.Floor((double)((Math.Abs(location.Location.X - Rectangle.X)) / Amount));
            XnY[1] = (int)Math.Floor((double)((Math.Abs(location.Location.Y - Rectangle.Y)) / Amount));

            return XnY;
        }

        private Course Dijkstra(Waypoint Start, Waypoint End, int[] GridIndexes)
        {
            List<Waypoint> NodeQue = new List<Waypoint>();
            Course BestRoute = new Course();
            Waypoint current = Start;

            #region Initialize
            //Initialize each Node
            for (int i = 0; i < Nodes.Count; i++)
            {
                //add neighbors to each Node
                #region add neighbors to each Node
                Nodes[i].Neighbors.Clear();
                for (int ix = -1; ix <= 1; ix++)
                {//changes the x value
                    for (int iy = -1; iy <= 1; iy++)
                    {//changes the y value
                        GridIndexes = GetClosestGridIndexes(Nodes[i]);
                        int gridX = GridIndexes[0];
                        int gridY = GridIndexes[1];

                        if (((gridX + ix) < Math.Sqrt((double)Grid.Length)) && ((gridX + ix) >= 0) &&
                                ((gridY + iy) < Math.Sqrt((double)Grid.Length)) && ((gridY + iy) >= 0))
                        {//makes sure the node being checked and inside the bounds of the array
                            if (!(ix == 0 && iy == 0))
                            {
                                if (Grid[gridX + ix, gridY + iy] != null)
                                {//makes sure the node being checked isn't null
                                    Nodes[i].Neighbors.Add(Grid[gridX + ix, gridY + iy]);
                                }
                            }
                        }
                    }
                }
                #endregion

                //add each node to the que
                NodeQue.Add(Nodes[i]);

                //make each node's tmpweight to be Max value
                Nodes[i].TempWeight = int.MaxValue;

                //set starting node weight to 0
                if (Nodes[i] == Start)
                    Nodes[i].TempWeight = 0;
            }
            #endregion

            #region activate(discover) best weighted nodes
            while (current != End)
            {//stops when it finds the end node


                #region change neighbors weights
                for (int i = 0; i < current.Neighbors.Count; i++)
                {
                    Waypoint neighbor = current.Neighbors[i];
                    if (!neighbor.Discovered)
                    {

                        int disFromStart = Math.Max(Math.Abs(Start.Location.X - neighbor.Location.X), Math.Abs(Start.Location.Y - neighbor.Location.Y));
                        int disFromEnd = Math.Max(Math.Abs(End.Location.X - neighbor.Location.X), Math.Abs(End.Location.Y - neighbor.Location.Y));
                        //distance based on cooradinates
                        int distance = (disFromStart + disFromEnd);
                        //elevation based on weight vs. start weight
                        int elevation = (int)Math.Abs(Start.Weight - neighbor.Weight);

                        //distance = 0;
                        //elevation = 0;

                        if (elevation < ECC)
                        {
                            //get weight with distance constants
                            neighbor.TempWeight = elevation + (distance * D);
                        }
                        else
                        {
                            //get weight with elevation and distance constants
                            neighbor.TempWeight = ((elevation * E) + (distance * D));
                        }

                    }
                }
                #endregion

                #region find lightest node in nodeQue
                Waypoint minWeight = new Waypoint(int.MaxValue, int.MaxValue);
                for (int i = 0; i < NodeQue.Count; i++)
                {
                    //set QueIndex
                    NodeQue[i].QueIndex = i;

                    //find the least weighted node
                    if (NodeQue[i].TempWeight < minWeight.TempWeight)
                        minWeight = NodeQue[i];
                }
                #endregion

                //if no lighter node was found
                if (minWeight.Weight != int.MaxValue)
                {
                    //Move to the next node and remove current from que
                    minWeight.Discovered = true;
                    NodeQue.Remove(current);
                    current = minWeight;
                }
                else
                {
                    if (current.Neighbors[0] != null)
                        current.Neighbors[0].Discovered = true;
                }
            }
            #endregion

            //start from the end node
            current = End;
            //remove from que
            NodeQue.Remove(End);

            #region Make best route
            if (current != null && Start != null)
            {
                while (current != Start)
                {//stops when it gets back to start

                    //prevent lookback
                    current.Discovered = false;
                    //add to path
                    BestRoute.Path.Add(current);

                    Waypoint minWeight = new Waypoint(int.MaxValue, int.MaxValue);

                    //Example of where i could have used LinQ

                    //List<Waypoint> discNeighbors =
                    //    (from disc in current.Neighbors
                    //     where (disc.Discovered)
                    //     select disc).ToList();

                    //foreach (Waypoint wp in discNeighbors)
                    //{
                    //    if (wp.TempWeight < minWeight.TempWeight)
                    //        minWeight = wp;
                    //}

                    //instead of the below loop

                    for (int i = 0; i < current.Neighbors.Count; i++)
                    {
                        //find the least weighted node
                        if (current.Neighbors[i].Discovered)
                        {
                            if (current.Neighbors[i].TempWeight < minWeight.TempWeight)
                                minWeight = current.Neighbors[i];

                        }
                    }

                    //if no lighter node was found
                    if (minWeight.Weight == int.MaxValue)
                    {
                        Waypoint supposedEnd = BestRoute.Path[BestRoute.Path.Count - 1];
                        if (supposedEnd != Start)
                        {
                            for (int i = 0; i < BestRoute.Path.Count; i++)
                            {
                                BestRoute.Path[i].Discovered = true;
                            }
                            supposedEnd.Discovered = false;
                            current = End;
                            BestRoute.Path.Clear();
                        }
                        else
                        {
                            break;
                        }
                    }
                    else
                    {
                        //move current to next node
                        current = minWeight;
                    }
                }
            }
            #endregion

            BestRoute.Path.Add(Start);

            End.TempWeight = 0;

            //make start the 0 index
            BestRoute.FlipPath();

            return BestRoute;
        }

        public void PlanRoute()
        {// Pixelate() must run first
            int[] GridIndexes = { 0, 0 };
            GridIndexes = GetClosestGridIndexes(MarsRover.StartWaypoint);
            Waypoint Start = Grid[GridIndexes[0],GridIndexes[1]];
            GridIndexes = GetClosestGridIndexes(MarsRover.DestinationWaypoint);
            Waypoint End = Grid[GridIndexes[0],GridIndexes[1]];
            
            

            if (Grid != null)
            {//test if Pixelate() has generated the Grid

                MarsRover.Route = Dijkstra(Start, End, GridIndexes);
                MarsRover.Route.CleanRoute(0);
                
                MarsRover.CurrentWaypoint = MarsRover.Route.Start;

            }
            else
            {//image not pixelated
                MarsRover.Route = new Course();
            }
        }

        private Course PlanNewRoute(Waypoint start, Waypoint end)
        {// Pixelate() must run first
            int[] GridIndexes = { 0, 0 };

            GridIndexes = GetClosestGridIndexes(start);
            start = Grid[GridIndexes[0], GridIndexes[1]];
            GridIndexes = GetClosestGridIndexes(end);
            end = Grid[GridIndexes[0], GridIndexes[1]];

            if (Grid != null)
            {//test if Pixelate() has generated the Grid
                return Dijkstra(start, end, GridIndexes);
            }

            //image not pixelated
            return new Course();
        }

        public void UpdateRoute()
        {
            Course newRoute = new Course();

            #region Pixelate scan area
            int offset = RoverScanRange;

            //Rover location on bm
            int x = MarsRover.CurrentWaypoint.Location.X;
            int y = MarsRover.CurrentWaypoint.Location.Y;
            
            //Set size of scan area
            int width = offset * 2;
            int height = offset * 2;
            
            //offset to be left center
            x = x - offset;
            y = y - offset;


            //to close to the left edge
            if (x < 0)
                x = 0;

            //to close to the right edge
            if ((Map.Width - x) < width)
                x = Map.Width - width - 2;

            //to close to the top edge
            if (y < 0)
                y = 0;

            //to close to the bottom edge
            if ((Map.Height - y) < height)
                y = Map.Height - height - 2;

            System.Drawing.Rectangle scanArea = new System.Drawing.Rectangle(x, y, width, height);

            Pixelate(Map, scanArea, RoverScanSensitivity);
            #endregion

            Waypoint roverPos = MarsRover.CurrentWaypoint;
            Waypoint furthestVisibleWaypoint = new Waypoint();
            List<Waypoint> roverPath = MarsRover.Route.Path;

            if (MarsRover.Route.Path.IndexOf(roverPos) >= 0)
            {
                #region Find the new route
                for (int i = roverPath.IndexOf(roverPos); i < roverPath.Count; i++)
                {//Finds the waypoint that is farthes away from the rover yet still withing the scan area
                    if (i >= 0)
                    {
                        if ((roverPath[i].Location.X < (x + width) && roverPath[i].Location.Y < (y + height)) && (roverPath[i].Location.X > x && roverPath[i].Location.Y > y))
                        {//rover scan area
                            furthestVisibleWaypoint = MarsRover.Route.Path[i];
                        }
                    }
                }

                int startRemove = MarsRover.Route.Path.IndexOf(roverPos);
                int endRemove = MarsRover.Route.Path.IndexOf(furthestVisibleWaypoint);
                if (!MarsRover.Route.Path[startRemove].Enhanced)
                {
                    for (int i = 0; i < endRemove - startRemove; i++)
                    {
                        if (MarsRover.Route.Path[startRemove] != MarsRover.DestinationWaypoint)
                            MarsRover.Route.Path.RemoveAt(startRemove);

                    }

                    newRoute = PlanNewRoute(roverPos, furthestVisibleWaypoint);
                    newRoute.CleanRoute(0);
                    MarsRover.CurrentWaypoint = newRoute.Start;

                    for (int i = 0; i < RoverUpdateFequency && i < newRoute.Path.Count; i++)
                    {
                        newRoute.Path[i].Enhanced = true;
                    }

                    MarsRover.Route.Path.InsertRange(startRemove, newRoute.Path.ToArray());
                    if (!MarsRover.Route.Path.Contains(MarsRover.DestinationWaypoint))
                        MarsRover.Route.Path.Add(MarsRover.DestinationWaypoint);

                    MarsRover.CurrentWaypoint = newRoute.Start;

                    Found.Clear();
                    for (int i = 0; i < MarsRover.Route.Path.Count; i++)
                    {
                        Found.Add(MarsRover.Route.Path[i]);
                        //MarsRover.Route.Path[i].Enhanced = false;
                    }

                }
                #endregion

                MarsRover.Route.CleanRoute(MarsRover.Route.Path.IndexOf(roverPos));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PathPlanningAI
{
    public partial class Form1 : Form
    {
        Rover MarsRover = new Rover();
        int MouseX = 0;
        int MouseY = 0;
        Waypoint Start = new Waypoint();
        Waypoint End = new Waypoint();
        Bitmap Origional;
        Bitmap Pixelated;
        Waypoint onPath = new Waypoint();

        

        public Form1()
        {
            InitializeComponent();
            LoadMap();
        }

        //testing
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            LoadMap();
        }

        //testing
        private void LoadMap()
        {
            Bitmap bm = new Bitmap(PathPlanningAI.Properties.Resources.hm_Crater);

            Origional = bm;
            Pixelated = bm;

            pictureBox1.Image = bm;


            MarsRover.AI.Initialize(bm, MarsRover);



            timer1.Start();

        }

      

        //testing
        private Bitmap WriteWeights(Bitmap bm, Graphics g)
        {
            int amount = MarsRover.AI.InitialScanSensitivity;

            for (int i = 0; i < MarsRover.AI.Nodes.Count; i++)
            {
                if (MarsRover.AI.Nodes[i].TempWeight != int.MaxValue)
                {
                    int nodeX = MarsRover.AI.Nodes[i].Location.X - (amount / 2);
                    int nodeY = MarsRover.AI.Nodes[i].Location.Y - (amount / 2);
                    string weight = MarsRover.AI.Nodes[i].TempWeight.ToString();
                    g.DrawString(weight, new Font("Arial", 7), new SolidBrush(Color.Yellow), nodeX, nodeY);
                }
            }

            return bm;
        }

        //testing
        private void Reset()
        {
            MouseX = 0;
            MouseY = 0;


            MarsRover = new Rover();
            LoadMap();


            Start = new Waypoint();
            End = new Waypoint();
            checkBox1.Checked = false;
        }

        //testing
        //update mouse and such
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            int amount = MarsRover.AI.InitialScanSensitivity;

            if (e.X < pictureBox1.Image.Width && e.Y < pictureBox1.Image.Height)
            {

                MouseX = e.X;
                MouseY = e.Y;
                
                #region Placing the rover on the route
                //if (checkBox1.Checked)
                //{
                //    Bitmap bm = new Bitmap(MarsRover.AI.Map);
                //    //Bitmap bm = new Bitmap(Pixelated);
                //    Graphics g = Graphics.FromImage(bm);
                //    if (MarsRover.Route != null)
                //    {

                //        #region Find the closest node in the list
                //        Waypoint closest = new Waypoint(0, new Vector2(int.MaxValue, int.MaxValue));
                //        int currentDistance;
                //        int bestDistance = Math.Abs(Math.Abs(closest.Location.X - e.X) + Math.Abs(closest.Location.Y - e.Y));
                //        for (int i = 0; i < MarsRover.Route.Path.Count; i++)
                //        {
                //            currentDistance = Math.Abs(MarsRover.Route.Path[i].Location.X - e.X) + Math.Abs(MarsRover.Route.Path[i].Location.Y - e.Y);

                //            if (currentDistance < bestDistance)
                //            {
                //                closest = MarsRover.Route.Path[i];
                //                bestDistance = currentDistance;
                //            }
                //        }
                //        #endregion
                //        for (int i = 0; i < MarsRover.Route.Path.Count; i++)
                //        {
                //            int x = MarsRover.Route.Path[i].Location.X;
                //            int y = MarsRover.Route.Path[i].Location.Y;
                //            if (e.X == x || e.Y == y)
                //            {
                //                if (onPath != null)
                //                {
                //                    int newWeight = Math.Abs(MouseX - x) + Math.Abs(MouseY - y);
                //                    int oldWeight = Math.Abs(MouseX - onPath.Location.X) + Math.Abs(MouseY - onPath.Location.Y);
                //                    if (newWeight < oldWeight)
                //                    {
                //                        onPath = MarsRover.Route.Path[i];
                //                    }
                //                }
                //                else
                //                {
                //                    onPath = MarsRover.Route.Path[i];
                //                }
                //            }
                //        }

                //        onPath = closest;
                //        g.DrawEllipse(new Pen(Color.Blue, 5), (closest.Location.X), (closest.Location.Y), 2, 2);
                //        DrawPath(bm);
                //    }
                //}
                #endregion

            }


        }

        //simmulate game
        //map click
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            int amount = MarsRover.AI.InitialScanSensitivity;

            #region Simmulate rover location
            if (checkBox1.Checked)
            {
                if (onPath != null)
                {
                    //set new position
                    MarsRover.UpdatePosition(onPath.Location.X, onPath.Location.Y);
                    MarsRover.CurrentWaypoint = onPath;

                    MarsRover.AI.UpdateRoute();

                    pictureBox1.Image = MarsRover.AI.DrawPath(MarsRover.CurrentWaypoint.Location.X, MarsRover.CurrentWaypoint.Location.Y);
                    checkBox1.Checked = false;
                }
            }
            #endregion

            #region Set start and end location
            else
            {//select the rover start and end
                if (!Start.Discovered)
                {
                    LoadMap();
                    //
                    MarsRover.SetStart(MouseX, MouseY);//
                    Start.Discovered = true;
                }
                else
                {
                    //
                    MarsRover.SetDestination(MouseX, MouseY);//
                    End.Discovered = true;
                }

                if (Start.Discovered && End.Discovered)
                {
                    MarsRover.AI.PlanRoute();//
                    pictureBox1.Image = MarsRover.AI.DrawPath(MarsRover.CurrentWaypoint.Location.X, MarsRover.CurrentWaypoint.Location.Y);

                    Start = new Waypoint();
                    End = new Waypoint();

                }
            }
            #endregion
        }

        //Reset
        private void button1_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {

                if (MarsRover.Route.Path != null)
                {
                    //moves the rover to next waypoint. returns -1 when reaches the end
                    MarsRover.AI.UpdateRoute();//

                    MarsRover.GotToNextWaypoint();//
                    label4.Text = MarsRover.Route.Path.Count.ToString();
                    if (MarsRover.CurrentWaypoint == MarsRover.DestinationWaypoint)
                        timer1.Stop();

                    pictureBox1.Image = MarsRover.AI.DrawPath(MarsRover.CurrentWaypoint.Location.X, MarsRover.CurrentWaypoint.Location.Y);
                }
            }

            if(MarsRover.StartWaypoint != null)
                label3.Text = MarsRover.StartWaypoint.ToString();

            label1.Text = MarsRover.CurrentWaypoint.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MarsRover.Route.CleanRoute(0);

        }
    }
}

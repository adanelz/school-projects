﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PathPlanningAI
{
    public class Course
    {
        
        public List<Waypoint> Path { get; set; }
        public int Weight { get; set; }
        protected Waypoint start;
        protected Waypoint end;

        public Waypoint Start
        {
            get
            {
                return Path[0];
            }
            set
            {
                start = value;
            }
        }

        public Waypoint End
        {
            get
            {
                if (Path.Count > 0)
                    return Path[Path.Count - 1];
                else
                    return Path[0];
            }
            set
            {
                end = value;
            }
        }

        public Course()
        {
            this.Start = new Waypoint();
            this.End = new Waypoint();
            this.Path = new List<Waypoint>();
            this.Weight = int.MaxValue;
        }

        /// <summary>
        /// Switches the course so that the start is at the 0 index
        /// </summary>
        /// <returns> the flipped path</returns>
        public void FlipPath()
        {
            List<Waypoint> flipped = new List<Waypoint>();
            for (int i = this.Path.Count - 1; i >= 0; i--)
            {
                this.Path[i].QueIndex = flipped.Count;
                flipped.Add(this.Path[i]);
            }

            this.Path = flipped;
        }

        public void CleanRoute(int startIndex) 
        {
            //new course to be made
            Course bestRoute = new Course();
            //current node path
            List<Waypoint> nodeQue = this.Path;
            //neighbor distance limit
            int distance = 7;
            //index of the node to jump to
            int jump = 0;

            //add first node to the new route
            if (startIndex >= 0)
            {
                bestRoute.Path.Add(this.Start);

                if (startIndex != 0)
                {
                    for (int i = 0; i < startIndex; i++)
                    {
                        bestRoute.Path.Add(nodeQue[i]);
                    }
                }

                for (int i = startIndex; i < nodeQue.Count; i++)
                {//move through the entire que

                    //will never jump to 0th index
                    jump = 0;
                    for (int j = i + 2; j < nodeQue.Count; j++)
                    {//move through the que to compare to the neighbors of i
                        for (int k = 0; k < nodeQue[i].Neighbors.Count; k++)
                        {
                            if (nodeQue[i].Neighbors[k] == nodeQue[j])
                            {//found neighbor in que keep index
                                if (Math.Abs(nodeQue[i].Neighbors[k].Weight - nodeQue[j].Weight) < 8)
                                    //make sure that the new connection is not so steep that the rover cannot traverse it
                                    jump = j;
                            }
                        }

                        if ((Math.Abs(nodeQue[i].Location.X - nodeQue[j].Location.X) <= distance) && (Math.Abs(nodeQue[i].Location.Y - nodeQue[j].Location.Y) <= distance))
                        {
                            if (nodeQue[i] != nodeQue[j])
                            {
                                jump = j;
                            }
                        }

                    }

                    if (bestRoute.Path[bestRoute.Path.Count - 1] != nodeQue[i])
                        //add current node if not already in que
                        bestRoute.Path.Add(nodeQue[i]);

                    if (jump > 0)
                    {// add the node to jump to to cut out the uneeded sections of the path
                        bestRoute.Path.Add(nodeQue[jump]);
                        i = jump - 1;
                    }
                }

                //new route without unwanted switchbacks
                this.Path = bestRoute.Path;
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PathPlanningAI
{
    public class Waypoint
    {
        public float Weight { get; set; }
        public float TempWeight { get; set; }
        public Vector2 Location { get; set; }
        public bool Discovered { get; set; }
        public bool Enhanced { get; set; }
        public List<Waypoint> Neighbors { get; set; }
        public int QueIndex { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Waypoint()
        {
            this.Weight = 0;
            this.TempWeight = 0;
            this.Location = new Vector2();
            this.Discovered = false;
            this.Enhanced = false;
            this.Neighbors = new List<Waypoint>();
            this.QueIndex = 0;
        }

        /// <summary>
        /// weight constructor
        /// </summary>
        /// <param name="wight">The difference of color from white</param>
        public Waypoint(int weight)
        {
            this.Weight = weight;
            this.TempWeight = 0;
            this.Location = new Vector2();
            this.Discovered = false;
            this.Enhanced = false;
            this.Neighbors = new List<Waypoint>();
            this.QueIndex = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="tmpWeight"></param>
        public Waypoint(int weight, int tmpWeight)
        {
            this.Weight = weight;
            this.TempWeight = tmpWeight;
            this.Location = new Vector2();
            this.Discovered = false;
            this.Enhanced = false;
            this.Neighbors = new List<Waypoint>();
            this.QueIndex = 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="weight">The difference of color from white</param>
        /// <param name="x">referes to x location of waypoint on map in grid</param>
        /// <param name="y">referes to y location of waypoint on map in grid</param>
        public Waypoint(int weight, Vector2 location)
        {
            this.Weight = weight;
            this.TempWeight = 0;
            this.Location = location;
            this.Discovered = false;
            this.Enhanced = false;
            this.Neighbors = new List<Waypoint>();
            this.QueIndex = 0;
        }

        /// <summary>
        /// Specific constructor
        /// </summary>
        /// <param name="weight">The difference of color from white</param>
        /// <param name="tempweight"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="disc"></param>
        public Waypoint(float weight, float tempweight, Vector2 location , bool disc, List<Waypoint> list, int queIndex)
        {
            this.Weight = weight;
            this.TempWeight = tempweight;
            this.Location = location;
            this.Discovered = disc;
            this.Neighbors = list;
            this.QueIndex = queIndex;
            this.Enhanced = false;
        }

        public override string ToString()
        {
            return "(" + this.Location.X + ", " + this.Location.Y + ")";
        }
    }
}
